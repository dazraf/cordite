/*
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
const Proxy = require('braid-client').Proxy;

const emea = new Proxy({url: 'https://emea-test.cordite.foundation:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

let saltedDaoName = 'testDao-'+new Date().getTime()

function onOpen() {
    console.log(new Date().getTime())
    
    emea.dao.daoInfo(saltedDaoName).then(daos => {
        console.log("there were", daos.length, "existing daos with name", saltedDaoName )
        
        return emea.dao.createDao(saltedDaoName, "O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB")
    }).then(dao => {
        console.log(saltedDaoName,"created with key",JSON.stringify(dao.daoKey))
    }).catch(error => {
        console.error(error)
    })
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}