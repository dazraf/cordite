<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Cordite Braid JavaScript Client Example

One of the first things you will want to do with Cordite is to connect to it using some form of programmable client. 

This is the smallest example client we could build using [nodejs](https://nodejs.org) and [braid](https://gitlab.com/bluebank/braid).  Essentially it:

 * connects to test cordite
 * creates a new dao
 * proposes adding a new member to the dao

This repository contins the final code.  You can follow along with nice verbose instructions [here](docs.cordite.foundation/developers/braid-js-client)

## Pre-requisites

 * node js installed 

That's it.  We will be using the test cordite network, which is live, for this test.

## Terse Instructions

The rather terse synopsis is below:

 * create a directory in which to run your code
 * ```npm init```
 * ```npm install --save braid-client```
 * create a file called something like client.js
 * paste in the following code:

 ```javascript
const Proxy = require('braid-client').Proxy;

const emea = new Proxy({url: 'https://emea-test.cordite.foundation:8080/api/'}, onOpen, onClose, onError, {strictSSL: false})

let saltedDaoName = 'testDao-'+new Date().getTime()

function onOpen() {
    console.log(new Date().getTime())
    
    emea.dao.daoInfo(saltedDaoName).then(daos => {
        console.log("there were", daos.length, "existing daos with name", saltedDaoName )
        
        return emea.dao.createDao(saltedDaoName, "O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB")
    }).then(dao => {
        console.log(saltedDaoName,"created with key",JSON.stringify(dao.daoKey))
    }).catch(error => {
        console.error(error)
    })
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}
 ```

 * note that you have
   * imported the braid js Proxy
   * connected to the alphe one node
   * proven that there are 0 daos with the salted dao name
   * created a new dao
 


