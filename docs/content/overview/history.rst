History
=======


Cordite is as much a philosophy as it is an application, and to
understand it’s significance, you must also understand its journey.

The Distributed Ledger Technology space is fueled by disruption and
innovation with which unavoidably attracts challenges and at times
reservations. 2016 was all about performance and scalability, 2017 was
all about security. These are technical challenges. The harder
challenges are the social and political ones.

One of the key unsolved challenges is how to support a decentralised
business model. We believe that there is no point replacing one
centralised business model with another. 2018 will be the year that many
in the blockchain domain wake up to the fact that there is little point
in adopting DLT and distributing their technology if they are not going
to distribute the business model as well.

See the case for decentralisation:
https://medium.com/@rickcrook/the-case-for-de-centralisation-1ac14935a3fc

And it is this same philosophy, that lies at the core of Cordite.
Cordite builds on the work of Richard Gendal Brown, James Carlyle, Ian
Grigg and Mike Hearn in their Corda whitepaper in August 2016. The Corda
whitepaper introduces us to a Corda Distributed Application (“CorDapp”).
We expect a great number of CorDapps will be built to provide improved
or new services and to make operational efficiencies. One of the core
features of a CorDapp is that it is distributed with no centralised
aspect of the technology layer. We believe that distribution should not
be limited to the technology layer. Distribution needs to also permeate
through the business process the application supports. We would propose
that any centralisation in either the technology or business layers
would make the use of a distribute ledger technology redundant. A
traditional server-client based application would suffice if there is
centralisation in either the technology or business layers.

See the Corda Whitepaper at:
https://docs.corda.net/_static/corda-technical-whitepaper.pdf

The technology itself is travelling broadly in the right direction, with
open-source protocols remaining dominant over proprietary schemes. The
initial vendors in this space have largely pivoted their business models
after clients were reluctant to engage in full stack solutions. The
large technology vendors have attempted to gain ground in the space, and
to date no single vendor has managed to dominate and create vendor lock
in.

The activity is moving away from its exploration and research phase, and
towards targeted delivery and commercialisation of distributed
applications. This is largely being driven by consortiums, such as R3,
who are working with financial institutions and other leaders to launch
propositions into the market.

Many organisations have learnt that there is only so much fun you can
have on your own with a distributed ledger and collaborated to build
CorDapps. Some have chosen to create joint ventures or centralised legal
entities to fund and operate these CorDapps. The centralised business
layer renders the use of a distributed ledger technology redundant.
These CorDapps need a distributed funding and incentive for the build
and operation of these respectively. For these CorDapps to be truly
distributed their governance and economic model needs to be
de-centralised too.

As a result, we have Cordite, an open source CorDapp that provides
companies and corporations the distributed governance and economic
services that CorDapps will need to be truly distributed.
