Contributing
============


.. toctree::
    :hidden:
    :glob:

    contributing/*


Getting in touch
----------------

-  News is announced on `@We_are_Cordite <https://twitter.com/we_are_cordite>`__
-  More information can be found on `Cordite website <https://cordite.info>`__
-  We use #cordite channel on `Corda slack <https://slack.corda.net/>`__
-  We informally meet at the `Corda London meetup <https://www.meetup.com/pro/corda/>`__
-  email `community@cordite.foundation <mailto:community@cordite.foundation>`__

Troubleshooting
---------------

If you encounter any issues whilst using Cordite, and your issue is not
documented, please `raise an
issue <https://gitlab.com/cordite/cordite/issues/new>`__.

.. _contributing-1:

Contributing
------------

We welcome contributions both technical and non-technical with open
arms! There’s a lot of work to do here, and we’re especially concerned
with ensuring the longevity and reliability of the project.

Please take a look at our `issue
backlog <https://gitlab.com/cordite/cordite/issues>`__ if you are unsure
where to start with getting involved!

Repository Access
-----------------

While we are in private alpha, please go to the slack channel (#cordite
channel on `Corda slack <https://slack.corda.net/>`__) to request access
to the `repository <https://gitlab.com/cordite/cordite>`__

Releases
--------

The first release, v0.1.0, was tagged through gitlab, on a version of
the code that we saw pass the pipeline. Unless mentioned otherwise the
process will continue the same and we will be releasing every two weeks.
Currently we will not really follow SEMVAR specification util the api
settles down. Once it is, we will release v1.0.0 and follow SEMVAR from
there onwards.


