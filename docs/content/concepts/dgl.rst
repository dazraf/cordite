.. _dgl:

DGL
===

The Distributed General Ledger provides a de-centralised way to manage accounts and issue tokens by:
-  Providing de-centralised stores and transfers of value allowing new financial instruments to be created inside the existing regulatory framework. eg. tokens, crypto-coins, digital cash, virtual currency, distributed fees, securitisation
-  Allowing participants to record, manage and execute financial agreements in perfect synchrony with their peers, creating a world of frictionless commerce.
-  Providing a financial toolkit that can include promissory notes, settlement issuances and account aliases.

Why are we interested in a DGL?
-------------------------------

When a company provides goods or services to another, they tend to
expect payment in return. For hundreds of years, companies have recorded
such sales or purchases on their respective ledgers. These general
ledgers represent what they own (their assets) and what they owe (their
liabilities) and both companies will endeavor to ensure both ledgers
match.

In order to do this, they rely on sharing purchase orders, invoices,
payment instructions and receipts. These formal documents are followed
up with a range of communication including meetings, emails, facsimile
and phone calls. All of these resources and efforts are exhausted,
simply to try and keep the two over lapping accounts (sales and purchase
in this example) matched on their respective ledgers.

A company will likely have many of these overlapping accounts between
themselves, their clients and suppliers. As you can imagine, keeping
these all aligned becomes a tremendous drag on all parties involved.

Where do banks fits in?
-----------------------

Banks will also have a general ledger representing their assets and
liabilities. One of the liabilities on this ledger will be the deposits
their corporate customers have made into the bank. The bank calls these
deposit 'accounts' on their ledger, but the corporate customer will see
these as 'cash (or bank) accounts' on their ledger.

Just another case of overlapping accounts between two general ledgers
which need to be aligned. Another case of expended effort and drag.

More madness
------------

In order to make payments between corporate customers at different
banks, banks also need to have relationships with each other. Be it
between a corresponding bank, a central bank, a reserve bank or a
clearing bank.

Each bank will hold deposits with these other banks to cement the
relationship. Bank A will hold a nostro account to record the banks
money held in a deposit at another bank B. Bank B will hold a vostro
account to record the deposits they have received from bank A. All of
these banks might have many of these types of vostro or nostro accounts
on their respective ledgers and guess what, each bank needs to keep all
these overlapping accounts aligned. More madness, more drag.

Distributed General Ledger
--------------------------

One way of keeping all the overlapping accounts aligned would be to
share all the transactions on a single public ledger and have all the
corporates validate each other's transactions. The obvious disadvantage
of this solution is that everyone can see each other's transactions.

A second way would be to allow each corporate over time to connect their
general ledger with the general ledger of their clients, suppliers and
banks. Thus, creating a private peer to peer network of inter-connected
ledgers. Enter the Distributed General Ledger.

The Distributed General Ledger would allow corporates to record, manage
and execute financial agreements in perfect synchrony with their client,
suppliers and banks, creating a world of frictionless commerce.

Cordite DGL / Tokens requirements summary
-----------------------------------------

Some have asked for the requirements for Tokens in Cordite. This a precis of a large number of requirements captured as project tickets.
Built for a NatWest project but with an eye on new use-cases being discovered.

1. Define a unit level instrument (Token type) that can represent fiat, crypto currencies, reward tokens, audio credits or units of basic tokenised assets - equities, bonds, mortgages
2. Should be able to support aliasing - it multiple ‘instrument’ codes - for example XBT, BTC
3. Aliasing should also support validity date ranges (for example ISINs)
4. Must be able to support micro units and hence exponent should be part of definitiion
5. Corda Parties should be able to create Token types with ‘strong’ issuance such that Tokens are unique by Issuer and Token Name and be universally unique
6. Tokens should be referenced by their unique identifier across nodes without the need for copying the whole token definition
7. Corda Parties must be able to create accounts to store tokens
8. Accounts must be able to support multiple token types
9. Accounts should be uniquely identifiable across all nodes - using node identity plus account name
10. Accounts must support aliases so that they can be identified by existing methods, such as IBAN, Sortcode and Account Number
11. Accounts must support multiple financial structures such as trading book hierarchies and accounting hierarchies so that aggregation of positions and financial position can be calculated
12. Accounts should support multiple owners (e.g. joint accounts, business accounts) and only allow transfers when a quorum of signatures is achieved
13. Accounts should be able to span multiple nodes for resilience and decentralisation
14. Accounts should be able to be owned by non node (i.e. user) parties
15. Tokens must only be issued to accounts
16. Tokens must be able to be transferred between accounts on the same node or between nodes using primary or aliased account identifiers
17. All Token transactions must produce a summary of debits and credits
18. Tokens and Accounts should be searchable by primary and secondary (alias) identifiers
19. We must be able to listen to account changes by specifying single or groups of primary identifiers or tags.
20. Tokens may be issued back-to-back from existing off-ledger assets, in which case it should be possible to immobilise the off-ledger asset thereby ensuring that the underlying assets cannot be 'spent' independantly from the issued token. The Token should be a specialised form a PromissoryNote with immediate or future maturity and partial redemption.


DGL : Token issuance and transfer
---------------------------------

**Georgina**\ 's great-great-great-grandfather (**grandfather**) was
shipwrecked on Kirrin Island. On the **Ship** was a 100 gold coins
(**XAU**). **Georgina** lives with her **Uncle Quentin** at **Kirrin
Cottage**. **Georgina** and her friends - **Julian**, **Dick** and
**Anne** find the gold on Kirrin Island. **Georgina** puts the gold in
her **pocket**. She returns to **Kirrin Cottage** and gives it to
**Uncle Quentin**. **Uncle Quentin** splits the gold between the four
children - **Georgina**, **Julian**, **Dick** and **Anne**. A simple
test of the Cordite DGL functionality. Each actor can run a separate
Cordite node creating a Distributed General Ledger (DGL).

.. mermaid::

    sequenceDiagram 
        Grandfather ->> CorditeNodeA : createAccount(Ship)
        Grandfather ->> CorditeNodeA : createTokenType(XAU)
        Grandfather ->> CorditeNodeA : issueToken(Ship, 100, XAU)
        Georgina ->> CorditeNodeA : createAccount(Pocket)
        Grandfather ->> CorditeNodeA : transferToken(Ship, Pocket@Georgina, 100, XAU) 
        Uncle Quentin ->> CorditeNodeA : createAccount(Kirrin Cottage)
        Georgina ->> CorditeNodeA : transferToken(Pocket, Kirrin Cottage@Uncle Quentin, 100, XAU) 
        Julian ->> CorditeNodeA : createAccount(Pocket)
        Dick ->> CorditeNodeA : createAccount(Pocket) 
        Anne ->> CorditeNodeA : createAccount(Pocket) 
        Julian ->> CorditeNodeA : createAccount(Pocket) 
        Uncle Quentin ->> CorditeNodeA : transferToken(Kirrin Cottage, pocket@Julian, 25, XAU) 
        Uncle Quentin ->> CorditeNodeA : transferToken(Kirrin Cottage, pocket@Dick, 25, XAU) 
        Uncle Quentin ->> CorditeNodeA : transferToken(Kirrin Cottage, pocket@Anne, 25, XAU) 
        Uncle Quentin ->> CorditeNodeA : transferToken(Kirrin Cottage, pocket@Georgina, 25, XAU) 

|

Direct pay: FX transfers
------------------------

**Anne** has moved to the US, and **Julian** wants to send some pocket
money to **Anne**. However, **Julian** only has British pounds (GBP)
while **Anne** needs US dollars (USD). Therefore, Julian sends GBP to a
**FX provider** with instructions of converting the GBP to USD before
sending to **Anne**. The **FX provider** carries out the currency
conversion using the current FX rate, sends USD to **Anne**, and levies
a fee that is deducted from the transaction. **Anne** then receives USD
in her account.

.. mermaid::

    sequenceDiagram 
        Julian ->> FXprovider : Sends GBP 
        Julian ->> FXprovider : Requests GBP to USD Conversion (Cx) 
        FXprovider ->> FXprovider : Converts GBP to USD for Cx 
        FXprovider ->> FXprovider : Deducts a fee 
        FXprovider ->> Anne : Sends USD

|

Multi-currency wallet
---------------------

**Anne** realises that she still needs GBP when she goes back the UK, so
she created a **Multi-Currency Wallet** that can hold both USD and GBP.
Next time, **Julian** can send GBP directly to **Anne**, who can receive
the GBP and store it in her **Wallet**.

.. mermaid::

    sequenceDiagram
         Julian ->> Anne : Sends GBP 
         Anne ->> MultiCurrencyWallet : Stores GBP 


|

Apologies to `Enid
Blyton <https://en.wikipedia.org/wiki/Enid_Blyton>`__, the author of
`The Famous Five
Series <https://en.wikipedia.org/wiki/The_Famous_Five_(novel_series)>`__.
The characters and stories are based on this series.


CLI Demonstration
-----------------

:ref:`Click here for the CLI Demonstration <dglcli>`