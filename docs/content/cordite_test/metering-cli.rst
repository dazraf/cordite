.. _meteringcli:

Metering CLI Demonstration
==========================

.. admonition:: Current node addresses

    -  https://amer-test.cordite.foundation:8080
    -  https://apac-test.cordite.foundation:8080
    -  https://emea-test.cordite.foundation:8080

In this CLI demonstration we will:
- Transfer some tokens between accounts on different nodes
- Look at outstanding invoices on the metering notary
- Pay the metering invoice for the transaction
- Look at the balances of the metering notary, guardian notary and committee node to check that the funds from the invoice payment have been dispersed correctly

.. raw:: html

    <script src="https://asciinema.org/a/sNhfbBr1ZoN9KnFT6oeMgOyuy.js" id="asciicast-sNhfbBr1ZoN9KnFT6oeMgOyuy" async data-autoplay=0 data-t=03:12 data-theme="solarized-light"></script>

Commands used
-------------

+-----------+-----------------------------------------------------------------------------------+
|   Node    | Command                                                                           | 
+===========+===================================================================================+
|   amer    | ``ledger.transferToken("50.00", Token1.descriptor.uri, "Georgina",                |
|           | Dick, "transfer", notaries.corditeMeteringNotary.name)``                          |
+-----------+-----------------------------------------------------------------------------------+
|   emea    | ``ledger.balanceForAccount("Dick")``                                              | 
+-----------+-----------------------------------------------------------------------------------+
|   amer    | ``meterer.listInvoices("ISSUED")``                                                |
+-----------+-----------------------------------------------------------------------------------+
|   amer    | ``transactionId = meterer.listInvoices("ISSUED"[0].meteringInvoiceProperties.me   |
|           | teredTransactionId``                                                              |
+-----------+-----------------------------------------------------------------------------------+
|   amer    | ``meterer.payInvoice({meteredTransactionId:transactionId,from                     |
|           | Account:"Georgina"})``                                                            |
+-----------+-----------------------------------------------------------------------------------+
|  metering | ``ledger.balanceForAccount("metering-notary-account1")``                          |
|  notary   |                                                                                   |
+-----------+-----------------------------------------------------------------------------------+
|  guardian | ``ledger.balanceForAccount(“guardian-notary-account1”)``                          |
|  notary   |                                                                                   |
+-----------+-----------------------------------------------------------------------------------+
| committee | ``ledger.balanceForAccount(dao-foundation-account”)``                             |
+-----------+-----------------------------------------------------------------------------------+
     