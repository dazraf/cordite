<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
## Supported tags and respective Dockerfile links

* `v0.1.6`, `test`, `latest` [*(0.1.6/Dockerfile)*](https://gitlab.com/cordite/cordite/blob/v0.1.6/node/Dockerfile)
* `v0.1.5` [*(0.1.5/Dockerfile)*](https://gitlab.com/cordite/cordite/blob/v0.1.5/node/Dockerfile)

## Quick reference
Documentation: [Cordite Docs](https://docs.cordite.foundation/)
Repository: [Cordite](https://gitlab.com/cordite/cordite)
News is announced on [@We_are_Cordite](https://twitter.com/we_are_cordite)
More information can be found on [Cordite website](https://cordite.info)
We use #cordite channel on [Corda slack](https://slack.corda.net/) 
We informally meet at the [Corda London meetup](https://www.meetup.com/pro/corda/)

## What is Cordite?
Cordite provides decentralised economic and governance services including:

  + decentralised stores and transfers of value allowing new financial instruments to be created inside the existing regulatory framework. eg. tokens, crypto-coins, digital cash, virtual currency, distributed fees, micro-billing  
  + decentralised forms of governance allowing new digital autonomous organisations to be created using existing legal entities eg. digital mutual societies or digital joint stock companies  
  + decentralised consensus in order to remove the need for a central operator, owner or authority. Allowing Cordite to be more resilient, cheaper, agile and private than incumbent market infrastructure  

Cordite is open source, regulatory friendly, enterprise ready and finance grade.  

Cordite is built on [Corda](http://corda.net), a finance grade distributed ledger technology, meeting the highest standards of the banking industry, yet it is applicable to any commercial scenario. The outcome of over two years of intense research and development by over 80 of the world’s largest financial institutions.  

## How to use this image
### Starting up a node
```  
$ docker run -p 8080:8080 -p 10002:10002 cordite/cordite
```
Once the node is running, you will be able to see the REST API for accessing Cordite at `https://localhost:8080/api`

## Configuring a node
### List of Environment Variables

Env Name| Description | Default 
------------- |:-------------:|-------------:|
CORDITE_LEGAL_NAME | The name of the node | O=Cordite-XXX, OU=Cordite, L=London, C=GB
CORDITE_P2P_ADDRESS | The address other nodes will use to speak to your node | localhost:10002
CORDITE_COMPATIBILITY_ZONE_URL | The address of the Network Map Service. | https://network-map-test.cordite.foundation/
CORDITE_KEY_STORE_PASSWORD | Keystore password | cordacadevpass
CORDITE_TRUST_STORE_PASSWORD | Truststore password | trustpass
CORDITE_DB_USER | Username for db | sa
CORDITE_DB_PASS | Password for db | dnpass
CORDITE_BRAID_PORT | Braid port | 8080
CORDITE_DEV_MODE | Start up node in dev mode | true
CORDITE_DETECT_IP | Allow node to auto detect external visible IP | false
CORDITE_TLS_CERT_PATH | Path to TLS certificate | null
CORDITE_TLS_KEY_PATH | Path to TLS Key | null


An example of including environment variables when running the cordite image:
```
$ docker run -p 8080:8080 -p 10002:10002 -e CORDITE_LEGAL_NAME="O=MyCorditeNode, OU=Cordite, L=London, C=GB" cordite/cordite
```
Firewalls need to be opened on port 8080/tcp and 10002/tcp.

### Volume Mount
There are certain files and folders that are needed for a Cordite node. These are as follows

File/Folder| Description 
------------- |:-------------:|
/opt/cordite/node.conf | Configuration file detailing specifics of your node - will be created using env variables if a node.conf is not mounted
/opt/cordite/db | Location of the database for that node - for persistence, use volume mount
/opt/cordite/certificates | Location of the nodes' corda certificates - will be created if no certificates are mounted to node
/opt/cordite/tls-certificates | Location of TLS certificates - will use self-signed certificates if none are mounted to node

## License
View [license information](https://gitlab.com/cordite/cordite/blob/master/LICENSE) for the software contained in this image.

As with all Docker images, these likely also contain other software which may be under other licenses (such as Bash, etc from the base distribution, along with any direct or indirect dependencies of the primary software being contained).

As for any pre-built image usage, it is the image user's responsibility to ensure that any use of this image complies with any relevant licenses for all software contained within.
