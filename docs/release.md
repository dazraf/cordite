<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# Release Process
Below is the release process we have in place for Cordite and some considerations regarding the process we can work through.
It is a work in process and we expect to improve this over time. This is the current standing of the progress; which we can alter as we experience each release.

## Releasing a new version of Cordite

### Create release branch and tag
1. Find the last [release tag](https://gitlab.com/cordite/cordite/tags) 
2. Agree next release tag - see section below on SEMVER - `vX.X.X`
3. Check that the latest [master build is passing](https://gitlab.com/cordite/cordite/pipelines)
4. Update the cordaledger/cordite channel that it is release day and the new release tag
5. run `./release.sh <RELEASE_TAG>` which will create new branch and tag called <RELEASE_TAG>

### Add release notes
1. Check what has merged since last release `git log --merges --first-parent master --pretty=format:"%h %<(15)%ar %s"`
2. Prepare release notes following the [release notes format](https://www.prodpad.com/blog/writing-release-notes/)
3. Update the release notes through the GitLab UI on the [tags](https://gitlab.com/cordite/cordite/tags/) (little pencil icon top right)

### Post release
1. Make sure your new [pipelines pass](https://gitlab.com/cordite/cordite/pipelines)
2. Do **NOT** create a merge request for your branch into master
3. Check the jars have been published to [Maven](https://repo1.maven.org/maven2/io/cordite/). See Maven central section below for further steps.
4. Check [docker hub](https://hub.docker.com/r/cordite/cordite/), under `tags` your new version will be part of the list.
5. Update the [docker hub](https://hub.docker.com/r/cordite/cordite/) description with the addition of the new tag under `Supported tags and respective Dockerfile links`.
6. Check [npmjs](https://www.npmjs.com/package/cordite-cli?activeTab=versions) has been updated with latest version of node client
7. Check [RTD](https://readthedocs.org/projects/cordite/builds/) has been updated with latest version of docs. If not kick a manual build. If this is a major version then go to [version](https://readthedocs.org/projects/cordite/versions/) and change it to a public version.
8. Update the cordaledger/cordite channel with links to the new releases (npm, mvn, rtd, docker) and release notes.
9.  Tweet from `@we_are_cordite` account of new release of cordite

### Maven central steps
1. Log into https://oss.sonatype.org using the password for `devops@cordite.foundation`
2. Click open the “staged repositories” and search for `iocordite`. There should be only one that matches. 
3. Select the package
4. Press `Close` on action toolbar 
5. Hit the `Refresh` button in the Nexus UI and select the “Actions” toolbar. 
6. Keep clicking `Refresh` until all the actions are done and the the repository has status “Closed”
7. Click `Release` to release to Maven central
8. Check Maven central repo - https://repo1.maven.org/maven2/io/cordite/

## Release versioning - SEMVER
For the moment, we are using SEMVER loosely and will become more strict with how we are versioning from release v1.0.0 onwards. For now, we want to understand what out bounderies are and what services we are exposing that our clients will interact with. Over time we will better the versioning we use for cordite. 

Each release will be checked to see what changes are made a which version to release (major, minor, patch) to determine the version number. See here for more info -- https://semver.org/. 

We will be using the release process following steps from Trunk Based development. In summary, a few hours prior to release a new branch is created that acts as the release branch. Given the time before release, there is time to make sure the release branch is stable and prepped to go live. On release day, a tag is created from the release branch.

## Patching
Changes that are critical to a current release are to be done on the release branch. If the change is not specific to that release, the dev making said change must cherry-pick the commit into master. Steps for cherry-picking are below.

## Cherry-Picking
1. Through the gitlab UI, copy the commit SHA you wish to cherry pick
2. Run the following command using your `<SHA>`

```
$ git cherry-pick -n <SHA>
```

You can now commit that change onto your local branch.

## Additional Notes / Considerations
Some topics that need to be considered as we iterate the release process.

- Managing the gradle.properties files over release versions/branches and master - need to discuss and come to consensus on how to approach this aspect of code. Currently on each release we are changing the version of cordite in the file, and then having to manually update it on master to the next release version. Need to find a way to remove this as a step of the release process
- Merging commits/patches between release branch and master - a better way to merge patches/ bug fixes between release branch and master
- Automating the release process to make it more consistent and refined
