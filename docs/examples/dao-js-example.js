/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


const Proxy = require('braid-client').Proxy;

 // set up using test network
let emeaAddress = "https://emea-test.cordite.foundation:8080/api/"
let amerAddress = "https://amer-test.cordite.foundation:8080/api/"

const emea = new Proxy({url: emeaAddress}, onOpenEmea, onClose, onError, {strictSSL: false})
var amer

let saltedDaoName = 'testDao-'+new Date().getTime()
let meteringNotaryName = "O=Cordite Metering Notary, OU=Cordite Foundation, L=London,C=GB"
let emeaNodeName = "OU=Cordite Foundation, O=Cordite EMEA, L=London, C=GB"

var daoKey
var emeaParty
var normalProposalKey

function onOpenEmea() {
    console.log("connected to emea.  conneting to amer...")
    amer = new Proxy({url: amerAddress}, onBothReady, onClose, onError, {strictSSL: false})
}

function onBothReady() {
    console.log("also connected to amer...starting test")

    emea.dao.createDao(saltedDaoName, meteringNotaryName).then(daoState => {
        daoKey = daoState.daoKey
        console.log("emea created dao with name",saltedDaoName,"and key",daoKey)
        return amer.network.getNodeByLegalName(emeaNodeName)
    }).then(emeaNode => {
        emeaParty = emeaNode.legalIdentities[0]
        console.log("amer asking to join dao")
        return amer.dao.createNewMemberProposal(saltedDaoName, emeaParty)
    }).then(proposalState => {
        console.log("proposalKey:",proposalState.proposal.proposalKey)
        console.log("both members already support so we can just propose acceptance")
        return amer.dao.acceptNewMemberProposal(proposalState.proposal.proposalKey, emeaParty)
    }).then(proposal => {
        console.log("proposal state now:", proposal.lifecycleState)
        console.log("dao members now", proposal.members.map(x => x.name).join())
        console.log("now amer proposes to change the membership rules")
        return amer.dao.createProposal("change voting percentage","change the voting percentage of people needed to accept", daoKey)
    }).then(normProposalState => {
        normalProposalKey = normProposalState.proposal.proposalKey
        console.log("new proposal created with key",normalProposalKey)
        console.log("emea decides to support proposal")
        return emea.dao.voteForProposal(normalProposalKey)
    }).then(votedProposalState => {
        console.log("should be two supporters now", votedProposalState.supporters.length)
        console.log("amer proposes to accept")
        return amer.dao.acceptProposal(normalProposalKey)
    }).then(acceptedProposal => {
        console.log("should be accepted",acceptedProposal.lifecycleState)
        console.log("and we are done :-)")
}).catch(error => {
        console.error(error)
    })
}

function onClose() {
    console.log("closed")
}

function onError(err) {
    console.error(err)
}
