#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# Release script
# ./release.sh <tag>


set -e
VERSION=${1:-NOT_GIVEN}

if [ $VERSION= "NOT_GIVEN" ]
then
    echo "tag must be passed as first parameter eg ./release.sh v0.3.3"
    exit 1
fi

# Branch from master (should branch from passed master build?)
git pull origin master
git checkout -b ${VERSION}

# Update version numbers in the code
sed -i.bak 's/\(version=\).*/\1'${VERSION:1}'/g' cordapps/gradle.properties
sed -i.bak 's/\("version"\).*/\1: "'${VERSION:1}'",/g' clients/cli/package.json 

# commit and push branch
git commit -a -m 'Cutting '${VERSION}' branch and updating version number'
git push origin ${VERSION}

# tag and push tag
git tag -a ${VERSION} -m 'Tagging new release of Cordite - '${VERSION}''
git push origin tag ${VERSION}