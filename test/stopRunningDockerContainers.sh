#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

IMAGE_COUNT=$(docker ps -q | wc -l)
if [[ ${IMAGE_COUNT} -eq "0" ]]; then 
    echo no running containers
else 
    echo "cleaning up ${IMAGE_COUNT} running docker images"
    docker stop $(docker ps -q)
fi

IMAGE_COUNT=$(docker ps -aq | wc -l)
if [[ ${IMAGE_COUNT} -eq  "0" ]]; then 
    echo no stopped containers
else
    echo "cleaning up ${IMAGE_COUNT} stopped docker images"
    docker rm $(docker ps -aq)
fi