#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# Same as test:docker with the addition of a network map service (NMS). NMS is pulled as an image from NMS registry
echo "docker-compose test with a network map service (NMS). latest NMS image is pulled as an image from NMS registry"

set -e # stop on error

IMAGE_VERSION=${1:-cordite/cordite:edge}
ENVIRONMENT=${2:-dev}

# # script directory, so we can run from anywhere
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "script directory is '${SCRIPT_DIR}'"

docker-compose -p ${ENVIRONMENT} down

./build_env.sh ${IMAGE_VERSION} ${ENVIRONMENT}

echo "starting the integration tests"
cd ${SCRIPT_DIR}/../cordapps
./gradlew integrationTest --info

echo "stoping docker  compose environment"
cd ../test
docker-compose -p ${ENVIRONMENT} down

