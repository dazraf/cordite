#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# build environment
# usage ./build_env.sh <cordite image tag> <environment>

IMAGE_TAG=${1:-cordite/cordite:edge}
ENVIRONMENT_SLUG=${2:-dev}
MINIMUM=${3:-no}

if [ $MINIMUM = "yes" ]
then
    echo "starting a minimum cordite network"
    declare -a notaries=("bootstrap-notary")
    declare -a database=()
    declare -a nodes=("emea")
    declare -a ports=("8081")
else
    echo "starting a full cordite network"
    declare -a notaries=("bootstrap-notary guardian-notary metering-notary")
    #declare -a databases=("amer-db emea-db apac-db committee-db guardian-db metering-db bootstrap-db")
    declare -a nodes=("apac emea amer committee")
    declare -a ports=("8081 8082 8083 8084 8085 8086 8087")
fi

echo -e "\xE2\x9C\x94 $(date) create environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
set -e

# clean up any old docker-compose
docker-compose -p ${ENVIRONMENT_SLUG} down

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start databases
for DATABASE in $databases 
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${DATABASE}
done
for DATABASE in $databases 
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${DATABASE} | grep -q "database system is ready to accept connections"
  do
    echo -e "waiting for ${DATABASE} to start up and register..."
    sleep 5
  done
done


# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in $notaries
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# Pause Notaries but not before downloading their NodeInfo-* and whitelist.txt
for NOTARY in $notaries
do    
    NODE_NAME=${ENVIRONMENT_SLUG}_${NOTARY}_1
    NODEINFO=$(docker exec ${NODE_NAME} ls | grep nodeInfo-)
    docker cp ${NODE_NAME}:/opt/cordite/${NODEINFO} ${NODEINFO}
    docker cp ${NODE_NAME}:/opt/cordite/whitelist.txt whitelist.txt
    docker exec ${NODE_NAME} rm network-parameters
    docker pause ${NODE_NAME}
done

# Copy Notary NodeInfo-* and whitelist.txt to NMS
NMS_NAME=${ENVIRONMENT_SLUG}_network-map_1
docker cp whitelist.txt ${NMS_NAME}:/mnt/cordite/network-map/db/inputs/whitelist.txt
rm whitelist.txt
echo -e "\xE2\x9C\x94 copied whitelist.txt to ${NMS_NAME}"
for NODEINFO in nodeInfo-*
do
    docker cp ${NODEINFO} ${NMS_NAME}:/mnt/cordite/network-map/db/inputs/validating-notaries/${NODEINFO}
    rm ${NODEINFO}
    echo -e "\xE2\x9C\x94 copied ${NODEINFO} to ${NMS_NAME}"
done

# re-start the notaries
for NOTARY in $notaries
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} restart ${NOTARY}
done


# start regional nodes (and wait for them to be ready)
for NODE in $nodes
do
    IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in $nodes
do
  until IMAGE_TAG=${IMAGE_TAG} docker-compose -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in $ports
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

echo -e "\xE2\x9C\x94 $(date) created environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"