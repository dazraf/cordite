#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# usage: <script> [image name - defaults to cordite/cordite:local]
set -e # stop on error
IMAGE_NAME=${1:-"cordite/cordite:local"}
echo "stopping all containers"
docker stop $(docker ps -q) 2>/dev/null || true
echo "removing all containers"
docker rm $(docker ps -aq) 2>/dev/null || true
echo "starting rebuild of image ${IMAGE_NAME}"
echo "building cordapps"
pushd ../cordapps
./gradlew clean deployNodes buildNode -PCI_COMMIT_REF_NAME=a -PCI_COMMIT_SHA=b -PCI_PIPELINE_ID=c
popd

echo "build cordite image"
pushd ../node
docker rmi ${IMAGE_NAME} || true
docker build --no-cache -t ${IMAGE_NAME} .
popd
