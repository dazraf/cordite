<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->

# Cordite CorDapps


## Pre-Requisites

You will need the following installed on your machine before you can start:

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 
  installed and available on your path (Minimum version: 1.8_131).
* [IntelliJ IDEA](https://www.jetbrains.com/idea/download/) (Minimum version 2017.1)
* git
* Optional: [h2 web console](http://www.h2database.com/html/download.html)
  (download the "platform-independent zip")

For more detailed information, see the
[getting set up](https://docs.corda.net/getting-set-up.html) page on the
Corda docsite.

## Getting Set Up

To get started, clone this repository with:

     git clone https://github.com/corda/cordapp-template-kotlin.git

And change directories to the newly cloned repo:

     cd cordapp-template-kotlin

## Building the CorDapp template:

**Unix:** 

     ./gradlew deployNodes

**Windows:**

     gradlew.bat deployNodes

Note: You'll need to re-run this build step after making any changes to
the template for these to take effect on the node.


### Updating all files with License header

```bash
./gradlew applyLicense
```

## Updating Code Coverage Values

To run the suite of tests for Cordite, run gradle from the Cordite directory with the following command:

    ./gradlew clean test coveralls
  
Two reports will be generated under `cordapp/build/jacoco/jacocoRootReport/`, where you will find `jacocoRootReport.xml` and `html/index.html`, which are the two output report files from the above command.

It's also possible to run JaCoCo reports for each of the subprojects by running:

    ./ gradlew clean test jacocoTestReport

Where a HTML report will be generated in the respective `build/jacocoHtml/` directories for each project with a test suite specified.