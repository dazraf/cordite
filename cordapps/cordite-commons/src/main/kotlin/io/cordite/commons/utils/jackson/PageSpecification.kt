/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.commons.utils.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonMappingException
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.deser.std.StdDeserializer
import com.fasterxml.jackson.databind.ser.std.StdSerializer
import net.corda.core.node.services.vault.PageSpecification


private const val PAGE_NUMBER = "pageNumber"
private const val PAGE_SIZE = "pageSize"


class PageSpecificationSerializer : StdSerializer<PageSpecification>(PageSpecification::class.java) {
  override fun serialize(pageSpecification: PageSpecification, generator: JsonGenerator, serializerProvider: SerializerProvider) {
    generator.writeStartObject()
    try {
      generator.writeNumberField(PAGE_NUMBER, pageSpecification.pageNumber)
      generator.writeNumberField(PAGE_SIZE, pageSpecification.pageSize)
    } finally {
      generator.writeEndObject()
    }
  }
}

class PageSpecificationDeserializer : StdDeserializer<PageSpecification>(PageSpecification::class.java) {
  override fun deserialize(parser: JsonParser, context: DeserializationContext): PageSpecification {
    val node = parser.codec.readTree<JsonNode>(parser)
    checkNode(node, parser)
    return PageSpecification(node[PAGE_NUMBER].intValue(), node[PAGE_SIZE].intValue())
  }

  private fun checkNode(node: JsonNode, parser: JsonParser) {
    if (!node.isObject) throw JsonMappingException.from(parser, "expected a JSON object")
    checkHasIntField(PAGE_NUMBER, node, parser)
    checkHasIntField(PAGE_SIZE, node, parser)
  }

  private fun checkHasIntField(fieldName: String, node: JsonNode, parser: JsonParser) {
    if (!node.has(fieldName)) {
      throw JsonMappingException.from(parser, "missing field: $fieldName")
    }
    if (!node[fieldName].isInt) throw JsonMappingException.from(parser, "expected int field $fieldName")
  }
}
