/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import io.bluebank.braid.corda.BraidConfig
import io.bluebank.braid.corda.rest.RestConfig
import io.bluebank.braid.core.http.HttpServerConfig
import io.cordite.certs.CertsToJksOptionsConverter
import io.cordite.commons.utils.Resources
import io.cordite.commons.utils.jackson.CorditeJacksonInit
import io.cordite.dao.DaoApiImpl
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.metering.api.impl.MeteringServiceImpl
import io.vertx.core.http.HttpServerOptions
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.core.net.JksOptions
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.CordaService
import net.corda.core.serialization.SingletonSerializeAsToken
import net.corda.core.utilities.loggerFor
import java.io.File

const val BRAID_CONFIG_FILENAME = "braid-config.json"
const val BRAID_DISABLED_PORT = -1
private const val BRAID_PORT_FIELD = "port"

@CordaService
class BraidServerCordaService(serviceHub: AppServiceHub) : SingletonSerializeAsToken() {

  companion object {
    const val CORDITE_CERT_PATH_PROPERTY = "cordite.tls.cert.path"
    const val CORDITE_KEY_PATH_PROPERTY = "cordite.tls.key.path"

    private val log = loggerFor<BraidServerCordaService>()

    init {
      CorditeJacksonInit.init()
    }
  }

  private val org = serviceHub.myInfo.legalIdentities.first().name.organisation.replace(" ", "")

  private val config: JsonObject by lazy {
    try {
      JsonObject(Resources.loadResourceAsString(BRAID_CONFIG_FILENAME)).let {
        log.info("found $BRAID_CONFIG_FILENAME")
        it
      }
    } catch (_: Throwable) {
      log.warn("could not find braid config $BRAID_CONFIG_FILENAME")
      JsonObject()
    }
  }

  private val baseConfig: BraidConfig by lazy {

    val json = if (config.containsKey(org)) {
      log.info("found config for org $org")
      config.getJsonObject(org)
    } else {
      log.warn("cannot find braid config for $org")
      JsonObject().put("port", BRAID_DISABLED_PORT)
    }

    val overridePort = getBraidPortFromEnvironment()
    when {
      overridePort != null -> json.put(BRAID_PORT_FIELD, overridePort)
      json.containsKey(BRAID_PORT_FIELD) -> try {
        json.getInteger(BRAID_PORT_FIELD)
      } catch (_: Throwable) {
        log.error("'$BRAID_PORT_FIELD' is not an int. default to $BRAID_DISABLED_PORT")
        json.put(BRAID_PORT_FIELD, BRAID_DISABLED_PORT)
      }
      else -> {
        log.error("no port provided for $org in config nor environment variable. defaulting to $BRAID_DISABLED_PORT ")
        json.put(BRAID_PORT_FIELD, BRAID_DISABLED_PORT)
      }
    }
    Json.decodeValue(json.toString(), BraidConfig::class.java)
        .withHttpServerOptions(createHttpServerOptions())
  }

  init {
    log.info("starting $org braid on port ${baseConfig.port}")
    val ledger = LedgerApiImpl(serviceHub)
    val dao = DaoApiImpl(serviceHub)
    val meterer = MeteringServiceImpl(serviceHub)

    if (baseConfig.port != BRAID_DISABLED_PORT) {
      baseConfig
          .withService("ledger", ledger)
          .withService("dao", dao)
          .withService("meterer", meterer)
          .mountRestEndPoints(ledger)
          .bootstrapBraid(serviceHub)
    } else {
      log.info("no braid configuration nor environment variable for node $org")
      log.info("not starting braid for node $org")
    }
  }

  private fun BraidConfig.mountRestEndPoints(ledger: LedgerApi): BraidConfig {
    return this.withRestConfig(
        RestConfig()
            .withServiceName("cordite")
            .withDescription("REST API for accessing Cordite")
            .withApiPath("/rest")
            .withPaths {
              group("ledger") {
                get("/ledger/all", ledger::listAccounts)
                post("/ledger/all", ledger::listAccountsPaged)
              }
            }
    )
  }

  private fun getBraidPortFromEnvironment(): Int? {
    val property = "braid.$org.port"
    return getProperty(property)?.toInt()
  }

  private fun getProperty(propertyName: String, default: String? = null): String? {
    val property = System.getProperty(propertyName)
    if (property != null) {
      log.info("found property: $property with value $property")
      return property
    }
    val envVariableName = propertyName.replace('.', '_').toUpperCase()

    val env = System.getenv(envVariableName)
    if (env != null) {
      log.info("found env variable: $envVariableName with value $env")
      return env
    }
    log.info("could not find property $propertyName nor environment variable $envVariableName. defaulting to $default")
    return default
  }

  private fun createHttpServerOptions(): HttpServerOptions {
    val cp = getProperty(CORDITE_CERT_PATH_PROPERTY)
    val kp = getProperty(CORDITE_KEY_PATH_PROPERTY)
    if (cp == null || kp == null) {
      log.info("without either cert or key paths not present, defaulting to developer HttpServerOptions")
      return HttpServerConfig.defaultServerOptions()
    }

    if (!File(cp).exists()) {
      log.error("certificate path does not exist $cp")
      return HttpServerConfig.defaultServerOptions()
    }

    if (!File(kp).exists()) {
      log.error("key path does not exists $kp")
      return HttpServerConfig.defaultServerOptions()
    }

    return HttpServerOptions().apply {
      isSsl = true
      keyCertOptions = convertPemFilesToJksOptions(cp, kp)
    }
  }

  private fun convertPemFilesToJksOptions(certificatePath: String, privateKeyPath: String): JksOptions {
    return CertsToJksOptionsConverter(certificatePath, privateKeyPath).createJksOptions()
  }
}


