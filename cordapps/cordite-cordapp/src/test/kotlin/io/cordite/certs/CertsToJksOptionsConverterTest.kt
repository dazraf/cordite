/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.certs

import io.bluebank.braid.corda.router.Routers
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClientOptions
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import org.bouncycastle.asn1.x500.X500Name
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.net.ServerSocket
import java.security.cert.X509Certificate

@RunWith(VertxUnitRunner::class)
class CertsToJksOptionsConverterTest {
  private val vertx = Vertx.vertx()

  @After
  fun after() {
    vertx.close()
  }

  @Test(timeout = Long.MAX_VALUE)
  fun testWithFiles(context: TestContext) {
    `validate that we can load the certificate and key and serve a http request`("src/test/resources/tls.crt", "src/test/resources/tls.key", context)
    `validate that we can load the certificate and key and serve a http request`("src/test/resources/cert.pem", "src/test/resources/private.key", context)
  }

  private fun `validate that we can load the certificate and key and serve a http request`(cert: String, key: String, context: TestContext) {
    val port = ServerSocket(0).use { it.localPort }
    val converter = CertsToJksOptionsConverter(cert, key)
    val keyStore = converter.keyStore
    val cc = keyStore.aliases().asSequence().flatMap { keyStore.getCertificateChain(it).asSequence() }.filter { it != null }.map { it as X509Certificate }.map { X500Name(it.subjectDN.name) }.toList()
    val jksOptions = converter.createJksOptions()
    val router = Routers.create(vertx, port)
    router.get("/").handler { it.response().setChunked(true).end("Hello") }
    val readyWebServer = context.async()

    val server = vertx.createHttpServer(HttpServerOptions()
      .setSsl(true)
      .setKeyStoreOptions(jksOptions))
      .requestHandler(router::accept)
      .listen(port) {
        if (it.failed()) {
          context.fail(it.cause())
        } else {
          readyWebServer.complete()
        }
      }

    readyWebServer.await()

    val operationCompleted = context.async()
    vertx.createHttpClient(HttpClientOptions()
      .setSsl(true)
      .setTrustAll(true)
      .setTrustStoreOptions(jksOptions)
      .setVerifyHost(false)
    ).get(port, "127.0.0.1", "/").connectionHandler {
      val peerChain = it.peerCertificateChain()
      val found = peerChain.all { cert ->
        val name = X500Name(cert.subjectDN.name)
        cc.contains(name)
      }
      context.assertTrue(found, "matching server certificate names to source names")
      server.close(context.asyncAssertSuccess())
      operationCompleted.complete()
    }
      .exceptionHandler { context.fail(it) }
      .handler { }
      .end()
  }
}