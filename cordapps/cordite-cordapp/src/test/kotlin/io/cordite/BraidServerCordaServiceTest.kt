/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import io.bluebank.braid.corda.rest.RestConfig
import io.cordite.dao.DaoApi
import io.cordite.dao.proposal.CreateProposalFlowResponder
import io.cordite.dgl.corda.LedgerApi
import io.cordite.metering.flow.IssueMeteringInvoiceFlow
import io.cordite.test.utils.*
import io.vertx.core.Vertx
import io.vertx.core.http.HttpClientOptions
import io.vertx.ext.unit.TestContext
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(VertxUnitRunner::class)
class BraidServerCordaServiceTest {
  private val braidPortHelper = BraidPortHelper()
  private lateinit var network: MockNetwork
  private lateinit var node1: StartedMockNode
  private lateinit var node2: StartedMockNode
  private lateinit var node3: StartedMockNode
  private val vertx = Vertx.vertx()
  private val httpClient = vertx.createHttpClient(HttpClientOptions()
    .setSsl(true)
    .setTrustAll(true)
    .setVerifyHost(false)
    .setDefaultHost("localhost"))

  @Before
  fun before() {
    braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName)

    network = MockNetwork(listOf(BraidServerCordaServiceTest::class.java.`package`.name))

    node1 = network.createPartyNode(proposerName)
    node2 = network.createPartyNode(newMemberName)
    node3 = network.createPartyNode(anotherMemberName)
    listOf(node1, node2, node3).forEach {
      it.registerInitiatedFlow(CreateProposalFlowResponder::class.java)
      it.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    }
    network.runNetwork()
  }

  @After
  fun after() {
    network.stopNodes()
  }

  @Test
  fun test(context: TestContext) {
    val node1Port = braidPortHelper.portForNode(node1)
    val node2Port = braidPortHelper.portForNode(node2)
    val daoClient = BraidClientHelper.braidClient(node1Port, "dao")
    val ledgerClient = BraidClientHelper.braidClient(node2Port, "ledger")
//    daoClient.bind(DaoApi::class.java)
    ledgerClient.bind(LedgerApi::class.java)
    val async = context.async()
    httpClient.get(
      node1Port,
      "localhost",
      RestConfig.DEFAULT_SWAGGER_PATH
    )
      .handler { result ->
        println(result.statusCode())
        async.complete()
      }
      .exceptionHandler {
        context.fail(it)
      }
      .end()
  }
}
