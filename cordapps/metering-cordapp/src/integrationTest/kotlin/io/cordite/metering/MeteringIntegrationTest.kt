/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering

import io.bluebank.braid.core.async.getOrThrow
import io.cordite.commons.utils.contextLogger
import io.cordite.commons.utils.jackson.CorditeJacksonInit
import io.cordite.dao.core.DaoState
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.api.MeteringInvoiceDetails
import io.cordite.metering.api.MeteringInvoicePayRequest
import io.cordite.metering.api.MeteringInvoicePayRequests
import io.cordite.metering.api.SimpleResult
import io.cordite.metering.contract.MeteringSplitState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.daostate.*
import io.cordite.metering.testutils.*
import io.cordite.test.utils.SimpleStopWatch
import io.vertx.core.Future
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.junit.AfterClass
import org.junit.BeforeClass
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertTrue
import kotlin.test.fail

/**
 * This has now been refactored a bit to be able to take a discriminator, which it expects
 * as the INT_TEST_ENV java property.
 *
 * Values are:
 *
 *  - local - then it runs up the env using mock servers in a single vm
 *  - docker - this assumes you have the env running in docker locally
 *  - [edge|demo|test] - in this case it attempts to hit a live env for a smoke test
 *
 * Teh default is docker.
 *
 * You can run the tests on their own by running:
 *
 *   ./gradlew integrationTest -x test -DINT_TEST_ENV=edge
 *
 * If you want to override the default cordite.foundation domain you can:
 *
 *   ./gradlew integrationTest -x test -DINT_TEST_ENV=edge -DINT_TEST_DOM=cordite.biz --info
 *
 * You can also change the serperator between the evn hostDiscrim and the env using:
 *
 *   -DINT_TEST_SEP=-
 *
 * the -x excludes the tests, the env specified above is obviously edge
 */
class MeteringIntegrationTest {

  companion object {
    private val log = contextLogger()

    //Test Nodes
    private lateinit var daoNode: TestNode
    private lateinit var guardianNotaryNode: TestNode
    private lateinit var meteringNotaryNode: TestNode
    private lateinit var bootstrapNotaryNode: TestNode
    private lateinit var sector1Bank: TestNode
    private lateinit var sector2Bank: TestNode
    private lateinit var sector3Bank: TestNode
    private lateinit var testEnv: TestEnvironment

    //This must be configured in the test metering service and dispersal service configs
    private const val daoName = "Cordite Committee"
    private const val testCurrencyXTS = "XTS"

    //For Metering Funds
    const val sector1BankAccount1 = "sector1-bank-account1"
    const val sector2BankAccount1 = "sector2-bank-account1"
    const val meteringNotaryAccount1 = "metering-notary-account1"
    const val daoHoldingAccount = "dao-holding-account"
    const val daoFoundationAccount = "dao-foundation-account"
    const val guardianNotaryAccount1 = "guardian-notary-account1"

    //For Testing DGL Transfers
    const val daoIssuingAccount = "dao-issuing-account"
    const val sector1BankAccount2 = "sector1-bank-account2"
    const val sector2BankAccount2 = "sector2-bank-account2"
    const val sector3BankAccount2 = "sector3-bank-account2"
    const val daoSmokeTestFund = "dao-smoke-test-fund"

    private lateinit var xtsTokenType: TokenType.State

    //For timings
    private val stopWatch = SimpleStopWatch()

    @BeforeClass
    @JvmStatic
    fun setup() {
      CorditeJacksonInit.init()
      // Note : This test is primarily for running with local docker
      // But you can run it locally using 'setUpLocal'
      // or for the other cordite environments (use will care under strict instructions from
      // a senior member of the cordite team)

      // env options: local, docker (locally), edge, demo, test
      val env = System.getProperty("INT_TEST_ENV", "docker")
      val domain = System.getProperty("INT_TEST_DOM", "cordite.foundation")

      log.info("running integration test against $env")

      if (env == "local") {
        stopWatch.elapseAndRecord({ testEnv = LocalTestEnvironment() }, "Setting up local environment")
      } else {
        stopWatch.elapseAndRecord({ testEnv = RemoteTestEnvironment(env, domain) }, "Setting up remote environment $env")
      }

      sector1Bank = testEnv.node(NodeDetails.amer)
      sector2Bank = testEnv.node(NodeDetails.apac)
      sector3Bank = testEnv.node(NodeDetails.emea)

      daoNode = testEnv.node(NodeDetails.daoNode)

      bootstrapNotaryNode = testEnv.node(NodeDetails.bootstrapNotary)
      guardianNotaryNode = testEnv.node(NodeDetails.guardianNotary)
      meteringNotaryNode = testEnv.node(NodeDetails.meteringNotary)

      NetworkRefresh.runNetwork = testEnv::runNetwork
    }

    @AfterClass
    @JvmStatic
    fun shutdown() {
      stopWatch.elapseAndRecord({ testEnv.shutdown() }, "shutting down")
      stopWatch.logOutTimings()
      stopWatch.clearTimings()
    }
  }

  @Test
  fun `single end to end test - dont care about initial state`() {
    runEndToEndTest()
  }

  @Ignore
  fun `clear down issued states`() {
    payOldInvoices(sector1Bank, sector1BankAccount1)
  }

  @Test
  fun `compare whitelists`() {
    val commWl = daoNode.daoApi.whitelist()

    val commP = commWl["io.cordite.dao.proposal.ProposalContract"]

    val nodes = listOf(guardianNotaryNode, bootstrapNotaryNode, meteringNotaryNode, sector1Bank, sector2Bank, sector3Bank)

    println("dao: $commP")

    nodes.forEach {
      print("${it.party.name} - ")
      val wl = it.daoApi.whitelist()
      val pl = wl["io.cordite.dao.proposal.ProposalContract"]
      if (pl != commP) {
        println("different")
        println(pl)
      } else {
        println("same \\o/")
      }
      assertTrue("${it.party.name} is different to community node whitelist") { pl == commP }
    }
  }

  private fun runEndToEndTest() {

    println("Hello, this test does not care if we have created anything first, it will just carry on adding stuff - and therefore is a nice smoke test for an environment that has been lazy around for a while")

    val dontCareIfTheyAlreadyExist = true

    stopWatch.elapseAndRecord({ createDaoAndMembers(dontCareIfTheyAlreadyExist) }, "creating dao and members")

    stopWatch.elapseAndRecord({ setUpDaoMeteringParameters(dontCareIfTheyAlreadyExist) }, "setting up metering parameters")

    stopWatch.elapseAndRecord({ createAndLoadAccountsForMeteringFundsII(dontCareIfTheyAlreadyExist) }, "creating accounts and loading metering funds")

    stopWatch.elapseAndRecord({ createAccountsAndDoSomeTransfers(dontCareIfTheyAlreadyExist) }, "creating accounts and doing transfers for metered transactions")

    stopWatch.elapseAndRecord({ checkForMeteringInvoicesAndPayThem() }, "waiting for and checking the metering invoices")

    stopWatch.elapseAndRecord({ checkMeteringDispersals() }, "waiting for and checking metering split invoices")

    stopWatch.elapseAndRecord({ checkFinalBalances() }, "checking final balances")

    stopWatch.logOutTimings()
  }

  private fun checkForMeteringInvoicesAndPayThem() {
    checkAndPayMeteringInvoice(testEnv.node(NodeDetails.amer), sector1BankAccount1)
    checkAndPayMeteringInvoice(sector2Bank, sector2BankAccount1)
  }

  private fun checkAndPayMeteringInvoice(testNode: TestNode, account: String) {

    log.info("Checking for Metering Invoices on ${testNode.party.name.organisation}")

    val unpaidMeteringInvoices = waitForUnpaidInvoices(testNode, 100, 500)

    NetworkRefresh.runNetwork()

    //Note: Paying from account with funds from Guardian Notary
    val unpaidMeteringInvoice = unpaidMeteringInvoices.last()

    val payRequest = MeteringInvoicePayRequest(meteredTransactionId = unpaidMeteringInvoice.meteringInvoiceProperties?.meteredTransactionId as String, fromAccount = account)
    val paymeteringInvoiceResult: SimpleResult = NetworkRefresh.runWithRefresh { testNode.meteringService.payInvoice(payRequest) }

    assert(paymeteringInvoiceResult.result)
    NetworkRefresh.runNetwork()
  }

  private fun waitForUnpaidInvoices(testNode: TestNode, retries: Int, interval: Long): List<MeteringInvoiceDetails> {
    var retry = 0

    log.info("finding unpaid invoices")
    while (retry < retries) {

      val issuedMeteringInvoices = testNode.meteringService.listInvoices(MeteringState.ISSUED)
      issuedMeteringInvoices.forEach { log.info("\n\nIssued MeteringInvoices Loaded $it\n\n") }

      log.info("unpaid invoices found: ${issuedMeteringInvoices.size}")

      if (issuedMeteringInvoices.count() > 0) {
        issuedMeteringInvoices.forEach { log.info("\n\nUnpaid MeteringInvoices Loaded $it\n\n") }
        return issuedMeteringInvoices.map { MeteringInvoiceDetails(it.meteringInvoiceProperties, it.invoiceOwner) }
      }
      retry++
      Thread.sleep(interval)
      NetworkRefresh.runNetwork()
    }
    fail("Could not find any unpaid metering invoices for ${testNode.party.name}")
  }

  private fun checkMeteringDispersals() {
    checkMeteringDispersal(meteringNotaryNode, 4, 60, 1000)
    checkMeteringDispersal(guardianNotaryNode, 1, 60, 1000)
    checkMeteringDispersal(daoNode, 5, 60, 1000)
  }

  private fun checkMeteringDispersal(testNode: TestNode, amount: Int, retries: Int, interval: Long) {
    var retry = 0
    var isChecked = false
    while (!isChecked && retry < retries) {
      log.info("Attempt $retry: Checking for Metering dispersal on ${testNode.party.name.organisation}")
      val testNodeSplitDetails = testNode.meteringService.listInvoiceSplits(MeteringSplitState.SPLIT_DISPERSED).getOrThrow()
      if (!isChecked && testNodeSplitDetails != null && testNodeSplitDetails!!.isNotEmpty()) {
        val testNodeMeteringSplitProperties = testNodeSplitDetails!!.last().meteringInvoiceSplitProperties
        assertTrue("Split dispersed for ${testNode.party.name.organisation} is different to the expected amount ${testNodeMeteringSplitProperties!!.amount}") {
          testNodeMeteringSplitProperties!!.amount == amount
        }
        isChecked = true
      }
      retry++
      Thread.sleep(interval)
      NetworkRefresh.runNetwork()
    }
    if (!isChecked) {
      fail("Unable to find Metering split dispersal state on ${testNode.party.name.organisation} within the limit of $retry retries")
    } else {
      log.info("Able to check Metering split dispersal state on ${testNode.party.name.organisation} within the limit of $retry retries")
    }
  }

  private fun checkAndWaitForBalance(testNode: TestNode, accountName: String, expectedQuantity: Long, maxRetries: Int = 60, interval: Long = 1000): Boolean {

    var gotBalance = false
    var retries = 0

    while (!gotBalance && retries < maxRetries) {

      val accountBalance = testNode.ledger.balanceForAccount(accountName).getOrThrow()

      if (expectedQuantity == 0L && accountBalance.count() == 0) {
        gotBalance = true
      } else {
        if (accountBalance.first().quantity == expectedQuantity) gotBalance = true
      }
      Thread.sleep(1000)
      NetworkRefresh.runNetwork()
      retries++
    }
    return gotBalance
  }

  private fun checkFinalBalances() {

    log.info("Checking final balances")
    var gotAllBalances = false
    var retries = 0

    while (!gotAllBalances && retries < 60) {

      val daoHoldingBalance = daoNode.ledger.balanceForAccount(daoHoldingAccount).getOrThrow()
      val daoFoundationAccountBalance = daoNode.ledger.balanceForAccount(daoFoundationAccount).getOrThrow()
      val meteringNotaryBalance = meteringNotaryNode.ledger.balanceForAccount(meteringNotaryAccount1).getOrThrow()
      val guardianNotaryAccountBalance = guardianNotaryNode.ledger.balanceForAccount(guardianNotaryAccount1).getOrThrow()

      val gotDaoFoundationBalance = (daoFoundationAccountBalance.count() > 0) && (daoFoundationAccountBalance.first().quantity == 1000L)
      val gotMeteringNotaryBalance = (meteringNotaryBalance.count() > 0) && (meteringNotaryBalance.first().quantity == 800L)
      val gotGuardianNotaryBalance = (guardianNotaryAccountBalance.count() > 0) && (guardianNotaryAccountBalance.first().quantity >= 200L)
      val gotDaoHoldingBalance = (daoHoldingBalance.count() > 0) && (daoHoldingBalance.first().quantity == 20000L)

      log.info("***** final balances *****")
      log.info("daoHoldingBalance $daoHoldingBalance got expected balance: $gotDaoHoldingBalance ")
      log.info("daoFoundationAccountBalance $daoFoundationAccountBalance got expected balance $gotDaoFoundationBalance")
      log.info("meteringNotaryBalance $meteringNotaryBalance got expected balance $gotMeteringNotaryBalance")
      log.info("guardianNotaryAccountBalance $guardianNotaryAccountBalance got expected balance $gotGuardianNotaryBalance")

      if (gotDaoFoundationBalance
          && gotMeteringNotaryBalance
          && gotGuardianNotaryBalance
          && gotDaoHoldingBalance
      ) {
        gotAllBalances = true
      }
      Thread.sleep(1000)
      NetworkRefresh.runNetwork()
      retries++
    }
    NetworkRefresh.runNetwork()
  }

  private fun createDaoAndMembers(dontCareIfTheyAlreadyExist: Boolean = false): DaoState {

    log.info("Creating Dao Members for $daoName")

    val daoState = DaoTestUtils.createDaoWithName(daoName, daoNode, bootstrapNotaryNode.party.name, dontCareIfTheyAlreadyExist)

    DaoTestUtils.addMemberToDao(guardianNotaryNode, daoNode, daoName, dontCareIfTheyAlreadyExist)
    DaoTestUtils.addMemberToDao(meteringNotaryNode, daoNode, daoName, dontCareIfTheyAlreadyExist, guardianNotaryNode)
    NetworkRefresh.runNetwork()
    return daoState
  }

  private fun createOrCheckForTokenType(meteringNotaryX500Name: CordaX500Name, dontCareIfTheyAlreadyExist: Boolean = false): TokenType.State {

    var xtsTokenType: TokenType.State?

    if (dontCareIfTheyAlreadyExist) {
      xtsTokenType = daoNode.ledger.listTokenTypes().getOrThrow().find { it.descriptor.symbol == testCurrencyXTS }

      if (xtsTokenType != null) {
        return xtsTokenType
      }
    }
    xtsTokenType = daoNode.ledger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()

    return xtsTokenType
  }

  private fun checkOrCreateAccount(testNode: TestNode, bankAccountName: String, notaryName: CordaX500Name, dontCareIfTheyAlreadyExist: Boolean = false): Account.State {

    var bankAccount: Account.State?

    if (dontCareIfTheyAlreadyExist) {
      bankAccount = testNode.ledger.listAccounts().getOrThrow().find { it.address.accountId == bankAccountName }

      if (bankAccount != null) {
        assert(bankAccount.address.accountId == bankAccountName)
        return bankAccount
      }
    }

    bankAccount = NetworkRefresh.runWithRefresh { testNode.ledger.createAccount(bankAccountName, notaryName) }
    assert(bankAccount.address.accountId == bankAccountName)

    NetworkRefresh.runNetwork()

    return bankAccount
  }

  private fun createAndLoadAccountsForMeteringFundsII(dontCareIfTheyAlreadyExist: Boolean = false) {

    log.info("Creating and Loading Accounts for metering")

    //I had a really bad time writing this function - it will look better in cordite the movie staring fuzz as groucho marx
    val meteringNotaryX500Name = meteringNotaryNode.party.name
    val guardianNotaryX500Name = guardianNotaryNode.party.name

    xtsTokenType = createOrCheckForTokenType(guardianNotaryX500Name, dontCareIfTheyAlreadyExist)

    checkOrCreateAccount(sector1Bank, sector1BankAccount1, guardianNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(sector2Bank, sector2BankAccount1, guardianNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(meteringNotaryNode, meteringNotaryAccount1, guardianNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(daoNode, daoHoldingAccount, guardianNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(daoNode, daoFoundationAccount, guardianNotaryX500Name, dontCareIfTheyAlreadyExist)

    NetworkRefresh.runNetwork()

    //Need to create two matching accounts for Guardian Notary, One on Dao, One on the Guardian Notary
    //This is because funds are temporarily stored on the Dao before switching notary to send fees to Guardian notary (with the metering notary notarising)
    checkOrCreateAccount(guardianNotaryNode, guardianNotaryAccount1, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(daoNode, guardianNotaryAccount1, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)

    NetworkRefresh.runNetwork()

    //Move any balances left hanging round to the smoke test fund
    transferBalanceToAnotherAccount(sector1Bank, sector1BankAccount1, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)
    transferBalanceToAnotherAccount(sector2Bank, sector2BankAccount1, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)
    transferBalanceToAnotherAccount(daoNode, daoHoldingAccount, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)
    transferBalanceToAnotherAccount(daoNode, daoFoundationAccount, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)
    transferBalanceToAnotherAccount(meteringNotaryNode, meteringNotaryAccount1, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)
    transferBalanceToAnotherAccount(daoNode, guardianNotaryAccount1, daoNode.party, daoSmokeTestFund, guardianNotaryX500Name)

    // also pay off outstanding invoices...
    payOldInvoices(sector1Bank, sector1BankAccount1)
    payOldInvoices(sector2Bank, sector2BankAccount1)

    //wait for zero balance on DAO holding account
    assert(checkAndWaitForBalance(daoNode, daoHoldingAccount, 0), { "Dao holding account balance should be zero" })

    NetworkRefresh.runNetwork()

    val doaHoldingAccountInitialAmount = "500.00"
    val amountToTransferToSector1Bank = "100.00"
    val amountToTransferToSector2Bank = "200.00"

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.issueToken(daoHoldingAccount,
          doaHoldingAccountInitialAmount
          , testCurrencyXTS, "issuance to Dao",
          guardianNotaryX500Name)
    }

    NetworkRefresh.runWithRefresh {

      daoNode.ledger.transferAccountToAccount(amountToTransferToSector1Bank,
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.party.name}",
        "$sector1BankAccount1@${sector1Bank.party.name}",
        "transfer",
        guardianNotaryX500Name)
    }

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.transferAccountToAccount(amountToTransferToSector2Bank,
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.party.name}",
        "$sector2BankAccount1@${sector2Bank.party.name}",
        "transfer",
        guardianNotaryX500Name)
    }

    NetworkRefresh.runNetwork()

    //Lets check the balances
    val sector1BankBalanceAfter = NetworkRefresh.runWithRefresh { sector1Bank.ledger.balanceForAccount(sector1BankAccount1) }.first().quantity
    val sector2BankBalanceAfter = NetworkRefresh.runWithRefresh { sector2Bank.ledger.balanceForAccount(sector2BankAccount1) }.first().quantity
    val daoRemainingBalanceAfter = NetworkRefresh.runWithRefresh { daoNode.ledger.balanceForAccount(daoHoldingAccount) }.first().quantity

    assert(sector1BankBalanceAfter == 10000L, { "Expected Balance 10000L, but got $sector1BankBalanceAfter" })
    assert(sector2BankBalanceAfter == 20000L, { "Expected Balance 10000L, but got $sector2BankBalanceAfter" })
    assert(daoRemainingBalanceAfter == 20000L, { "Expected Balance 20000L, but got $daoRemainingBalanceAfter" })

    NetworkRefresh.runNetwork()
  }

  private fun payOldInvoices(node: TestNode, nodeAccount: String) {
    val issuedMeteringInvoiceTxIds = node.meteringService.listInvoiceTxIds(MeteringState.ISSUED)
    log.info("found ${issuedMeteringInvoiceTxIds.size} tx ids to pay")

    if (issuedMeteringInvoiceTxIds.isEmpty()) {
      log.info("0 invoices so not paying off...")
      return
    }

// you will need to comment this in if it's not created...can't check in this version of kotlin
//    xtsTokenType = createOrCheckForTokenType(guardianNotaryNode.party.name, true)

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.issueToken(daoHoldingAccount,
          "${issuedMeteringInvoiceTxIds.size * 10}"
          , testCurrencyXTS, "issuance to Dao",
          guardianNotaryNode.party.name)
    }

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.transferAccountToAccount("${issuedMeteringInvoiceTxIds.size * 10}",
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.party.name}",
        "$nodeAccount@${node.party.name}",
        "transfer",
        guardianNotaryNode.party.name)
    }

    NetworkRefresh.runNetwork()

    NetworkRefresh.runWithRefresh { node.meteringService.payInvoices(MeteringInvoicePayRequests(issuedMeteringInvoiceTxIds, nodeAccount)) }
    NetworkRefresh.runNetwork()
  }

  private fun transferBalanceToAnotherAccount(testNode: TestNode, fromAccountName: String, toParty: Party, toAccount: String, notaryName: CordaX500Name) {
    val currentBalance = testNode.ledger.balanceForAccount(fromAccountName).getOrThrow().findLast { it.token == xtsTokenType.descriptor }?.quantity
        ?: 0

    log.info("balance for $fromAccountName@${testNode.party.name} $currentBalance before transfer to $toAccount@${toParty.name}")

    if (currentBalance > 0) {
      NetworkRefresh.runWithRefresh {

        testNode.ledger.transferAccountToAccount((currentBalance / 100).toString(),
          xtsTokenType.descriptor.uri,
          "${WellKnownTagCategories.DGL_ID}:$fromAccountName@${testNode.party.name}",
          "$toAccount@${toParty.name}",
          "TransferBack",
          notaryName)
      }
      NetworkRefresh.runNetwork()
    }
  }

  private fun createAccountsAndDoSomeTransfers(dontCareIfTheyAlreadyExist: Boolean = false) {

    log.info("Creating Accounts for DGL Transfers")

    val meteringNotaryX500Name = meteringNotaryNode.party.name

    checkOrCreateAccount(sector1Bank, sector1BankAccount2, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(sector2Bank, sector2BankAccount2, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(sector3Bank, sector3BankAccount2, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(daoNode, daoIssuingAccount, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)
    checkOrCreateAccount(daoNode, daoSmokeTestFund, meteringNotaryX500Name, dontCareIfTheyAlreadyExist)

    if (dontCareIfTheyAlreadyExist) {
      //The lazy way to clean out funds before re-running the test - and hey the dao gets cash !)
      transferBalanceToAnotherAccount(sector1Bank, sector1BankAccount2, daoNode.party, daoSmokeTestFund, meteringNotaryX500Name)
      transferBalanceToAnotherAccount(sector2Bank, sector2BankAccount2, daoNode.party, daoSmokeTestFund, meteringNotaryX500Name)
      transferBalanceToAnotherAccount(sector3Bank, sector3BankAccount2, daoNode.party, daoSmokeTestFund, meteringNotaryX500Name)
      transferBalanceToAnotherAccount(daoNode, daoIssuingAccount, daoNode.party, daoSmokeTestFund, meteringNotaryX500Name)
    }

    val initialDaoAmount = "500.00"
    val transferToSector1Bank = "400.00"
    val transferToSector2Bank = "200.00"
    val transferToSector3Bank = "100.00"

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.issueToken(daoIssuingAccount,
          initialDaoAmount,
          testCurrencyXTS,
          "issuance toDao",
          meteringNotaryX500Name)
    }

    NetworkRefresh.runWithRefresh {
      daoNode.ledger.transferAccountToAccount(transferToSector1Bank,
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$daoIssuingAccount@${daoNode.party.name}",
        "$sector1BankAccount2@${sector1Bank.party.name}",
        "Dao Transfer to Sector1Bank",
        meteringNotaryX500Name)
    }

    NetworkRefresh.runWithRefresh {
      sector1Bank.ledger.transferAccountToAccount(transferToSector2Bank,
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$sector1BankAccount2@${sector1Bank.party.name}",
        "$sector2BankAccount2@${sector2Bank.party.name}",
        "Sector1Bank transfer to Sector2Bank",
        meteringNotaryX500Name)
    }

    NetworkRefresh.runWithRefresh {
      sector2Bank.ledger.transferAccountToAccount(transferToSector3Bank,
        xtsTokenType.descriptor.uri,
        "${WellKnownTagCategories.DGL_ID}:$sector2BankAccount2@${sector2Bank.party.name}",
        "$sector3BankAccount2@${sector3Bank.party.name}",
        "Sector2Bank transfer to Sector3Bank",
        meteringNotaryX500Name)
    }

    val retries = 10
    val pollInterval = 1000L

    //Lets check the balances
    val sector1BankBalance = NetworkRefresh.runAndRetryOnCollection({ sector1Bank.ledger.balanceForAccount(sector1BankAccount2).getOrThrow() }, retries, pollInterval)
    val sector2BankBalance = NetworkRefresh.runAndRetryOnCollection({ sector2Bank.ledger.balanceForAccount(sector2BankAccount2).getOrThrow() }, retries, pollInterval)
    val sector3BankBalance = NetworkRefresh.runAndRetryOnCollection({ sector3Bank.ledger.balanceForAccount(sector3BankAccount2).getOrThrow() }, retries, pollInterval)
    val daoIssuingAccountBalance = NetworkRefresh.runAndRetryOnCollection({ daoNode.ledger.balanceForAccount(daoIssuingAccount).getOrThrow() }, retries, pollInterval)

    assert(sector1BankBalance.first().quantity == 20000L)
    assert(sector2BankBalance.first().quantity == 10000L)
    assert(sector3BankBalance.first().quantity == 10000L)
    assert(daoIssuingAccountBalance.first().quantity == 10000L)

    NetworkRefresh.runNetwork()
  }

  private fun setUpDaoMeteringParameters(dontCareIfTheyAlreadyExist: Boolean = false) {

    log.info("Setting up metering parameters")

    val daoStates = DaoTestUtils.getDaoWithRetry(daoNode, daoName)

    val daoState = daoStates.first()
    val meteringModelData = checkMeteringDaoStatesAlreadyExist(daoState)

    if (meteringModelData == null || dontCareIfTheyAlreadyExist) {

      val meteringDaoModel = buildMeteringModelData(meteringNotaryNode.party, guardianNotaryNode.party, daoHoldingAccount, daoFoundationAccount, meteringNotaryAccount1, guardianNotaryAccount1)

      val meteringModelProposalState = NetworkRefresh.runWithRefresh { daoNode.daoApi.createModelDataProposal("MeteringParameters", meteringDaoModel, daoState.daoKey) }

      NetworkRefresh.runWithRefresh { guardianNotaryNode.daoApi.voteForProposal(meteringModelProposalState.proposal.key()) }
      NetworkRefresh.runWithRefresh { meteringNotaryNode.daoApi.acceptProposal(meteringModelProposalState.proposal.key()) }
    }
    //Lets get the dao state and check
    checkMeteringDaoStates(daoNode)
    checkMeteringDaoStates(guardianNotaryNode)
    checkMeteringDaoStates(meteringNotaryNode)
  }

  private fun checkMeteringDaoStates(testNode: TestNode) {
    val daoStates = DaoTestUtils.getDaoWithRetry(testNode, daoName, 20)
    val meteringModelData = daoStates.first().get(MeteringModelData::class) as MeteringModelData

    assert(meteringModelData.meteringNotaryMembers.count() == 2)
    assert(meteringModelData.meteringFeeAllocation.daoHoldingAccountId == daoHoldingAccount)
    assert(meteringModelData.meteringFeeAllocation.daoFoundationAccount == daoFoundationAccount)
    assert(meteringModelData.meteringTransactionCost == MeteringTransactionCost(10, TokenType.Descriptor("XTS", 2, daoNode.party.name)))
  }

  private fun checkMeteringDaoStatesAlreadyExist(daoState: DaoState): MeteringModelData? {
    var meteringModelData: MeteringModelData? = null

    val potentialMeteringModel = daoState.get(MeteringModelData::class)

    if (potentialMeteringModel != null) {
      meteringModelData = potentialMeteringModel
      log.info("Metering Model Data Loaded")
      return meteringModelData
    }
    return meteringModelData
  }

  private fun buildMeteringModelData(meteringNotaryParty: Party, guardianNotaryParty: Party, daoHoldingAccount: String, daoFoundationAccount: String, meteringNotaryAccountId: String, guardianNotaryAccountId: String): MeteringModelData {
    val meteringNotaryMember = MeteringNotaryMember(meteringNotaryParty, null, meteringNotaryAccountId, "I am a cool metering notary", MeteringNotaryType.METERER)
    val guardianNotaryMember = MeteringNotaryMember(guardianNotaryParty, null, guardianNotaryAccountId, "I am a guardian of the galaxy", MeteringNotaryType.GUARDIAN)

    val meteringFeeAllocation = MeteringFeeAllocation(daoHoldingAccount, 50, 40, 10, daoFoundationAccount)
    val meteringTransactionCost = MeteringTransactionCost(10, TokenType.Descriptor("XTS", 2, daoNode.party.name))
    val meteringNotaryMembers = mapOf(meteringNotaryMember.notaryParty.name.toString() to meteringNotaryMember, guardianNotaryMember.notaryParty.name.toString() to guardianNotaryMember)

    return MeteringModelData(meteringTransactionCost, meteringNotaryMembers, meteringFeeAllocation)
  }
}

class NetworkRefresh {
  companion object {
    lateinit var runNetwork: () -> Unit

    fun <T> runWithRefresh(call: () -> Future<T>): T {
      val future = call()
      while (!future.isComplete) {
        Thread.sleep(1000)
        runNetwork()
      }
      if (future.failed()) {
        throw RuntimeException(future.cause())
      }
      return future.result()
    }

    inline fun <T> runAndRetryOnCollection(myApiFunc: () -> Collection<T>, retries: Int, interval: Long): Collection<T> {
      var currentRetry = 0
      var found = false
      var result = myApiFunc()

      while (currentRetry < retries && !found) {
        found = result.count() > 0
        currentRetry++
        Thread.sleep(interval)
        runNetwork()
        result = myApiFunc()
      }
      return result
    }
  }
}