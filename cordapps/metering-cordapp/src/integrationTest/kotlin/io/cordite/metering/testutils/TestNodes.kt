/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.bluebank.braid.client.BraidProxyClient
import io.bluebank.braid.corda.services.SimpleNetworkMapService
import io.cordite.commons.utils.contextLogger
import io.cordite.dao.DaoApi
import io.cordite.dao.proposal.SponsorProposalFlowResponder
import io.cordite.dao.proposal.VoteForProposalFlowResponder
import io.cordite.dgl.corda.LedgerApi
import io.cordite.metering.api.MeteringApi
import io.cordite.metering.flow.IssueMeteringInvoiceFlow
import io.cordite.metering.testutils.NodeDetails.*
import io.cordite.test.utils.BraidClientHelper
import io.cordite.test.utils.BraidPortHelper
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.node.services.transactions.SimpleNotaryService
import net.corda.node.services.transactions.ValidatingNotaryService
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import java.net.UnknownHostException

interface TestNode {

  val meteringService: MeteringApi
  val daoApi: DaoApi
  val ledger: LedgerApi
  val networkService: SimpleNetworkMapService
  val party: Party
  fun shutdown() {

  }
}

class RemoteTestNode(port: Int, hostname: String) : TestNode {

  companion object {
    private val log = contextLogger()
    fun tryToConnectToHost(node: NodeDetails, env: String, domain: String, retries: Int = 20, pollInterval: Long = 2000L): TestNode {
      var retry = 0
      val port = node.portFor(env)
      val hostname = node.hostFor(env, domain)
      while (retry < retries) {
        var testNode: TestNode
        try {
          testNode = RemoteTestNode(port, hostname)
          log.info("Succesfully connected to $node ($hostname:$port)")
          return testNode
        } catch (unknownHostException: Exception) {
          log.info("cant connect yet to $hostname:$port ${unknownHostException.message.toString()}")
        }
        retry++
        Thread.sleep(pollInterval)
      }
      throw UnknownHostException("cant connect to host $hostname")
    }
  }

  private val networkMapBraidClient = BraidClientHelper.braidClient(port, "network", hostname)
  override val networkService = networkMapBraidClient.bind(SimpleNetworkMapService::class.java)
  override val party: Party = networkService.myNodeInfo().legalIdentities.first()

  private val daoBraidClient = BraidClientHelper.braidClient(port, "dao", hostname)
  override val daoApi = daoBraidClient.bind(DaoApi::class.java)

  private val meteringBraidClient = BraidClientHelper.braidClient(port, "meterer", hostname)
  override val meteringService = meteringBraidClient.bind(MeteringApi::class.java)

  private val ledgerBraidClient = BraidClientHelper.braidClient(port, "ledger", hostname)
  override val ledger = ledgerBraidClient.bind(LedgerApi::class.java)

  override fun shutdown() {
    networkMapBraidClient.close()
    daoBraidClient.close()
    meteringBraidClient.close()
    ledgerBraidClient.close()
  }

}

class LocalTestNode(node: StartedMockNode, braidPortHelper: BraidPortHelper) : TestNode {

  override val party: Party = node.info.legalIdentities.first()
  override val daoApi: DaoApi
  override val meteringService: MeteringApi
  override val ledger: LedgerApi
  override val networkService: SimpleNetworkMapService

  private val networkMapBraidClient : BraidProxyClient
  private val daoBraidClient : BraidProxyClient
  private val meteringBraidClient : BraidProxyClient
  private val ledgerBraidClient : BraidProxyClient

  init {
    val port = braidPortHelper.portForParty(party)

    networkMapBraidClient = BraidClientHelper.braidClient(port, "network")
    networkService = networkMapBraidClient.bind(SimpleNetworkMapService::class.java)

    daoBraidClient = BraidClientHelper.braidClient(port, "dao")
    daoApi = daoBraidClient.bind(DaoApi::class.java)

    meteringBraidClient = BraidClientHelper.braidClient(port, "meterer")
    meteringService = meteringBraidClient.bind(MeteringApi::class.java)

    ledgerBraidClient = BraidClientHelper.braidClient(port, "ledger")
    ledger = ledgerBraidClient.bind(LedgerApi::class.java)
  }

  override fun shutdown() {
    networkMapBraidClient.close()
    daoBraidClient.close()
    meteringBraidClient.close()
    ledgerBraidClient.close()
  }
}

/*
Host names look like this
host = emea   (or metering-notary or guardian-notary)
environment = edge
domain = cordite.foundation

therefore:

https://emea.edge.cordite.foundation:8080

 */

@Suppress("EnumEntryName")
enum class NodeDetails(private val hostDiscrim: String, private val dockerPort: Int, val partyName: String) {
  emea("emea", 8081, "Cordite EMEA"),
  apac("apac", 8082, "Cordite AMER"),
  amer("amer", 8083, "Cordite APAC"),
  daoNode("committee", 8084, "Cordite Committee"),
  guardianNotary("guardian-notary", 8085, "Cordite Guardian Notary"),
  meteringNotary("metering-notary", 8086, "Cordite Metering Notary"),
  bootstrapNotary("bootstrap-notary", 8087, "Cordite Bootstrap Notary");

  private val separator = System.getProperty("INT_TEST_SEP", "-")

  fun portFor(env: String): Int {
    return if (env == "docker") dockerPort else 8080
  }

  fun hostFor(env: String, domain: String = "cordite.foundation"): String {
    return if (env == "docker") "localhost" else "$hostDiscrim$separator$env.$domain"
  }
}

interface TestEnvironment {
  fun runNetwork()
  fun node(node: NodeDetails): TestNode
  fun shutdown()
}

class RemoteTestEnvironment(private val env: String, private val domain: String): TestEnvironment {

  private val nodeMap = NodeDetails.values().associate { it to RemoteTestNode.tryToConnectToHost(it, env, domain) }

  override fun runNetwork() {
    // not relevant remotely
  }

  override fun node(node: NodeDetails): TestNode {
    return nodeMap[node]!!
  }

  override fun shutdown() {
    nodeMap.values.forEach { it.shutdown() }
  }

}

class LocalTestEnvironment: TestEnvironment {

  private var mockNet: MockNetwork
  private val braidPortHelper = BraidPortHelper()

  private val sector3BankX500Name = CordaX500Name(emea.partyName, "London", "GB")
  private val sector2BankX500Name = CordaX500Name(apac.partyName, "Singapore", "SG")
  private val sector1BankX500Name = CordaX500Name(amer.partyName, "New York City", "US")

  private val meteringNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = meteringNotary.partyName, locality = "London", country = "GB")
  private val guardianNotaryX500Name = CordaX500Name(commonName = ValidatingNotaryService::class.java.name, organisation = guardianNotary.partyName, locality = "London", country = "GB")
  private val bootstrapNotaryX500Name = CordaX500Name(commonName = ValidatingNotaryService::class.java.name, organisation = bootstrapNotary.partyName, locality = "London", country = "GB")

  private val daoX500Name = CordaX500Name(daoNode.partyName, "London", "GB")
  private val nodeMap = mutableMapOf<NodeDetails, TestNode>()

  init {
    braidPortHelper.setSystemPropertiesFor(sector1BankX500Name, sector2BankX500Name, sector3BankX500Name, meteringNotaryX500Name, guardianNotaryX500Name, daoX500Name, bootstrapNotaryX500Name)
    val testPackages = listOf(DaoApi::class.java.`package`.name, "io.cordite")

    mockNet = MockNetwork(
        cordappPackages = testPackages,
        notarySpecs = listOf(MockNetworkNotarySpec(bootstrapNotaryX500Name, true),
                              MockNetworkNotarySpec(meteringNotaryX500Name, true),
                              MockNetworkNotarySpec(guardianNotaryX500Name, false)))

    runNetwork()

    val guardianNotaryLocal = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == guardianNotaryX500Name }
    val bootstrapNotaryLocal = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == bootstrapNotaryX500Name }

    val meteringNotaryLocal = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == meteringNotaryX500Name }
    meteringNotaryLocal.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)

    val sector1BankNodeLocal = mockNet.createPartyNode(legalName = sector1BankX500Name)
    sector1BankNodeLocal.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

    val sector2BankNodeLocal = mockNet.createPartyNode(legalName = sector2BankX500Name)
    sector2BankNodeLocal.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

    val sector3BankNodeLocal = mockNet.createPartyNode(legalName = sector3BankX500Name)
    sector3BankNodeLocal.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

    val daoNodeLocal = mockNet.createPartyNode(legalName = daoX500Name)
    daoNodeLocal.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    daoNodeLocal.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    daoNodeLocal.registerInitiatedFlow(SponsorProposalFlowResponder::class.java)

    runNetwork()

    // TODO: come back and extract all this into an object and a for loop...
    nodeMap[amer] = LocalTestNode(sector1BankNodeLocal, braidPortHelper)
    nodeMap[apac] = LocalTestNode(sector2BankNodeLocal, braidPortHelper)
    nodeMap[emea] = LocalTestNode(sector3BankNodeLocal, braidPortHelper)
    nodeMap[daoNode] = LocalTestNode(daoNodeLocal, braidPortHelper)
    nodeMap[meteringNotary] = LocalTestNode(meteringNotaryLocal, braidPortHelper)
    nodeMap[guardianNotary] = LocalTestNode(guardianNotaryLocal, braidPortHelper)
    nodeMap[bootstrapNotary] = LocalTestNode(bootstrapNotaryLocal, braidPortHelper)

    runNetwork()
  }

  override fun runNetwork() { mockNet.runNetwork() }

  override fun node(node: NodeDetails): TestNode {
    return nodeMap[node]!!
  }

  override fun shutdown() {
    nodeMap.values.forEach { it.shutdown() }
    mockNet.stopNodes()
  }
}


