/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.bluebank.braid.client.BraidProxyClient
import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.metering.api.MeteringApi
import io.cordite.test.utils.BraidClientHelper
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.TempHackedAppServiceHubImpl
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode

class MeteringTestSetup() {

  data class TestEntityDescriptor(
    val baseName: String,
    var x500Name: CordaX500Name,
    var node: StartedMockNode? = null,
    var accountNames: List<String>? = null,
    var requiresBraidClient: Boolean = false,
    var braidClient: MeteringApi? = null) {
    lateinit var party: Party
    lateinit var ledger: LedgerApi
    lateinit var defaultAccountName: String
    val accountAddress by lazy {
      AccountAddress(defaultAccountName, x500Name)
    }
  }

  companion object {

    fun initialiseEntities(mockNet: MockNetwork, braidPortHelper: BraidPortHelper, notary: CordaX500Name, uninitialisedEntities: List<MeteringTestSetup.TestEntityDescriptor>) {
      uninitialisedEntities.forEach { entity ->

        if (entity.node == null) entity.node = mockNet.createPartyNode(legalName = entity.x500Name); mockNet.runNetwork()
        entity.party = entity.node?.info?.legalIdentities?.first()!!
        entity.ledger = LedgerApiImpl(TempHackedAppServiceHubImpl(entity.node!!)) { mockNet.runNetwork() }

        if (entity.accountNames == null) {
          entity.defaultAccountName = entity.baseName + "-account1"
          entity.accountNames = listOf(entity.defaultAccountName)
        } else {
          entity.defaultAccountName = entity.accountNames!!.first()
        }
        for (accountName in entity.accountNames!!) {
          val createAccountResult = entity.ledger.createAccount(accountName, notary).getOrThrow(); mockNet.runNetwork()
          assert(createAccountResult.address.accountId == accountName)
        }

        // TODO: XTS should be created on one node
        entity.ledger.createTokenType("XTS", 2, notary).getOrThrow(); mockNet.runNetwork()

        // Next, set up braid client for the test nodes as required
        if (entity.requiresBraidClient) {
          entity.braidClient = TestNode(entity.node!!, braidPortHelper).meteringService
        }
      }

      mockNet.runNetwork()
    }
  }

  class TestNode(node: StartedMockNode, braidPortHelper: BraidPortHelper) {

    private val braidClient: BraidProxyClient
    val party: Party = node.info.legalIdentities.first()
    val meteringService: MeteringApi

    init {
      val port = braidPortHelper.portForParty(party)
      braidClient = BraidClientHelper.braidClient(port, "meterer")
      meteringService = braidClient.bind(MeteringApi::class.java)
    }

    fun shutdown() {
      braidClient.close()
    }

  }

}
