/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.integration


import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dao.proposal.VoteForProposalFlowResponder
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.api.*
import io.cordite.metering.contract.*
import io.cordite.metering.daostate.MeteringFeeAllocation
import io.cordite.metering.daostate.MeteringFeeAllocator
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.flow.DisperseMeteringInvoiceFundsFlow
import io.cordite.metering.flow.IssueMeteringInvoiceFlow
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import io.cordite.metering.flow.PayMeteringInvoiceFlow
import io.cordite.metering.schema.MeteringInvoiceSplitSchemaV1
import io.cordite.metering.testutils.MeteringDaoSetup
import io.cordite.metering.testutils.MeteringRunAndRetry
import io.cordite.metering.testutils.MeteringTestSetup
import io.cordite.test.utils.BraidPortHelper
import io.cordite.test.utils.run
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.Builder.equal
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.getOrThrow
import net.corda.node.services.transactions.SimpleNotaryService
import net.corda.node.services.transactions.ValidatingNotaryService
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import org.junit.*
import org.slf4j.LoggerFactory
import java.lang.Math.pow
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue


class MeteringApiTest {
  companion object {

    private val log = LoggerFactory.getLogger(MeteringApiTest::class.java)

    private lateinit var mockNet: MockNetwork

    private val braidPortHelper = BraidPortHelper()

    private val daoName = "CorditeDao"

    private val centralBankX500Name = CordaX500Name("CentralBank", "London", "GB")
    private val sector1BankX500Name = CordaX500Name("Sector1Bank", "London", "GB")
    private val sector2BankX500Name = CordaX500Name("Sector2Bank", "London", "GB")
    private val meteringNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "MeteringNotary", locality = "Argleton", country = "GB")
    private val guardianNotaryX500Name = CordaX500Name(commonName = ValidatingNotaryService::class.java.name, organisation = "GuardianNotary", locality = "Argleton", country = "GB")
    private val bootstrapNotaryx500Name = CordaX500Name("Bootstrap", "Wollaston", "GB")
    private val daoX500Name = CordaX500Name("Dao", "London", "GB")

    private val daoHoldingAccount: String = "dao-holding-account"
    private val daoFoundationAccount: String = "dao-foundation-account"
    private val guardianNotaryAccount: String = "guardian-notary-account"

    private var bootstrapNotary = MeteringTestSetup.TestEntityDescriptor("bootstrapNotary", x500Name = bootstrapNotaryx500Name)
    private var meteringNotary = MeteringTestSetup.TestEntityDescriptor("meteringNotary", x500Name = meteringNotaryX500Name, requiresBraidClient = true)
    private var guardianNotary = MeteringTestSetup.TestEntityDescriptor("guardianNotary", x500Name = guardianNotaryX500Name, accountNames = listOf(guardianNotaryAccount), requiresBraidClient = true)
    private var dao = MeteringTestSetup.TestEntityDescriptor("Dao", x500Name = daoX500Name, accountNames = listOf(daoFoundationAccount, daoHoldingAccount, guardianNotaryAccount), requiresBraidClient = true)
    private var sector1Bank = MeteringTestSetup.TestEntityDescriptor("sector1Bank", x500Name = sector1BankX500Name, requiresBraidClient = true)
    private var sector2Bank = MeteringTestSetup.TestEntityDescriptor("sector2Bank", x500Name = sector2BankX500Name, requiresBraidClient = true)
    private var centralBank = MeteringTestSetup.TestEntityDescriptor("centralBank", x500Name = centralBankX500Name)

    private lateinit var meteringModelData: MeteringModelData

    private val testCurrencyXTS = "XTS"
    private var daoTestTokenType: TokenType.Descriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name)
    private var sector1BankTestTokenType: TokenType.Descriptor = TokenType.Descriptor(testCurrencyXTS, 2, sector1BankX500Name)
    private val meteringInvoiceAmount: Int = 10;


    @BeforeClass
    @JvmStatic
    fun setup() {

      braidPortHelper.setSystemPropertiesFor(centralBankX500Name, sector1BankX500Name, sector2BankX500Name, meteringNotaryX500Name, guardianNotaryX500Name, daoX500Name)

      mockNet = MockNetwork(
        cordappPackages = listOf("io.cordite.metering.testutils", "net.corda.core.contracts", "io.cordite.token.contract", "io.cordite.token.contract.schema", "io.cordite.token.flow",
          "io.cordite.metering.contract", "io.cordite.metering.flow", "io.cordite.metering.schema", "io.cordite.dgl", "io.cordite.dao", "io.cordite.test.utils"),
        notarySpecs = listOf(
          MockNetworkNotarySpec(bootstrapNotaryx500Name, false),
          MockNetworkNotarySpec(meteringNotaryX500Name, true),
          MockNetworkNotarySpec(guardianNotaryX500Name, true)))

      mockNet.runNetwork()

      bootstrapNotary.node = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == bootstrapNotaryx500Name }
      guardianNotary.node = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == guardianNotaryX500Name }
      meteringNotary.node = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == meteringNotaryX500Name }

      mockNet.runNetwork()

      // set up names, nodes, ledgers etc for each of the test entities
      MeteringTestSetup.initialiseEntities(mockNet, braidPortHelper, meteringNotaryX500Name,
        listOf(
          bootstrapNotary,
          meteringNotary,
          guardianNotary,
          dao,
          sector1Bank,
          sector2Bank,
          centralBank
        )
      )
      mockNet.runNetwork()

      // register the core needed by these tests with the created test nodes
      meteringNotary.node?.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
      centralBank.node?.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
      sector1Bank.node?.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
      sector2Bank.node?.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
      dao.node?.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)

      mockNet.runNetwork()

      // set up a metering model for the metering notary
      meteringModelData = MeteringDaoSetup.setUpDaoAndMeteringData(
        daoName,
        daoHoldingAccount,
        daoFoundationAccount,
        dao.node!!,
        meteringNotary.node!!,
        guardianNotary.node!!,
        bootstrapNotary.node!!,
        mockNet,
        meteringNotary.defaultAccountName,
        guardianNotary.defaultAccountName)

//      sector2Bank.ledger.createAccount(sector2Bank.defaultAccountName, guardianNotaryX500Name)

      // issue some tokens to Dao
      loadAccount(dao.ledger, daoHoldingAccount, daoTestTokenType, 50000, guardianNotaryX500Name)

      // and give some of them to Sector1Bank to pay metering invoices with
      dao.ledger.transferAccountToAccount("1000",
        testCurrencyXTS,
        "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${dao.node?.info?.legalIdentities?.first()?.name}",
        "${sector1Bank.defaultAccountName}@${sector1Bank.node?.info?.legalIdentities?.first()?.name}",
        "transfer",
        guardianNotaryX500Name).getOrThrow()

      // and issue some of Sector1Banks own tokens to pay other people and create meterable transactions with
      loadAccount(sector1Bank.ledger, sector1Bank.defaultAccountName, sector1BankTestTokenType, 50000, meteringNotaryX500Name)
    }

    @AfterClass
    @JvmStatic
    fun tearDown() {
      mockNet.runNetwork()
      mockNet.stopNodes()
    }


    fun loadAccount(ledger: LedgerApi, accountName: String, tokenType: TokenType.Descriptor, amount: Long, notary: CordaX500Name) {
      ledger.issueToken(accountName, amount.toString(), tokenType.symbol, "issuance", notary).getOrThrow()
      mockNet.runNetwork()
    }


    /**
     * transfer the entire balance issued by <issuer> in <ledger>'s <bankAccount> and sweep it into <beneficiary>
     */
    fun drainAccount(ledger: LedgerApi, issuer: CordaX500Name, accountName: String, beneficiary: AccountAddress, notary: CordaX500Name) {

      var balances = ledger.balanceForAccount(accountName).getOrThrow()

      balances.toList().forEach {

        if (it.token.issuerName == issuer) {

          val exp = it.token.exponent.toDouble()
          val amountToTransfer = (it.quantity / pow(10.0, exp)).toString()

          // sweep the money into the dao account
          ledger.transferAccountToAccount(amountToTransfer,
            it.token.symbol,
            accountName,
            beneficiary.toString(),
            "transfer",
            notary).getOrThrow()

          mockNet.runNetwork()
        }
      }
    }

    fun doTokenTransfer(ledger: LedgerApi, fromAccount: String, toAccount: AccountAddress, tokenType: TokenType.Descriptor, amount: Long, notary: CordaX500Name): SecureHash {

      val txId = ledger.transferAccountToAccount(amount.toString(),
        tokenType.symbol,
        fromAccount,
        toAccount.toString(),
        "transfer",
        notary).getOrThrow()

      mockNet.runNetwork()

      return txId;

    }

    fun printBalances(ledger: LedgerApi, accountName: String): Unit {

      var balances = ledger.balanceForAccount(accountName).getOrThrow()

      balances.toList().forEach {
        log.info("Balance of ${it.token.issuerName} is: ${it.quantity} ${it.token.symbol}")
      }

      mockNet.runNetwork()

    }

  }

  //===  end companion object =======================================================================

  @Before
  fun prepareTest() {

    // put some money into Sector1bank's default account
    loadAccount(sector1Bank.ledger, sector1Bank.defaultAccountName, sector1BankTestTokenType, 1000, meteringNotaryX500Name)
  }

  @After
  fun cleanupTest() {

    // transfer anything left in Sector1Bank's default account to ...the Dao?
    drainAccount(sector1Bank.ledger, sector1Bank.x500Name, sector1Bank.defaultAccountName, dao.accountAddress, meteringNotaryX500Name)
  }


  @Test
  fun `should transfer money between auto created entities`() {

    doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name)
    printBalances(sector1Bank.ledger, sector1Bank.defaultAccountName)
    // to do assert

  }


  @Test
  fun `should list all invoices`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name);

    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();
    val invoices: List<MeteringInvoiceDetails> = sector1Bank.braidClient!!.listInvoices()

    assertEquals(invoices.size, 1, "there should be one invoice in the vault!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId.toString(), "wrong transaction id")

  }

  @Test
  fun `should list invoices by Vault Recorded Date`() {

    val txId1 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId1.toString())

    val txId2 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())

    PayMeteringInvoiceAndCheck(txId2.toString())

    mockNet.runNetwork()

    val invoices = sector1Bank.braidClient!!.listInvoicesByVaultRecordedDate(startDate = Instant.now().minusSeconds(600), endDate = Instant.now()).takeLast(2)
    assertEquals(invoices.size, 2, "there should be two invoices in the vault between this time period!")

    val futureInvoices = sector1Bank.braidClient!!.listInvoicesByVaultRecordedDate(startDate = Instant.now().plus(1, ChronoUnit.DAYS))
    assertEquals(futureInvoices.size, 0, "there should be zero invoice in the vault!")

    val issuedInvoices = sector1Bank.braidClient!!.listInvoicesByVaultRecordedDate(MeteringState.ISSUED, startDate = Instant.now().minusSeconds(600), endDate = Instant.now()).takeLast(1)
    assertEquals(issuedInvoices[0].meteringInvoiceProperties!!.meteredTransactionId, txId1.toString(), "wrong transaction id")

    val paidInvoices = sector1Bank.braidClient!!.listInvoicesByVaultRecordedDate(MeteringState.PAID, startDate = Instant.now().minusSeconds(600), endDate = Instant.now()).takeLast(1)
    assertEquals(paidInvoices[0].meteringInvoiceProperties!!.meteredTransactionId, txId2.toString(), "wrong transaction id")

  }


  @Test
  fun `should list invoices in the requested state`() {

    // issue four invoices
    val txId1 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId1.toString());

    val txId2 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId2.toString());

    val txId3 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId3.toString());

    val txId4 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, TokenType.Descriptor(testCurrencyXTS, 2, sector1Bank.x500Name), 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId4.toString());


    mockNet.runNetwork();

    var invoices: List<MeteringInvoiceDetails> = sector1Bank.braidClient!!.listInvoices(MeteringState.ISSUED).takeLast(4)

    // .. check that we can see all invoices in the ISSUED state
    assertEquals(4, invoices.size, "there should be four ISSUED invoices in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId1.toString(), "wrong transaction id")
    assertEquals(invoices.get(1).meteringInvoiceProperties!!.meteredTransactionId, txId2.toString(), "wrong transaction id")
    assertEquals(invoices.get(2).meteringInvoiceProperties!!.meteredTransactionId, txId3.toString(), "wrong transaction id")
    assertEquals(invoices.get(3).meteringInvoiceProperties!!.meteredTransactionId, txId4.toString(), "wrong transaction id")

    // now pay the third and fourth invoices
    PayMeteringInvoiceAndCheck(txId3.toString())
    PayMeteringInvoiceAndCheck(txId4.toString())

    // .. check that we can list the remaining ISSUED invoices
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.ISSUED).takeLast(2)

    assertEquals(2, invoices.size, "there should be two ISSUED invoices in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId1.toString(), "wrong transaction id")
    assertEquals(invoices.get(1).meteringInvoiceProperties!!.meteredTransactionId, txId2.toString(), "wrong transaction id")

    // .. check that we can list the PAID invoices
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.PAID).takeLast(2)
    assertEquals(2, invoices.size, "there should be three PAID invoices in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId3.toString(), "wrong transaction id")
    assertEquals(invoices.get(1).meteringInvoiceProperties!!.meteredTransactionId, txId4.toString(), "wrong transaction id")

    // now dispute the second invoice
    val disputeRequest = MeteringInvoice(txId2.toString());
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoice(disputeRequest) }
    assertTrue(result.result, "failed to dispute an invoice!")

    // .. check that we can list only the remaining ISSUED invoice
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.ISSUED).takeLast(1)
    assertEquals(1, invoices.size, "there should be one ISSUED invoice in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId1.toString(), "wrong transaction id")

    // .. check that we can list the PAID invoices
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.PAID).takeLast(2)
    assertEquals(2, invoices.size, "there should be two PAID invoices in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId3.toString(), "wrong transaction id")
    assertEquals(invoices.get(1).meteringInvoiceProperties!!.meteredTransactionId, txId4.toString(), "wrong transaction id")

    // .. check that we can list DISPUTED invoice
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.IN_DISPUTE).takeLast(1)
    assertEquals(1, invoices.size, "there should be one IN_DISPUTE invoice in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId2.toString(), "wrong transaction id")


    // disperse fees for the fourth invoice (not available on the API)
    DisperseMeteringInvoiceAndCheck(txId4.toString())


    // .. check that we can list only the remaining ISSUED invoice
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.ISSUED).takeLast(1)
    assertEquals(1, invoices.size, "there should be one ISSUED invoice in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId1.toString(), "wrong transaction id")

    // .. check that we can list the remaining PAID invoices
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.PAID).takeLast(1)
    assertEquals(1, invoices.size, "there should be one PAID invoices in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId3.toString(), "wrong transaction id")

    // .. check that we can list DISPUTED invoice
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.IN_DISPUTE).takeLast(1)
    assertEquals(1, invoices.size, "there should be one IN_DISPUTE invoice in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId2.toString(), "wrong transaction id")

    // .. check that we can list FUNDS_DISPERSED invoice
    invoices = sector1Bank.braidClient!!.listInvoices(MeteringState.FUNDS_DISPERSED).takeLast(1)
    assertEquals(1, invoices.size, "there should be one FUNDS_DISPERSED invoice in the list!")
    assertEquals(invoices.get(0).meteringInvoiceProperties!!.meteredTransactionId, txId4.toString(), "wrong transaction id")
  }


  @Test
  fun `should list all invoice splits`() {

    val fees: MeteringFeeAllocator.FeesToDisperse = allocateFeesFromAmount(meteringInvoiceAmount, meteringModelData.meteringFeeAllocation)

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name);

    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    PayMeteringInvoiceAndCheck(txId.toString());

    // disperse fees for the invoice
    DisperseMeteringInvoiceAndCheck(txId.toString())

    mockNet.runNetwork();

    // check the metering notary allocation:
    val notaryInvoiceSplits: List<MeteringInvoiceSplitDetails> = run(mockNet) { meteringNotary.braidClient!!.listInvoiceSplits() }
    val notarySplitDetails: MeteringInvoiceSplitProperties = notaryInvoiceSplits.last().meteringInvoiceSplitProperties!!;
    assertEquals(notarySplitDetails.meteredTransactionId, txId.toString(), "wrong transaction id")
    assertEquals(fees.meterNotaryAllocation, notarySplitDetails.amount, "wrong amount allocated to metering notary!")


    // check the guardian notary allocation:
    val guardianInvoiceSplits: List<MeteringInvoiceSplitDetails> = run(mockNet) { guardianNotary.braidClient!!.listInvoiceSplits() }
    val guardianSplitDetails: MeteringInvoiceSplitProperties = guardianInvoiceSplits.last().meteringInvoiceSplitProperties!!;
    assertEquals(guardianSplitDetails.meteredTransactionId, txId.toString(), "wrong transaction id")
    assertEquals(fees.guardianNotaryAllocation, guardianSplitDetails.amount, "wrong amount allocated to gusrdian notary!")

    // check the dao allocation:
    val daoInvoiceSplits: List<MeteringInvoiceSplitDetails> = run(mockNet) { dao.braidClient!!.listInvoiceSplits() }
    val daoSplitDetails: MeteringInvoiceSplitProperties = daoInvoiceSplits.last().meteringInvoiceSplitProperties!!;
    assertEquals(daoSplitDetails.meteredTransactionId, txId.toString(), "wrong transaction id")
    assertEquals(fees.daoFoundationAllocation, daoSplitDetails.amount, "wrong amount allocated to dao!")


  }

  @Test
  fun `should pay an invoice`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();

    val payRequest: MeteringInvoicePayRequest = MeteringInvoicePayRequest(meteredTransactionId = txId.toString(), fromAccount = sector1Bank.defaultAccountName);
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.payInvoice(payRequest) }

    // if the payment succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()

    // The invoice should now exist in the vaults of the payer, the metering notary and the DAO - all with metering state = PAID
    // In this state, the invoice will  be owned by the DAO

    dao.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state

      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
    }

    meteringNotary.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ meteringNotary.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
    }


    sector1Bank.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ sector1Bank.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
    }

  }

  @Test
  fun `should pay multiple invoices at once`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();

    val txId2 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId2.toString());

    mockNet.runNetwork();

    val payRequests = MeteringInvoicePayRequests(fromAccount = sector1Bank.defaultAccountName, meteredTransactionIds = listOf(txId.toString(), txId2.toString()))

    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.payInvoices(payRequests) }

    // if the payment succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()

    // Each invoice should now exist in the vaults of the payer, the metering notary and the DAO - all with metering state = PAID
    // In this state, the invoice will  be owned by the DAO

    dao.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.PAID }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in PAID state")
      assertTrue(mis1.single().state.data.owner == dao.party, "Transaction is not owned by DAO")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in PAID state")
      assertTrue(mis2.single().state.data.owner == dao.party, "Transaction is not owned by DAO")
    }

    meteringNotary.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.PAID }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in PAID state")
      assertTrue(mis1.single().state.data.owner == dao.party, "Transaction is not owned by DAO")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in PAID state")
      assertTrue(mis2.single().state.data.owner == dao.party, "Transaction is not owned by DAO")
    }

    sector1Bank.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.PAID }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in PAID state")
      assertTrue(mis1.single().state.data.owner == dao.party, "Transaction is not owned by DAO")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in PAID state")
      assertTrue(mis2.single().state.data.owner == dao.party, "Transaction is not owned by DAO")
    }
  }


  @Test
  fun `should not pay a non-existing invoice`() {

    val transactionId = UniqueIdentifier();

    mockNet.runNetwork();

    val payRequest: MeteringInvoicePayRequest = MeteringInvoicePayRequest(meteredTransactionId = transactionId.toString(), fromAccount = sector1Bank.defaultAccountName);
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.payInvoice(payRequest) }

    // the payment should fail (invoice doesn't exist)
    assertFalse { result.result }
    assertTrue { result.reason!!.contains("List is empty", ignoreCase = true) }


  }

  @Test
  fun `should not pay an already paid invoice`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();

    val payRequest: MeteringInvoicePayRequest = MeteringInvoicePayRequest(meteredTransactionId = txId.toString(), fromAccount = sector1Bank.defaultAccountName);
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.payInvoice(payRequest) }

    // if the payment succeeded, the boolean result with be true
    assertTrue { result.result }

    mockNet.runNetwork()

    // try to pay it again
    val result2: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.payInvoice(payRequest) }

    // The payment should fail (already paid!)
    assertFalse { result2.result }
    assertTrue { result2.reason!!.contains("metering input state must be issued or reissued", ignoreCase = true) }

  }

  @Test
  fun `should dispute an invoice`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name);
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();

    val disputeRequest = MeteringInvoice(txId.toString());
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoice(disputeRequest) }

    // if the operation succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()

    // The invoice should now exist in the vaults of the payer, the metering notary and the DAO - all with metering state = IN_DISPUTE
    // In this state, the invoice will  be owned by the Metering Notary

    dao.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state

      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == meteringNotary.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE)
    }

    meteringNotary.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ meteringNotary.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == meteringNotary.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE)
    }


    sector1Bank.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ sector1Bank.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == txId.toString())
      assert(meteringInvoiceState.data.owner == meteringNotary.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE)
    }


  }


  @Test
  fun `should dispute multiple invoices at once`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString());

    mockNet.runNetwork();

    val txId2 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId2.toString());

    mockNet.runNetwork();

    val disputeRequests = MeteringInvoices(meteredTransactionIds = listOf(txId.toString(), txId2.toString()))

    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoices(disputeRequests) }

    // if the operation succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()

    // Each invoice should now exist in the vaults of the payer, the metering notary and the DAO - all with metering state = PAID
    // In this state, the invoice will  be owned by the DAO

    dao.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }

    meteringNotary.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }

    sector1Bank.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }
  }

  @Test
  fun `should reissue multiple invoices at once`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString())

    mockNet.runNetwork();

    val txId2 = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())

    mockNet.runNetwork();

    val disputeRequests = MeteringInvoices(meteredTransactionIds = listOf(txId.toString(), txId2.toString()))

    var result = run(mockNet) { sector1Bank.braidClient!!.disputeInvoices(disputeRequests) }

    // if the operation succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()


    val reissueRequest1 = MeteringInvoiceReissueRequest(meteredTransactionId = txId.toString(), tokenType = sector1BankTestTokenType, amount = 90)
    val reissueRequest2 = MeteringInvoiceReissueRequest(meteredTransactionId = txId2.toString(), tokenType = sector1BankTestTokenType, amount = 90)

    val reissueRequests = MeteringInvoiceReissueRequests(reissueRequests = listOf(reissueRequest1, reissueRequest2))

    result = run(mockNet) { sector1Bank.braidClient!!.reissueInvoices(reissueRequests) }

    // if the operation succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()


    // Each invoice should now exist in the vaults of the payer, the metering notary and the DAO - all with metering state = PAID
    // In this state, the invoice will  be owned by the DAO

    dao.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }

    meteringNotary.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }

    sector1Bank.node!!.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node!!.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis1.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in IN_DISPUTE state")
      assertTrue(mis2.single().state.data.owner == meteringNotary.party, "Transaction is not owned by metering notary")
    }
  }


  @Test
  fun `should fail to dispute a non-existing invoice`() {

    val txId = UniqueIdentifier()

    mockNet.runNetwork();

    val disputeRequest = MeteringInvoice(txId.toString());
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoice(disputeRequest) }

    assertFalse { result.result }
    assertTrue { result.reason!!.contains("List is empty", ignoreCase = true) }

  }

  @Test
  fun `should fail to dispute an invoice that's already in dispute`() {

    val txId = doTokenTransfer(sector1Bank.ledger, sector1Bank.defaultAccountName, sector2Bank.accountAddress, sector1BankTestTokenType, 100, meteringNotaryX500Name)
    IssueMeteringInvoiceAndCheck(transactionId = txId.toString())

    mockNet.runNetwork()

    val disputeRequest = MeteringInvoice(txId.toString());
    val result: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoice(disputeRequest) }

    // if the operation succeeded, the boolean result will be true
    assertTrue { result.result }

    mockNet.runNetwork()

    val result2: SimpleResult = run(mockNet) { sector1Bank.braidClient!!.disputeInvoice(disputeRequest) }

    // The operation should fail (already in dispute!)
    assertFalse { result2.result }
    assertTrue { result2.reason!!.contains("metering input state must be issued or reissued", ignoreCase = true) }

  }


  private fun IssueMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) {
    return IssueMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun IssueMeteringInvoiceAndCheck(transactionId: String, amount: Int = 10, invoicedParty: Party = sector1Bank.party, invoicingNotary: Party = meteringNotary.party) {
    val issueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
      meteredTransactionId = transactionId.toString(),
      tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, dao.x500Name),
      amount = amount,
      payAccountId = daoHoldingAccount,
      daoParty = dao.party,
      invoicingNotary = invoicingNotary,
      meteringPolicingNotary = guardianNotary.party,
      invoicedParty = invoicedParty)

    val issueMeteringInvoiceFlow = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
    val issueMeteringInvoiceFlowFuture = meteringNotary.node?.startFlow(issueMeteringInvoiceFlow)

    mockNet.runNetwork()

    val issueMeteringInvoiceFlowResult = issueMeteringInvoiceFlowFuture?.getOrThrow()!!
    println("result: " + (issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).toString())
    println("result: " + (issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull())
    assert(((issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull().toString() == invoicedParty.name.toString()))

    mockNet.runNetwork()

    meteringNotary.node?.transaction {

      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ meteringNotary.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)

      val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state
      assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(originalMeteringInvoice.data.owner == invoicedParty)
    }

    sector1Bank.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ sector1Bank.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state

      assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(originalMeteringInvoice.data.owner == sector1Bank.party)
    }

  }

  private fun IssueMeteringInvoice(transactionId: String, amount: Int = 10, invoicedParty: MeteringTestSetup.TestEntityDescriptor,
                                   invoicingNotary: MeteringTestSetup.TestEntityDescriptor,
                                   dao: MeteringTestSetup.TestEntityDescriptor) {

    val issueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
      meteredTransactionId = transactionId,
      tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, dao.x500Name),
      amount = amount,
      payAccountId = daoHoldingAccount,
      daoParty = dao.party,
      invoicingNotary = invoicingNotary.party,
      meteringPolicingNotary = guardianNotary.party,
      invoicedParty = invoicedParty.party)


    val issueMeteringInvoiceFlow = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
    val issueMeteringInvoiceFlowFuture = invoicingNotary.node?.startFlow(issueMeteringInvoiceFlow)

    mockNet.runNetwork()

    val issueMeteringInvoiceFlowResult = issueMeteringInvoiceFlowFuture?.getOrThrow()!!
    assert(((issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull().toString() == invoicedParty.x500Name.toString()))

    mockNet.runNetwork()

    invoicingNotary.node?.transaction {

      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ invoicingNotary.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)

      val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state
      assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(originalMeteringInvoice.data.owner == invoicedParty.party)
    }

    invoicedParty.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ invoicedParty.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state

      assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(originalMeteringInvoice.data.owner == invoicedParty.party)
    }
  }


  private fun PayMeteringInvoiceAndCheck(transactionId: UniqueIdentifier): Unit {
    return PayMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun PayMeteringInvoiceAndCheck(transactionId: String): Unit {

    val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(transactionId, sector1Bank.defaultAccountName)
    val payMeteringInvoiceFlow = PayMeteringInvoiceFlow.MeteringInvoicePayer(payMeteringInvoiceRequest = payMeteringInvoiceRequest)
    val payMeteringInvoiceFlowFuture = sector1Bank.node?.startFlow(payMeteringInvoiceFlow)!!

    mockNet.runNetwork()

    payMeteringInvoiceFlowFuture.getOrThrow()

    mockNet.runNetwork()

    dao.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state

      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
    }

    meteringNotary.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ meteringNotary.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
    }
  }

  private fun DisperseMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) {
    return DisperseMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun DisperseMeteringInvoiceAndCheck(transactionId: String) {

    val disperseMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(transactionId)
    val disperseMeteringInvoiceFlow = DisperseMeteringInvoiceFundsFlow.MeteringInvoiceFundDisperser(disperseFundsForMeteringInvoiceRequest = disperseMeteringInvoiceRequest, meteringModelData = meteringModelData)
    val disperseMeteringInvoiceFlowFuture = dao.node?.startFlow(disperseMeteringInvoiceFlow)!!

    mockNet.runNetwork()

    disperseMeteringInvoiceFlowFuture.getOrThrow()

    mockNet.runNetwork()

    dao.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ dao.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state

      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED)
    }

    meteringNotary.node?.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = MeteringRunAndRetry.runAndRetry({ meteringNotary.node?.services?.vaultService?.queryBy(criteria)!! }, 10, 500)
      val meteringInvoiceState = meteringInvoiceStatesPage.states.last().state
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId)
      assert(meteringInvoiceState.data.owner == dao.party)
      assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED)
    }
  }


  private fun checkInvoiceFeesSplitStateOK(nodeToCheck: StartedMockNode, transactionId: String): Unit {
    val expression = MeteringInvoiceSplitSchemaV1.PersistentMeteringInvoiceSplit::meteringSplitState.equal(MeteringSplitState.SPLIT_DISPERSED.name)
    val customCriteria = QueryCriteria.VaultCustomQueryCriteria(expression)
    val generalCriteria = QueryCriteria.VaultQueryCriteria(Vault.StateStatus.ALL)
    val fullCriteria = generalCriteria.and(customCriteria)

    nodeToCheck.transaction {
      val meteringInvoiceSplitStatesPage = MeteringRunAndRetry.runAndRetryWithMockNetworkRefresh({ nodeToCheck.services.vaultService.queryBy<MeteringInvoiceSplitState>(fullCriteria) }, 20, 1000, mockNet)
      val meteringInvoiceSplitState = meteringInvoiceSplitStatesPage.states.last().state

      assert(meteringInvoiceSplitState.data.meteringInvoiceSplitProperties.meteredTransactionId == transactionId)
      assert(meteringInvoiceSplitState.data.meteringInvoiceSplitProperties.meteringSplitState.name == MeteringSplitState.SPLIT_DISPERSED.name)
    }

  }


  private fun allocateFeesFromAmount(amount: Int, meteringFeeAllocation: MeteringFeeAllocation): MeteringFeeAllocator.FeesToDisperse {

    val totalAllocationDivisor = meteringFeeAllocation.daoFoundationAllocation + meteringFeeAllocation.guardianNotaryAllocation + meteringFeeAllocation.meterNotaryAllocation

    val daoFoundationAllocation = calcIntegerAllocation(amount, meteringFeeAllocation.daoFoundationAllocation, totalAllocationDivisor)
    val meteringNotaryAllocation = calcIntegerAllocation(amount, meteringFeeAllocation.meterNotaryAllocation, totalAllocationDivisor)
    val guardianNotaryAllocation = calcIntegerAllocation(amount, meteringFeeAllocation.guardianNotaryAllocation, totalAllocationDivisor)

    val totalAllocated = daoFoundationAllocation + meteringNotaryAllocation + guardianNotaryAllocation
    val unallocated = amount - totalAllocated
    val adjustedMeteringAllocation = meteringNotaryAllocation + unallocated

    return MeteringFeeAllocator.FeesToDisperse(daoFoundationAllocation, adjustedMeteringAllocation, guardianNotaryAllocation)
  }

  //Kept this calculation separate as we may change it later
  private fun calcIntegerAllocation(amount: Int, percentAllocation: Int, totalAllocationDivisor: Int): Int {
    if (amount == 0 || percentAllocation == 0 || totalAllocationDivisor == 0) return 0
    return (amount * percentAllocation).div(totalAllocationDivisor)
  }

  private fun runAndRetry(myQuery: () -> Vault.Page<MeteringInvoiceState>, retries: Int, interval: Long): Vault.Page<MeteringInvoiceState> {
    var currentRetry = 0
    var found = false
    var result = myQuery()

    while (currentRetry < retries && !found) {
      found = result.states.count() > 0
      currentRetry++
      Thread.sleep(interval)
      result = myQuery()
    }
    return result
  }

}
