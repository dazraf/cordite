/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.testutils

import io.cordite.metering.contract.MeteringInvoiceState
import net.corda.core.contracts.ContractState
import net.corda.core.node.services.Vault
import net.corda.testing.node.MockNetwork

class MeteringRunAndRetry {
  companion object {
    fun runAndRetry(myQuery: () -> Vault.Page<MeteringInvoiceState>, retries: Int, interval: Long): Vault.Page<MeteringInvoiceState> {
      var currentRetry = 0
      var found = false
      var result = myQuery()

      while (currentRetry < retries && !found) {
        found = result.states.count() > 0
        currentRetry++
        Thread.sleep(interval)
        result = myQuery()
      }
      return result
    }

    inline fun <reified T : ContractState> runAndRetryWithMockNetworkRefresh(myQuery: () -> Vault.Page<T>, retries : Int, interval: Long, mockNetwork: MockNetwork): Vault.Page<T> {
      var currentRetry = 0
      var found = false
      var result = myQuery()

      while (currentRetry < retries && !found) {
        found = result.states.count() > 0
        currentRetry++
        mockNetwork.runNetwork()
        Thread.sleep(interval)
        result = myQuery()
      }
      return result
    }
  }
}