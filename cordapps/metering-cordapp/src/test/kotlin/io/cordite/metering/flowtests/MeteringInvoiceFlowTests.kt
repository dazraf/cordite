/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flowtests

import io.bluebank.braid.core.async.getOrThrow
import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.dao.proposal.VoteForProposalFlowResponder
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.impl.LedgerApiImpl
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.contract.MeteringInvoiceState
import io.cordite.metering.contract.MeteringState
import io.cordite.metering.flow.*
import io.cordite.metering.testutils.MeteringDaoSetup.Companion.buildMeteringModelData
import io.cordite.metering.testutils.MeteringRunAndRetry.Companion.runAndRetry
import io.cordite.test.utils.TempHackedAppServiceHubImpl
import net.corda.core.contracts.Amount
import net.corda.core.contracts.ContractState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.utilities.getOrThrow
import net.corda.node.services.transactions.SimpleNotaryService
import net.corda.node.services.transactions.ValidatingNotaryService
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockNetworkNotarySpec
import net.corda.testing.node.StartedMockNode
import org.hibernate.exception.ConstraintViolationException
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.sql.ResultSet
import kotlin.test.assertFails
import kotlin.test.assertFalse
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class MeteringInvoiceFlowTests {

  private lateinit var mockNet: MockNetwork
  private lateinit var bootstrapNotary: StartedMockNode
  private lateinit var meteringNotary: StartedMockNode
  private lateinit var guardianNotary: StartedMockNode
  private lateinit var centralBankNode: StartedMockNode
  private lateinit var sector1BankNode: StartedMockNode
  private lateinit var sector2BankNode: StartedMockNode
  private lateinit var daoNode: StartedMockNode
  private lateinit var centralBank: Party
  private lateinit var sector1Bank: Party
  private lateinit var sector2Bank: Party
  private lateinit var meteringNotaryParty: Party
  private lateinit var guardianNotaryParty: Party
  private lateinit var daoNodeParty: Party
  private lateinit var bootstrapNotaryParty: Party


  private lateinit var  centralBankNodeLedger : LedgerApi
  private lateinit var  sector1BankNodeLedger : LedgerApi
  private lateinit var  sector2BankNodeLedger : LedgerApi
  private lateinit var  meteringNotaryLedger : LedgerApi
  private lateinit var  daoNodeLedger : LedgerApi
  private lateinit var  guardianNotaryLedger : LedgerApi

  private lateinit var centralBankAccountResult: Account.State
  private lateinit var sector1BankAccountResult: Account.State
  private lateinit var sector2BankAccountResult: Account.State
  private lateinit var meteringNotaryAccountResult: Account.State
  private lateinit var daoHoldingAccountResult: Account.State
  private lateinit var daoFoundationAccountResult: Account.State
  private lateinit var guardianNotaryAccountResult: Account.State
  private lateinit var guardianNotaryAccountOnDaoResult : Account.State


  private val centralBankX500Name = CordaX500Name("CentralBank", "London", "GB")
  private val sector1BankX500Name = CordaX500Name("Sector1Bank", "London", "GB")
  private val sector2BankX500Name = CordaX500Name("Sector2Bank", "London", "GB")

  private val meteringNotaryX500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "MeteringNotary", locality = "Argleton", country = "GB")
  private val guardianNotaryX500Name = CordaX500Name(commonName = ValidatingNotaryService::class.java.name, organisation = "GuardianNotary",locality =  "Argleton", country = "GB")
  private val bootstrapNotaryx500Name = CordaX500Name(commonName = SimpleNotaryService::class.java.name, organisation = "BootstrapNotary", locality = "Argleton", country = "GB")

  private val daoX500Name = CordaX500Name("Dao", "London", "GB")

  private val daoName="CorditeDao"

  private val testCurrencyXTS = "XTS"
  private val centralBankAccount1 = "central-bank-account1"
  private val sector1BankAccount1 = "sector1-bank-account1"
  private val sector2BankAccount1 = "sector2-bank-account1"
  private val meteringNotaryAccount1 = "metering-notary-account1"
  private val daoHoldingAccount = "dao-holding-account"
  private val daoFoundationAccount = "dao-foundation-account"
  private val guardianNotaryAccount1 = "guardian-notary-account1"

  @Before
  fun before() {

    /* WARNING !! - The list of cordapp packages here has been carefully handpicked and selected so that the Metering Service is not loaded,
    this allows for more isolated and predictable flow testing
    The metering service is tested (unsuprisingly) in the MeteringServiceTests
    In conclusion, if you just put 'io.cordite' in the packages below you'll undermine the whole philosophy of this test - just saying!
    */
    mockNet = MockNetwork(
        cordappPackages = listOf("net.corda.core.contracts", "io.cordite.token.contract", "io.cordite.token.contract.schema", "io.cordite.token.flow",
            "io.cordite.metering.contract", "io.cordite.metering.flow", "io.cordite.metering.schema", "io.cordite.dgl","io.cordite.dao","io.cordite.test.utils"),
        notarySpecs = listOf(MockNetworkNotarySpec(bootstrapNotaryx500Name, false),
            MockNetworkNotarySpec(meteringNotaryX500Name, true),
            MockNetworkNotarySpec(guardianNotaryX500Name, true)))

    guardianNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == guardianNotaryX500Name }
    guardianNotaryParty = guardianNotary.info.legalIdentities.first()

    bootstrapNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == bootstrapNotaryx500Name }
    bootstrapNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    bootstrapNotaryParty = bootstrapNotary.info.legalIdentities.first()

    meteringNotary = mockNet.notaryNodes.first { it.info.legalIdentities.first().name == meteringNotaryX500Name }
    meteringNotary.registerInitiatedFlow(VoteForProposalFlowResponder::class.java)
    meteringNotaryParty = meteringNotary.info.legalIdentities.first()

    centralBankNode = mockNet.createPartyNode(legalName = centralBankX500Name)
    centralBankNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    centralBank = centralBankNode.info.legalIdentities.first()

    sector1BankNode = mockNet.createPartyNode(legalName = sector1BankX500Name)
    sector1BankNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    sector1Bank = sector1BankNode.info.legalIdentities.first()

    sector2BankNode = mockNet.createPartyNode(legalName = sector2BankX500Name)
    sector2BankNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    sector2Bank = sector2BankNode.info.legalIdentities.first()

    daoNode = mockNet.createPartyNode(legalName = daoX500Name)
    daoNode.registerInitiatedFlow(IssueMeteringInvoiceFlow.MeteringInvoiceReceiver::class.java)
    daoNodeParty = daoNode.info.legalIdentities.first()

    centralBankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(centralBankNode), { mockNet.runNetwork() })
    sector1BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector1BankNode), { mockNet.runNetwork() })
    sector2BankNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(sector2BankNode), { mockNet.runNetwork() })
    meteringNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(meteringNotary), { mockNet.runNetwork() })
    daoNodeLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(daoNode), { mockNet.runNetwork() })
    guardianNotaryLedger = LedgerApiImpl(TempHackedAppServiceHubImpl(guardianNotary), { mockNet.runNetwork() })

    centralBankAccountResult = centralBankNodeLedger.createAccount(centralBankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(centralBankAccountResult.address.accountId == centralBankAccount1)
    sector1BankAccountResult = sector1BankNodeLedger.createAccount(sector1BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sector1BankAccountResult.address.accountId == sector1BankAccount1)
    sector2BankAccountResult = sector2BankNodeLedger.createAccount(sector2BankAccount1, meteringNotaryX500Name).getOrThrow()
    assert(sector2BankAccountResult.address.accountId == sector2BankAccount1)
    meteringNotaryAccountResult = meteringNotaryLedger.createAccount(meteringNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(meteringNotaryAccountResult.address.accountId == meteringNotaryAccount1)
    daoHoldingAccountResult = daoNodeLedger.createAccount(daoHoldingAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoHoldingAccountResult.address.accountId == daoHoldingAccount)
    daoFoundationAccountResult = daoNodeLedger.createAccount(daoFoundationAccount, meteringNotaryX500Name).getOrThrow()
    assert(daoFoundationAccountResult.address.accountId == daoFoundationAccount)
    guardianNotaryAccountResult = guardianNotaryLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryAccountResult.address.accountId == guardianNotaryAccount1)
    guardianNotaryAccountOnDaoResult = daoNodeLedger.createAccount(guardianNotaryAccount1, meteringNotaryX500Name).getOrThrow()
    assert(guardianNotaryAccountOnDaoResult.address.accountId == guardianNotaryAccount1)

    daoNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    sector1BankNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()
    sector1BankNodeLedger.createTokenType("ABC", 2, bootstrapNotaryx500Name).getOrThrow()
    sector2BankNodeLedger.createTokenType(testCurrencyXTS, 2, meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    createDaoTokensAndLoadAccounts()  // issues 500 in DAO and transfers 100 to sector1Bank

  }




  @Test
  fun `Should accept a valid metering invoice `() {
    // will test that the issue invoice flow accepts the invoice as valid based on the the fact that it recognises the original transaction

    val txId = doTokenTransfer();

    val result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())

    assertTrue (result, "invoice was rejected!")

  }

  @Test
  fun `we can issue multiple metering invoices in one transaction`() {

    // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // 2. and spend them over 2 transfers, having the metering notary notarise the transactions
    val txId1 = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("40",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    val issueMeteringInvoiceRequest1 = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
        meteredTransactionId = txId1.toString(),
        tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name),
        amount = 10,
        payAccountId = "paymebackhere",
        daoParty = daoNodeParty,
        invoicingNotary = meteringNotaryParty,
        meteringPolicingNotary = guardianNotaryParty,
        invoicedParty = sector1Bank)

    val issueMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
        meteredTransactionId = txId2.toString(),
        tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name),
        amount = 10,
        payAccountId = "paymebackhere",
        daoParty = daoNodeParty,
        invoicingNotary = meteringNotaryParty,
        meteringPolicingNotary = guardianNotaryParty,
        invoicedParty = sector1Bank)

    val issueMeteringInvoicesFlow = IssueMeteringInvoicesFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequests = listOf(issueMeteringInvoiceRequest1, issueMeteringInvoiceRequest2))
    val issueMeteringInvoiceFlowFuture = meteringNotary.startFlow(issueMeteringInvoicesFlow)

    mockNet.runNetwork()

    val issueMeteringInvoiceFlowResult = issueMeteringInvoiceFlowFuture.getOrThrow()
    assert(((issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull().toString() == sector1Bank.name.toString()))

    mockNet.runNetwork()

    meteringNotary.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.ISSUED }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId1.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in ISSUED state")
      assertTrue(mis1.single().state.data.owner == sector1Bank, "Transaction is not owned by invoiced party")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in ISSUED state")
      assertTrue(mis2.single().state.data.owner == sector1Bank, "Transaction is not owned by invoiced party")
     }

    sector1BankNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.ISSUED }

      val mis1 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId1.toString() }
      assertNotNull(mis1.single(), "Transaction was not found in ISSUED state")
      assertTrue(mis1.single().state.data.owner == sector1Bank, "Transaction is not owned by invoiced party")

      val mis2 = meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }
      assertNotNull(mis2.single(), "Transaction was not found in ISSUED state")
      assertTrue(mis2.single().state.data.owner == sector1Bank, "Transaction is not owned by invoiced party")
    }

  }

  @Test
  fun `Should reject an invoice from the wrong notary `() {

    val txId = doTokenTransfer();

    val result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString(), invoicingNotary = guardianNotaryParty)

    assertFalse (result, "invoice should not have been accepted!")

  }

  @Test
  fun `Should reject an invoice for a non-valid transaction `() {

    val transactionId = UniqueIdentifier()

    val result = IssueMeteringInvoiceAndCheck(transactionId)

    assertFalse (result, "invoice should not have been accepted!")

  }


  @Test
  fun `we can't steal money by raising duplicate invoices for the same transaction`() {

    val txId = doTokenTransfer();

    val issueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
        meteredTransactionId = txId.toString(),
        tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name),
        amount = 1000,
        payAccountId = "paymebackhere",
        daoParty = daoNodeParty,
        invoicingNotary = meteringNotaryParty,
        meteringPolicingNotary = guardianNotaryParty,
        invoicedParty = sector1Bank)

    val issueMeteringInvoiceFlow1 = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
    val issueMeteringInvoiceFlowFuture1 = meteringNotary.startFlow(issueMeteringInvoiceFlow1)

    mockNet.runNetwork()

    val issueMeteringInvoiceFlowResult1 = issueMeteringInvoiceFlowFuture1.getOrThrow()

    assert((issueMeteringInvoiceFlowResult1.tx.outputs[0].data as MeteringInvoiceState).meteringInvoiceProperties.meteredTransactionId == txId.toString())

    val issueMeteringInvoiceFlow2 = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
    val issueMeteringInvoiceFlowFuture2 = meteringNotary.startFlow(issueMeteringInvoiceFlow2)

    mockNet.runNetwork()

    try {
      issueMeteringInvoiceFlowFuture2.getOrThrow()
    } catch (e: RuntimeException) {
      println("***** \n\n\nResult $e \n cause :  ${e.cause} \n\n\n****")
      assert(e.cause!!.cause is ConstraintViolationException)
    }
  }

  @Test
  fun `we can pay a metering invoice`() {

    val txId = doTokenTransfer();

    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    result = PayMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "payment was rejected!")

  }

  @Test
  fun `we can pay multiple metering invoices in a single transaction`() {

     // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // 2. and spend them, having the metering notary notarise them
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    // 3. generate the metering invoices
    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("40",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    var result2 = IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())
    assertTrue (result2, "invoice was rejected!")


    // 4. pay the metering invoices in one batch
    val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId.toString(), sector1BankAccount1)
    val payMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId2.toString(), sector1BankAccount1)
    val payMeteringInvoicesFlow = PayMeteringInvoicesFlow.MeteringInvoicePayer(payMeteringInvoiceRequests = listOf(payMeteringInvoiceRequest, payMeteringInvoiceRequest2))
    val payMeteringInvoicesFlowFuture = sector1BankNode.startFlow(payMeteringInvoicesFlow)

    mockNet.runNetwork()

    payMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()

    daoNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.PAID }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in PAID state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in PAID state!")
    }

    meteringNotary.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of PAID states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.PAID }
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in PAID state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in PAID state!")
    }

  }


  @Test
  fun `we cant pay multiple metering invoices if we don't have enough dao-issued tokens to cover them all!`() {

    // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()


    // 2. spend them
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    // 3 issue a reasonable metering invoice
    var result = IssueMeteringInvoiceAndCheck(amount = 1, transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("15",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    // 3 issue an unreasonably large metering invoice
    var result2 = IssueMeteringInvoiceAndCheck(amount = 100, transactionId = txId2.toString())
    assertTrue (result2, "invoice was rejected!")


    // 4. pay the invoices in one batch
      assertFails {
        val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId.toString(), sector1BankAccount1)
        val payMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId2.toString(), sector1BankAccount1)
        val payMeteringInvoicesFlow = PayMeteringInvoicesFlow.MeteringInvoicePayer(payMeteringInvoiceRequests = listOf(payMeteringInvoiceRequest, payMeteringInvoiceRequest2))
        val payMeteringInvoiceFlowFuture = sector1BankNode.startFlow(payMeteringInvoicesFlow)
        mockNet.runNetwork()
        payMeteringInvoiceFlowFuture.getOrThrow()
      }


  }


  @Test
  fun `we can dispute a metering invoice`() {
    val txId = doTokenTransfer();

    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    result = DisputeMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "dispute failed!")
  }

  @Test
  fun `we can dispute multiple metering invoices in a single transaction`() {

    // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // 2. and spend them, having the metering notary notarise them
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    // 3. generate the metering invoices
    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("40",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    var result2 = IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())
    assertTrue (result2, "invoice was rejected!")


    // 4. dispute the metering invoices in one batch

    val disputeMeteringInvoiceRequest1 = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(txId.toString())
    val disputeMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(txId2.toString())


    val disputeMeteringInvoicesFlow = DisputeMeteringInvoicesFlow.MeteringInvoiceDisputer(disputeMeteringInvoiceRequests = listOf(disputeMeteringInvoiceRequest1, disputeMeteringInvoiceRequest2))
    val disputeMeteringInvoicesFlowFuture = sector1BankNode.startFlow(disputeMeteringInvoicesFlow)

    mockNet.runNetwork()

    disputeMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()

    daoNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in PAID state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in PAID state!")
    }

    meteringNotary.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE }
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in PAID state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in PAID state!")
    }

  }

  @Test
  fun `we can reissue multiple metering invoices in a single transaction`() {

    // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // 2. and spend them, having the metering notary notarise them
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    // 3. generate the metering invoices
    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("40",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    var result2 = IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())
    assertTrue (result2, "invoice was rejected!")


    // 4. dispute the metering invoices in one batch

    val disputeMeteringInvoiceRequest1 = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(txId.toString())
    val disputeMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(txId2.toString())


    val disputeMeteringInvoicesFlow = DisputeMeteringInvoicesFlow.MeteringInvoiceDisputer(disputeMeteringInvoiceRequests = listOf(disputeMeteringInvoiceRequest1, disputeMeteringInvoiceRequest2))
    val disputeMeteringInvoicesFlowFuture = sector1BankNode.startFlow(disputeMeteringInvoicesFlow)

    mockNet.runNetwork()

    disputeMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()

    // 5. re-issue the metering invoices in one batch

    val tokenType = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name )
    val newAmount = 15

    val reissueMeteringInvoiceRequest1 = MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest(txId.toString(), tokenType, newAmount )
    val reissueMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest(txId2.toString(), tokenType, newAmount)

    val reissueMeteringInvoicesFlow = ReissueMeteringInvoicesFlow.MeteringInvoiceReissuer (reissueMeteringInvoiceRequests = listOf(reissueMeteringInvoiceRequest1, reissueMeteringInvoiceRequest2))
    val reissueMeteringInvoicesFlowFuture = sector1BankNode.startFlow(reissueMeteringInvoicesFlow)

    mockNet.runNetwork()

    reissueMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()

    daoNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in REISSUED state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in REISSUED state!")
    }

    meteringNotary.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED }
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in REISSUED state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in REISSUED state!")
    }

  }



  @Test
  fun `we can issue, dispute, reissue and pay invoice, oh yeah`() {

    val txId = doTokenTransfer();

    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    result = DisputeMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "dispute failed!")

    result = ReissueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "reissued invoice was rejected!")

    result = PayMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "payment was rejected!")

  }

  @Test
  fun `we can issue, pay and disperse invoice funds`() {
    val txId = doTokenTransfer();

    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    result = PayMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "payment was rejected!")

    result = DisperseMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "disperse failed!")
  }

  @Test
  fun `we can issue, dispute repeatedly and eventually pay an invoice`() {
    val txId = doTokenTransfer();

    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    result = DisputeMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "dispute failed!")

    result = ReissueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "reissued invoice was rejected!")

    result = DisputeMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "dispute failed!")

    result = ReissueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "reissued invoice was rejected!")

    result = PayMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "payment was rejected!")
  }

  @Test
  fun `we can pay and disperse fees for multiple metering invoices in a single transaction`() {

    // 1. issue some sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // 2. and spend them, having the metering notary notarise them
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    // 3. generate the metering invoices
    var result = IssueMeteringInvoiceAndCheck(transactionId = txId.toString())
    assertTrue (result, "invoice was rejected!")

    val txId2 = sector1BankNodeLedger.transferAccountToAccount("40",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer2",
        meteringNotaryX500Name).getOrThrow()

    var result2 = IssueMeteringInvoiceAndCheck(transactionId = txId2.toString())
    mockNet.runNetwork()
    assertTrue (result2, "invoice was rejected!")


    // 4. pay the metering invoices in one batch

    val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId.toString(), sector1BankAccount1)
    val payMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(txId2.toString(), sector1BankAccount1)
    val payMeteringInvoicesFlow = PayMeteringInvoicesFlow.MeteringInvoicePayer(payMeteringInvoiceRequests = listOf(payMeteringInvoiceRequest, payMeteringInvoiceRequest2))
    val payMeteringInvoicesFlowFuture = sector1BankNode.startFlow(payMeteringInvoicesFlow)

    mockNet.runNetwork()

    payMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()


    // 4. disperse fees for the metering invoices in one batch
    val meteringModelData = buildMeteringModelData(meteringNotaryParty,guardianNotaryParty, daoNodeParty, daoHoldingAccount, daoFoundationAccount, meteringNotaryAccount1, guardianNotaryAccount1)

    val disperseFundsForMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(txId.toString())
    val disperseFundsForMeteringInvoiceRequest2 = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(txId2.toString())
    val disperseFundsForMeteringInvoicesFlow = DisperseMeteringInvoicesFundsFlow.MeteringInvoiceFundDisperser(disperseFundsForMeteringInvoiceRequests = listOf(disperseFundsForMeteringInvoiceRequest, disperseFundsForMeteringInvoiceRequest2),meteringModelData = meteringModelData)
    val disperseFundsForMeteringInvoicesFlowFuture = daoNode.startFlow(disperseFundsForMeteringInvoicesFlow)

    mockNet.runNetwork()

    disperseFundsForMeteringInvoicesFlowFuture.getOrThrow()

    mockNet.runNetwork()

    daoNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in REISSUED state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in REISSUED state!")
    }

    meteringNotary.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in REISSUED state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in REISSUED state!")
    }

    sector1BankNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
      val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

      // find the original metered transactions in the list of IN_DISPUTE states
      val meteringInvoiceStates = meteringInvoiceStatesPage.states.filter { it.state.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED }

      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId.toString() }.size == 1, "Transaction was not found in REISSUED state!")
      assertTrue ( meteringInvoiceStates.filter { it.state.data.meteringInvoiceProperties.meteredTransactionId == txId2.toString() }.size == 1, "Transaction was not found in REISSUED state!")
    }

  }


  private fun ReissueMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) : Boolean {
    return ReissueMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun ReissueMeteringInvoiceAndCheck(transactionId: String) : Boolean {
    try {
      val reissueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.ReIssueMeteringInvoiceRequest(meteredTransactionId = transactionId.toString(), amount = 8, tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name))
      val reissueMeteringInvoiceFlow = ReissueMeteringInvoiceFlow.MeteringInvoiceReissuer(reissueMeteringInvoiceRequest = reissueMeteringInvoiceRequest)
      val reissueMeteringInvoiceFlowFuture = meteringNotary.startFlow(reissueMeteringInvoiceFlow)

      mockNet.runNetwork()

      reissueMeteringInvoiceFlowFuture.getOrThrow()

      mockNet.runNetwork()

      daoNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == sector1Bank)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED)
      }

      sector1BankNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ sector1BankNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == sector1Bank)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.REISSUED)
      }
    } catch (e: Exception) {
      return false
    }
    return true
  }

  private fun PayMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) : Boolean {
    return PayMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun PayMeteringInvoiceAndCheck(transactionId: String) : Boolean {

    try {
      val payMeteringInvoiceRequest = MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest(transactionId.toString(), sector1BankAccount1)
      val payMeteringInvoiceFlow = PayMeteringInvoiceFlow.MeteringInvoicePayer(payMeteringInvoiceRequest = payMeteringInvoiceRequest)
      val payMeteringInvoiceFlowFuture = sector1BankNode.startFlow(payMeteringInvoiceFlow)

      mockNet.runNetwork()

      payMeteringInvoiceFlowFuture.getOrThrow()

      mockNet.runNetwork()

      daoNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == daoNodeParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
      }

      meteringNotary.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ meteringNotary.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == daoNodeParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.PAID)
      }
    }

    catch( e: Exception ) {
      return false
    }

    return true
  }

  private fun DisputeMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) : Boolean {
    return DisputeMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun DisputeMeteringInvoiceAndCheck(transactionId: String) : Boolean {

    try {
      println("\nDispute Metering Invoice - Checking what's in the database before we do stuff")
      checkWhatInvoicesWeHaveInTheDatabase(daoNode)
      checkWhatInvoicesWeHaveInTheDatabase(sector1BankNode)
      checkWhatInvoicesWeHaveInTheDatabase(meteringNotary)

      val disputeMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisputeMeteringInvoiceRequest(transactionId.toString())
      val disputeMeteringInvoiceFlow = DisputeMeteringInvoiceFlow.MeteringInvoiceDisputer(disputeMeteringInvoiceRequest = disputeMeteringInvoiceRequest)
      val disputeMeteringInvoiceFlowFuture = sector1BankNode.startFlow(disputeMeteringInvoiceFlow)

      mockNet.runNetwork()

      disputeMeteringInvoiceFlowFuture.getOrThrow()

      mockNet.runNetwork()

      daoNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == meteringNotaryParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE)
      }

      meteringNotary.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ meteringNotary.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == meteringNotaryParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE)
      }
    } catch (e: Exception) {
      return false
    }
    return true
  }

  private fun DisperseMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) : Boolean {
    return DisperseMeteringInvoiceAndCheck(transactionId.toString())

  }
  private fun DisperseMeteringInvoiceAndCheck(transactionId: String): Boolean {
    try {
      println("\nDisperse Metering Invoice - Checking what's in the database before we do stuff")
      checkWhatInvoicesWeHaveInTheDatabase(daoNode)
      checkWhatInvoicesWeHaveInTheDatabase(sector1BankNode)
      checkWhatInvoicesWeHaveInTheDatabase(meteringNotary)

      val meteringModelData = buildMeteringModelData(meteringNotaryParty,guardianNotaryParty, daoNodeParty, daoHoldingAccount, daoFoundationAccount, meteringNotaryAccount1, guardianNotaryAccount1)

      val disperseeMeteringInvoiceRequest = MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest(transactionId.toString())
      val disperseMeteringInvoiceFlow = DisperseMeteringInvoiceFundsFlow.MeteringInvoiceFundDisperser(disperseFundsForMeteringInvoiceRequest = disperseeMeteringInvoiceRequest,
          meteringModelData = meteringModelData)
      val disperseMeteringInvoiceFlowFuture = daoNode.startFlow(disperseMeteringInvoiceFlow)

      mockNet.runNetwork()

      disperseMeteringInvoiceFlowFuture.getOrThrow()

      mockNet.runNetwork()

      daoNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ daoNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == daoNodeParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED)
      }

      meteringNotary.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ meteringNotary.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == daoNodeParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED)
      }

      sector1BankNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ sector1BankNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val meteringInvoiceState = meteringInvoiceStatesPage.states.single().state

        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(meteringInvoiceState.data.owner == daoNodeParty)
        assert(meteringInvoiceState.data.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED)
      }
    } catch (e: Exception) {
      return false
    }
    return true
  }

  private fun IssueMeteringInvoiceAndCheck(transactionId: UniqueIdentifier) : Boolean {
    return IssueMeteringInvoiceAndCheck(transactionId.toString())
  }

  private fun IssueMeteringInvoiceAndCheck(transactionId: String, amount: Int = 10, invoicedParty: Party = sector1Bank, invoicingNotary: Party = meteringNotaryParty) : Boolean {
    val issueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
        meteredTransactionId = transactionId.toString(),
        tokenDescriptor = TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name),
        amount = amount,
        payAccountId = "paymebackhere",
        daoParty = daoNodeParty,
        invoicingNotary = invoicingNotary,
        meteringPolicingNotary = guardianNotaryParty,
        invoicedParty = invoicedParty)

    try {

      val issueMeteringInvoiceFlow = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
      val issueMeteringInvoiceFlowFuture = meteringNotary.startFlow(issueMeteringInvoiceFlow)

      mockNet.runNetwork()

      val issueMeteringInvoiceFlowResult = issueMeteringInvoiceFlowFuture.getOrThrow()
      println("result: " + (issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).toString())
      println("result: " + (issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull())
      assert(((issueMeteringInvoiceFlowResult.tx.outputs[0].data as MeteringInvoiceState).owner.nameOrNull().toString() == sector1Bank.name.toString()))

      mockNet.runNetwork()

      meteringNotary.transaction {

        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ meteringNotary.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)

        val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state
        assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteringState == MeteringState.ISSUED)
        assert(originalMeteringInvoice.data.owner == sector1Bank)
      }

      sector1BankNode.transaction {
        val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(MeteringInvoiceState::class.java))
        val meteringInvoiceStatesPage = runAndRetry({ sector1BankNode.services.vaultService.queryBy<MeteringInvoiceState>(criteria) }, 10, 500)
        val originalMeteringInvoice = meteringInvoiceStatesPage.states.last().state

        assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteredTransactionId == transactionId.toString())
        assert(originalMeteringInvoice.data.meteringInvoiceProperties.meteringState == MeteringState.ISSUED)
        assert(originalMeteringInvoice.data.owner == sector1Bank)
      }
    }
    catch( e: Exception ) {
      return false;
    }
    return true;
  }

  private fun doTokenTransfer(): SecureHash {

    // issue 50 sector1bank tokens
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    // transfer 10 of them to sector2bank
    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Check the balances
    val sector1BankBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assert(sector1BankBalance.first().quantity == 4000L)

    val sector2BankBalance = sector2BankNodeLedger.balanceForAccount(sector2BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assert(sector2BankBalance.first().quantity == 1000L)

    return txId;

  }


  private fun doTokenTransferWithChecks(): SecureHash {


    //Lets check the initial balances
    val sector1BankInitialBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assertTrue(sector1BankInitialBalance.contains(Amount<TokenType.Descriptor>(10000L, TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name))))

    // now issue another 50 in sector1bank
    val amount = "50.00"
    sector1BankNodeLedger.issueToken(sector1BankAccount1, amount, testCurrencyXTS, "issuance", meteringNotaryX500Name).getOrThrow()
    mockNet.runNetwork()

    val sector1BankNewBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assertTrue(sector1BankNewBalance.contains(Amount<TokenType.Descriptor>(10000L, TokenType.Descriptor(testCurrencyXTS, 2, daoX500Name))))
    assertTrue(sector1BankNewBalance.contains(Amount<TokenType.Descriptor>(5000L, TokenType.Descriptor(testCurrencyXTS, 2, sector1BankX500Name))))

    mockNet.runNetwork()

    val txId = sector1BankNodeLedger.transferAccountToAccount("10",
        testCurrencyXTS,
        sector1BankAccount1,
        "$sector2BankAccount1@${sector2BankNode.info.legalIdentities.first().name}",
        "transfer",
        meteringNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Check the balances
    val sector1BankBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assert(sector1BankBalance.first().quantity == 4000L)

    val sector2BankBalance = sector2BankNodeLedger.balanceForAccount(sector2BankAccount1).getOrThrow()
    mockNet.runNetwork()
    assert(sector2BankBalance.first().quantity == 1000L)

    sector1BankNode.transaction {
      val criteria = QueryCriteria.VaultQueryCriteria(contractStateTypes = setOf(ContractState::class.java))
      val allContractStates = sector1BankNode.services.vaultService.queryBy<ContractState>(criteria)
      mockNet.runNetwork()
      assert(allContractStates.states.map { stateAndRef -> stateAndRef.ref.txhash }.contains(txId))
    }

    mockNet.runNetwork()


    return txId;

  }

  private fun checkWhatInvoicesWeHaveInTheDatabase(node: StartedMockNode) {

    val sqlForCheckingStates = """
    select * from CORDITE_METERING_INVOICE
    """
    val nodeName = node.info.legalIdentities.first().name.toString()

    println("\n$nodeName listing transactions\n")

    node.transaction {
      node.services.jdbcSession().executeCaseInsensitiveQuery(sqlForCheckingStates).forEach { getAllMeteringInvoice(it) }
    }
  }

  private fun getAllMeteringInvoice(rs: ResultSet) {
    println("**************************************************")
    println("METERED_TRANSACTION_ID: " + rs.getString("METERED_TRANSACTION_ID"))
    println("SYMBOL: " + rs.getString("TOKEN_TYPE_SYMBOL"))
    println("EXPONENT: " + rs.getString("TOKEN_TYPE_EXPONENT"))
    println("ISSUER: " + rs.getString("TOKEN_TYPE_ISSUER"))
    println("AMOUNT: " + rs.getInt("AMOUNT").toString())
    println("INVOICE_ID: " + rs.getString("INVOICE_ID"))
    println("METERING_STATE: " + rs.getString("METERING_STATE"))
    println("INVOICED_PARTY: " + rs.getString("INVOICED_PARTY"))
    println("INVOICING_NOTARY: " + rs.getString("INVOICING_NOTARY"))
    println("DAO_PARTY: " + rs.getString("DAO_PARTY"))
    println("PAY_ACCOUNT_ID: " + rs.getString("PAY_ACCOUNT_ID"))
    println("OWNER: " + rs.getString("OWNER"))
    println("**************************************************\n")
  }

  private fun createDaoTokensAndLoadAccounts() {

    // drain both accounts, sweeping the balances into the central bank account :)
    drainAccount(daoNodeLedger,daoX500Name,daoHoldingAccount, centralBankX500Name, guardianNotaryX500Name )
    drainAccount(sector1BankNodeLedger, daoX500Name, "$sector1BankAccount1@${sector1BankNode.info.legalIdentities.first().name}", centralBankX500Name, guardianNotaryX500Name )


    val amount = "500.00"

    daoNodeLedger.issueToken(daoHoldingAccount, amount, testCurrencyXTS, "issuance to Dao", guardianNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    daoNodeLedger.transferAccountToAccount("100",
        testCurrencyXTS,
        "${WellKnownTagCategories.DGL_ID}:$daoHoldingAccount@${daoNode.info.legalIdentities.first().name}",
        "$sector1BankAccount1@${sector1BankNode.info.legalIdentities.first().name}",
        "transfer",
        guardianNotaryX500Name).getOrThrow()

    mockNet.runNetwork()

    //Lets check the balances
    val sector1BankBalance = sector1BankNodeLedger.balanceForAccount(sector1BankAccount1).getOrThrow()
    val daoRemainingBalance = daoNodeLedger.balanceForAccount(daoHoldingAccount).getOrThrow()

    mockNet.runNetwork()

    assert(sector1BankBalance.first().quantity == 10000L)
    assert( daoRemainingBalance.first().quantity== 40000L)
  }

  /**
   * transfer the entire balance issued by <issuer> in <ledger>'s <bankAccount> and sweep it into <beneficiary>
   */
  fun drainAccount(ledger: LedgerApi, issuer: CordaX500Name, accountName: String, beneficiary: CordaX500Name, notary: CordaX500Name) {

    var balances = ledger.balanceForAccount(accountName).getOrThrow()

    balances.toList().forEach {

      if (it.token.issuerName == issuer) {

        val exp = it.token.exponent.toDouble()
        val amountToTransfer = (it.quantity / Math.pow(10.0, exp)).toString()

        // sweep the money into the dao account
        ledger.transferAccountToAccount(amountToTransfer,
            it.token.symbol,
            accountName,
            beneficiary.toString(),
            "transfer",
            notary).getOrThrow()

        mockNet.runNetwork()
      }
    }
  }


  @After
  fun tearDown() {
    mockNet.runNetwork()
    mockNet.stopNodes()
  }
}