/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flowtests

import net.corda.core.contracts.Requirements.using
import net.corda.core.contracts.requireThat
import org.junit.Test
import kotlin.test.fail


data class tran(val id: String, val someValue: Int)


class TestCollectionCompare {

  val inputs = listOf(tran("1", 1), tran("2", 2))
  val outputs = listOf(tran("1", 1), tran("2", 2))

  val inputDups = listOf(tran("2", 1), tran("1", 2), tran("2", 2))
  val outputDups = listOf(tran("1", 1), tran("2", 2), tran("2", 2))

  val inputMisOut = listOf(tran("1", 1), tran("2", 2), tran("3", 2))
  val outputMisOut = listOf(tran("1", 1), tran("2", 2), tran("4", 2))

  val inputMCheekyCreate = listOf(tran("1", 1), tran("2", 2))
  val outputCheekyCreate = listOf(tran("1", 1), tran("2", 2), tran("4", 2))

  val inputMCheekyBurn = listOf(tran("1", 1), tran("2", 2), tran("3", 3))
  val outputCheekyBurn = listOf(tran("1", 1), tran("2", 2))

  @Test
  fun `clean test`() {
    checkIntegrity(inputs, outputs)
  }

  @Test
  fun `input duplicates`() {
    try {
      checkIntegrity(inputDups, outputs)
      fail("should throw exception")
    } catch (e: Exception) {
      assert(e.message =="Failed requirement: inputs are not unique")
      println(e.message)
    }
  }

  @Test
  fun `input output duplicates`() {
    try {
      checkIntegrity(inputDups, outputDups)
      fail("should throw exception")
    } catch (e: Exception) {
      println("Failed with {$e}")
    }
  }

  @Test
  fun `input  mismatch`() {
    try {
      checkIntegrity(inputMisOut, outputs)
      fail("should throw exception")
    } catch (e: Exception) {
      println("Failed with {$e}")
    }
  }

  @Test
  fun `input output mismatch`() {
    try {
      checkIntegrity(inputMisOut, outputMisOut)
      fail("should throw exception")
    } catch (e: Exception) {
      println("Failed with {$e}")
    }
  }

  @Test
  fun `cheeky create`() {
    try {
      checkIntegrity(inputMCheekyCreate, outputCheekyCreate)
      fail("should throw exception")
    } catch (e: Exception) {
      println("Failed with {$e}")
    }
  }

  @Test
  fun `cheeky burn`() {
    try {
      checkIntegrity(inputMCheekyBurn, outputCheekyBurn)
      fail("should throw exception")
    } catch (e: Exception) {
      println("Failed with {$e}")
    }
  }

  private fun checkIntegrity(inputs: List<tran>, outputs: List<tran>) {
    //for each item in array one, there should only be one of them in array 2
    requireThat {
      "some of the inputs are not in the outputs - indicates attempt to burn invoice " using (inputs.dropLastWhile { outputs.map { it.id }.contains(it.id) }.count() == 0)
      "some of the outputs are not in the inputs - indicates attempt to create duplicate invoice " using (outputs.dropLastWhile { inputs.map { it.id }.contains(it.id) }.count() == 0)
      "inputs are not unique" using (inputs.count() == inputs.distinctBy { it.id }.count())
      "outputs are not unique" using (inputs.count() == inputs.distinctBy { it.id }.count())
    }
  }
}