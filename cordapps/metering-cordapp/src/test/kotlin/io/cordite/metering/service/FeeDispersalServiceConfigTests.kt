/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import org.junit.Test

class FeeDispersalServiceConfigTests {

  @Test
  fun `I can load the config with all the parameters populated with default file`() {
    val feeDispersalServiceConfig = FeeDispersalServiceConfig.loadConfig()
    assert(feeDispersalServiceConfig.feeDispersalRefreshInterval == 2000L)
    assert(feeDispersalServiceConfig.feeDispersalServicePartyName == "Cordite Committee")
    assert(feeDispersalServiceConfig.daoName == "Cordite Committee")
  }

  @Test
  fun `I can load the config with all the parameters populated with a file of my choice`() {
    val feeDispersalServiceConfig = FeeDispersalServiceConfig.loadConfig("feeDispersal-config-file-of-my-choice.json")
    assert(feeDispersalServiceConfig.feeDispersalRefreshInterval == 2000L)
    assert(feeDispersalServiceConfig.feeDispersalServicePartyName == "daodaodao")
    assert(feeDispersalServiceConfig.daoName == "SomeOtherDao")
  }

  @Test
  fun `I will resort to defaults if I cant find a file`() {
    val feeDispersalServiceConfig = FeeDispersalServiceConfig.loadConfig("you-will-never-find-this-feeDispersal-config-file-of-my-choice.json")
    assert(feeDispersalServiceConfig.feeDispersalRefreshInterval == 2000L)
    assert(feeDispersalServiceConfig.feeDispersalServicePartyName == "")
    assert(feeDispersalServiceConfig.daoName == "")
  }
}