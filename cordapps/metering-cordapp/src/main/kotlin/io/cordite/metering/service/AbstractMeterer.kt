/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.transaction
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.daostate.MeteringNotaryMember
import io.cordite.metering.daostate.MeteringNotaryType
import io.cordite.metering.daostate.MeteringTransactionCost
import io.cordite.metering.flow.IssueMeteringInvoiceFlow
import io.cordite.metering.flow.MeteringInvoiceFlowCommands
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.getOrThrow
import net.corda.core.utilities.loggerFor

abstract class AbstractMeterer(protected val serviceHub: AppServiceHub, val meteringServiceConfig: MeteringServiceConfig) {

  private val log = loggerFor<AbstractMeterer>()
  private val meteringDaoStateLoader = MeteringDaoStateLoader(serviceHub, meteringServiceConfig.daoName)
  protected val myIdentity = serviceHub.myInfo.legalIdentities.first()
  protected lateinit var meteringModelData: MeteringModelData



  val sqlForCheckingIfTheMeteringInvoiceTableHasBeenCreated = """
  select count(1) TABLE_EXISTS from INFORMATION_SCHEMA.TABLES where UPPER(TABLE_NAME) = 'CORDITE_METERING_INVOICE'
  """

  init {
    log.info("Abstract Meterer Created")
  }

  protected abstract fun processUnmeteredTransactions()

  fun meterTransactions() {

    if (weAreReadyToMeter()) {
      processUnmeteredTransactions()
    } else {
      log.debug("Waiting for DaoMetering State to be loaded with all the things required for metering")
    }
  }

  /*
  we are only ready to meter if
    We have a meteringModel DaoState
    We are one of the metering notary members
    We have at least one guardian notary
  */
  fun weAreReadyToMeter(): Boolean {

    //TODO - note: this loads the dao state each time, but we could change it to listener to make this more efficient  https://gitlab.com/cordite/cordite/issues/196

    val loadedMeteringModelData = meteringDaoStateLoader.getMeteringDao() ?: return false

    //Have we got a Dao?

    log.debug("We have a metering DaoState")

    meteringModelData = loadedMeteringModelData

    //We should be on the notary member list - if you name's not danny you ain't metering
    if(!meteringModelData.meteringNotaryMembers.containsKey(myIdentity.name.toString())){ return false}

    log.debug("We are on the approved notary list")

    //We have at least one guardian notary, probably call skylord
    if (meteringModelData.meteringNotaryMembers.count { it.value.meteringNotaryType == MeteringNotaryType.GUARDIAN } < 0) {
      return false
    }
    return true
  }

  protected fun hasMeteringInvoiceTableBeenCreated(): Boolean {
    return serviceHub.transaction {
      val tableCount =
        serviceHub.jdbcSession()
        .executeCaseInsensitiveQuery(sqlForCheckingIfTheMeteringInvoiceTableHasBeenCreated)
        .map { it.getInt("TABLE_EXISTS") }.toBlocking().first()
      log.trace("Does the MeteredInvoice  Exist yet? $tableCount")
      if (tableCount > 0) {
        log.trace("The MeteredInvoice table really does exist")
      }
      tableCount > 0
    }
  }

  protected fun publishMeteringInvoice(unmeteredTransaction: MeterableTransaction) {

    val meteringTransactionCost = getTransactionCosts()
    val meteringTransactionCharge = meteringTransactionCost.meteringTransactionCost
    val meteringNotaryPoliceParty = getApproveGuardianNotary()
    val daoNodeParty = getPartyFromOrganisationName(meteringServiceConfig.daoPartyName)


    val daoAccount = meteringModelData.meteringFeeAllocation.daoHoldingAccountId
    val myParty = serviceHub.myInfo.legalIdentities.first()

    val invoicedParty = getPartyFromX500String(unmeteredTransaction.requestingPartyName)

    val issueMeteringInvoiceRequest = MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest(
      meteredTransactionId = unmeteredTransaction.txId,
      tokenDescriptor = meteringTransactionCost.meteringTransactionTokenDescriptor,
      amount = meteringTransactionCharge,
      payAccountId = daoAccount,
      daoParty = daoNodeParty,
      invoicingNotary = myParty,
      meteringPolicingNotary = meteringNotaryPoliceParty,
      invoicedParty = invoicedParty)

    log.info("\nAbout to send metering invoice request $issueMeteringInvoiceRequest")
    val issueMeteringInvoiceFlow = IssueMeteringInvoiceFlow.MeteringInvoiceIssuer(issueMeteringInvoiceRequest = issueMeteringInvoiceRequest)
    val issueMeteringInvoiceFlowFuture = serviceHub.startFlow(issueMeteringInvoiceFlow).returnValue

    val result = issueMeteringInvoiceFlowFuture.getOrThrow()

    log.info("\nIssued new metering invoice with details, tx hash = ${result.coreTransaction.id}")
  }

  private fun getApproveGuardianNotary(): Party {

    // 240 - get a list of approved guardian Notaries from the dao state; select a random guardian notary from all available
    val allMeteringNotaries: List<MeteringNotaryMember> = meteringDaoStateLoader.getMeteringNotariesFromDao()
    val guardianNotary = RandomNotarySelector.getRandomMeteringNotary(allMeteringNotaries, MeteringNotaryType.GUARDIAN)
    return guardianNotary.notaryParty
  }

  protected fun getPartyFromOrganisationName(organisation: String): Party {
    log.info("About to retrieve party info for organisation: $organisation")
    serviceHub.networkMapCache.allNodes.flatMap { it -> it.legalIdentities }.forEach { log.info("All nodes -Legal Identity ${it}") }

    val party = serviceHub.networkMapCache.allNodes.flatMap { it -> it.legalIdentities }.first { it.name.organisation == organisation }
    return party
  }

  private fun getPartyFromX500String(x500String: String): Party {
    log.info("About to retrieve party info for x500 String: $x500String")
    serviceHub.networkMapCache.allNodes.flatMap { it -> it.legalIdentities }.forEach { log.info("All nodes -Legal Identity ${it}") }

    val party = serviceHub.networkMapCache.allNodes.flatMap { it -> it.legalIdentities }.first { it.name.toString() == x500String }
    return party
  }

  //Gets the transaction cost and currency from the Dao state
  private fun getTransactionCosts(): MeteringTransactionCost {

    val meteringTransactionCost = meteringModelData.meteringTransactionCost

    return meteringTransactionCost
  }
}

