/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.commons.utils.getNotary
import io.cordite.metering.contract.*
import net.corda.core.contracts.Command
import net.corda.core.contracts.StateAndContract
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant

object IssueMeteringInvoiceFlow {

  private val log: Logger = LoggerFactory.getLogger(IssueMeteringInvoiceFlow.javaClass)

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoiceIssuer(val issueMeteringInvoiceRequest: MeteringInvoiceFlowCommands.IssueMeteringInvoiceRequest) : FlowLogic<SignedTransaction>() {

    private lateinit var otherPartyLocal: Party

    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {
      otherPartyLocal = serviceHub.networkMapCache.allNodes.flatMap { it.legalIdentities }.first { it.name.organisation == issueMeteringInvoiceRequest.invoicedParty.name.organisation }
      val txBuilder = buildTransaction()
      val partiallySignedTx = verifyAndSignTransaction(txBuilder)
      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx)
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx
    }

    fun buildTransaction(): TransactionBuilder {
      progressTracker.currentStep = BUILD_METERING_INVOICE_TRANSACTION

      val meteringInvoiceProperties = MeteringInvoiceProperties(
        meteringState = MeteringState.ISSUED,
        invoicedParty = issueMeteringInvoiceRequest.invoicedParty,
        invoicingNotary = issueMeteringInvoiceRequest.invoicingNotary,
        daoParty = issueMeteringInvoiceRequest.daoParty,
        payAccountId = issueMeteringInvoiceRequest.payAccountId,
        amount = issueMeteringInvoiceRequest.amount,
        tokenDescriptor = issueMeteringInvoiceRequest.tokenDescriptor,
        meteredTransactionId = issueMeteringInvoiceRequest.meteredTransactionId,
        reissueCount = 0,
        createdDateTime = Instant.now())

      val notary = serviceHub.getNotary(issueMeteringInvoiceRequest.meteringPolicingNotary.name.organisation)
      val meteringInvoiceStateToIssue = MeteringInvoiceState(meteringInvoiceProperties, meteringInvoiceProperties.invoicedParty)
      val txCommand = Command(MeteringInvoiceCommands.Issue(), meteringInvoiceStateToIssue.participants.map { it.owningKey })
      val txBuilder = TransactionBuilder(notary).withItems(StateAndContract(meteringInvoiceStateToIssue, MeteringInvoiceContract.METERING_CONTRACT_ID), txCommand)

      return txBuilder
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder)
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS
      val otherPartyFlowSession = initiateFlow(otherPartyLocal)
      val daoFlowSession = initiateFlow(issueMeteringInvoiceRequest.daoParty)

      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, setOf(otherPartyFlowSession,daoFlowSession), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoiceIssuer::class)
  class MeteringInvoiceReceiver(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {
          val output = stx.tx.outputs.single().data
          "This must be a MeteringInvoice Transaction." using (output is MeteringInvoiceState)
          val meteringInvoiceState = output as MeteringInvoiceState
          "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceState, serviceHub, log))
        }
      }
      return subFlow(signTransactionFlow)
    }

  }
}
