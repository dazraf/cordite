/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.service

import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.transaction
import net.corda.core.node.AppServiceHub
import net.corda.core.utilities.loggerFor
import java.sql.ResultSet

//The SingleNode Meterer is built to work with a single validating or non validating notary
//and not with the BFT Smart, Raft Notaries or HA Notaries or notaries that have not been invented yet.
//The key difference is that Single Notaries use a different table to store committed transactions.
//In the case of a single notary the NODE_NOTARY_COMMIT_LOG table is used to store committed transactions;
//hence why we have a specialised SingleNodeMeterer class, with the base AbstractMeterer current holding the
//common code to publish the metering invoice.

class SingleNodeMeterer(serviceHub : AppServiceHub, meteringServiceConfig: MeteringServiceConfig) : AbstractMeterer(serviceHub,meteringServiceConfig) {

  private val log = loggerFor<SingleNodeMeterer>()

  val sqlForFindingUnmeteredTransactionsWhenMeteredInvoiceTableHasNotBeenCreated ="""
  SELECT nncl.TRANSACTION_ID TRANSACTION_ID,
         'UNMETERED' METERING_STATE,
         nncl.REQUESTING_PARTY_NAME REQUESTING_PARTY_NAME,COUNT(*) STATE_COUNT
  FROM PUBLIC.NODE_NOTARY_COMMIT_LOG nncl
  WHERE nncl.REQUESTING_PARTY_NAME <>'%s'
  GROUP BY nncl.TRANSACTION_ID,METERING_STATE,nncl.REQUESTING_PARTY_NAME;
  """

  val sqlForFindingUnmeteredTransactionsAfterMeteringInvoiceTableHasBeenCreated = """
  SELECT nncl.TRANSACTION_ID TRANSACTION_ID,
         COALESCE(mi.METERING_STATE,'UNMETERED') METERING_STATE,
         nncl.REQUESTING_PARTY_NAME REQUESTING_PARTY_NAME,COUNT(*) STATE_COUNT
  FROM PUBLIC.NODE_NOTARY_COMMIT_LOG nncl
  LEFT JOIN CORDITE_METERING_INVOICE mi
  ON nncl.TRANSACTION_ID = mi.METERED_TRANSACTION_ID
  WHERE mi.TRANSACTION_ID is NULL
  AND nncl.REQUESTING_PARTY_NAME <>'%s'
  GROUP BY nncl.TRANSACTION_ID,mi.METERING_STATE,nncl.REQUESTING_PARTY_NAME;
  """

  protected override fun processUnmeteredTransactions() {

    val daoNodeParty = getPartyFromOrganisationName(meteringServiceConfig.daoPartyName)

    val sqlForFindingUnmeteredTransactions: String

    if (hasMeteringInvoiceTableBeenCreated()) sqlForFindingUnmeteredTransactions = sqlForFindingUnmeteredTransactionsAfterMeteringInvoiceTableHasBeenCreated.format(daoNodeParty.name) else {
      sqlForFindingUnmeteredTransactions = sqlForFindingUnmeteredTransactionsWhenMeteredInvoiceTableHasNotBeenCreated.format(daoNodeParty.name)
    }

    log.trace("\nFinding unmetered transactions for $myIdentity")
    log.trace("SQL to execute: $sqlForFindingUnmeteredTransactions")

    val unmeteredTransactions = mutableListOf<MeterableTransaction>()

    serviceHub.transaction {
      val session = serviceHub.jdbcSession()
      session.executeCaseInsensitiveQuery(sqlForFindingUnmeteredTransactions).forEach {
        getUnmeteredTransaction(it)
      }
      session.executeCaseInsensitiveQuery(sqlForFindingUnmeteredTransactions).forEach {
        unmeteredTransactions.add(getUnmeteredTransaction(it))
      }
    }
    unmeteredTransactions.forEach { publishMeteringInvoice(it) }
  }

  fun getUnmeteredTransaction(rs: ResultSet): MeterableTransaction {

    val unmeteredTransaction = MeterableTransaction(
      txId = rs.getString("TRANSACTION_ID"),
      meteringState = rs.getString("METERING_STATE"),
      requestingPartyName = rs.getString("REQUESTING_PARTY_NAME")
    )

    log.info("Retrieved Unmetered Transaction\n txId:${unmeteredTransaction.txId}" +
      "\n meteringState ${unmeteredTransaction.meteringState}" +
      "\n requestingPartyName ${unmeteredTransaction.requestingPartyName}")

    return unmeteredTransaction
  }
}