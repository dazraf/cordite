/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.Token
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.flows.TransferTokenSenderFunctions
import io.cordite.metering.contract.*
import io.cordite.metering.daostate.MeteringFeeAllocator
import io.cordite.metering.daostate.MeteringModelData
import io.cordite.metering.daostate.MeteringNotaryMember
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import net.corda.core.contracts.*
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.node.services.Vault
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.security.PublicKey
import java.time.Instant

object DisperseMeteringInvoicesFundsFlow {

  private val log: Logger = LoggerFactory.getLogger(DisperseMeteringInvoiceFundsFlow.javaClass)

  private lateinit var notarisingNotary: Party
  private lateinit var daoParty: Party
  private lateinit var tokenType: TokenType.Descriptor

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoiceFundDisperser(val disperseFundsForMeteringInvoiceRequests: List<MeteringInvoiceFlowCommands.DisperseFundsForMeteringInvoiceRequest>, val meteringModelData: MeteringModelData) : FlowLogic<SignedTransaction>() {

    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )

    @Suspendable
    override fun call(): SignedTransaction {
      checkAndExtractInvoiceAttributes()
      val splitInvoicesTx = splitMeteringInvoiceToAllocations()
      val meteringInvoiceSplits = splitInvoicesTx.coreTransaction.outRefsOfType<MeteringInvoiceSplitState>()
      dispersePayments(meteringInvoiceSplits)
      return splitInvoicesTx
    }

    fun checkAndExtractInvoiceAttributes() {

      val checker = MeteringInvoiceConsistencyChecker(originalInvoices = FlowUtils.getOriginalMeteringInvoices(serviceHub, disperseFundsForMeteringInvoiceRequests.map { it.meteredTransactionId }))

      notarisingNotary = checker.getNotarisingNotary()
      daoParty = checker.getDaoParty()
      tokenType = checker.getTokenType()
    }

    @Suspendable
    private fun dispersePayments(meteringInvoiceSplits: List<StateAndRef<MeteringInvoiceSplitState>>): List<SignedTransaction> {
      val signedTransactions = mutableListOf<SignedTransaction>()

      meteringInvoiceSplits.forEach {
        val meteringInvoiceSplitState = it.state.data
        if (meteringInvoiceSplitState.meteringInvoiceSplitProperties.finalParty == it.state.notary) {
          transferNotaryAndDispersePayment(it)
        } else {
          signedTransactions.add(dispersePayment(meteringInvoiceSplitState,meteringModelData.meteringFeeAllocation.daoHoldingAccountId))
        }
      }
      return signedTransactions
    }

    @Suspendable
    private fun transferNotaryAndDispersePayment(meteringInvoiceSplitStateAndRef: StateAndRef<MeteringInvoiceSplitState>) : SignedTransaction{
      //Pay Into Temp (local) guardian notary account
      val meteringInvoiceSplit = meteringInvoiceSplitStateAndRef.state.data

      val tempPaymentTx = doLocalPaymentForGuardianNotary(meteringInvoiceSplit, meteringInvoiceSplitStateAndRef.state.notary)

      //Pull out the funds again - just using account - verify amount == invoice split amount
      val paymentStateAndRefs = tempPaymentTx.coreTransaction.filterOutRefs<Token.State> { it.accountAddress.accountId == meteringInvoiceSplit.meteringInvoiceSplitProperties.finalAccountId }

      //Change Payment Notary
      paymentStateAndRefs.forEach {
       val committedStateAndRef =  subFlow(NotaryChangeFlow(it, meteringInvoiceSplit.meteringInvoiceSplitProperties.invoicingNotary))
        waitForLedgerCommit(committedStateAndRef.ref.txhash)
      }

      //Change Metering Invoice Split State Notary
      val committedStateAndRef = subFlow(NotaryChangeFlow(meteringInvoiceSplitStateAndRef,meteringInvoiceSplit.meteringInvoiceSplitProperties.invoicingNotary))
      waitForLedgerCommit(committedStateAndRef.ref.txhash)

      //Now we can finally pay the guardian notary
      val finalSplitDispersalTx =  dispersePayment(committedStateAndRef.state.data,meteringInvoiceSplit.meteringInvoiceSplitProperties.finalAccountId)

      return finalSplitDispersalTx
    }

    @Suspendable
    private fun doLocalPaymentForGuardianNotary(meteringInvoiceSplitState: MeteringInvoiceSplitState, notary : Party) : SignedTransaction {
        val txBuilder = TransactionBuilder(notary)

        val meteringInvoiceSplitProperties = meteringInvoiceSplitState.meteringInvoiceSplitProperties
        val fromAddress = AccountAddress(meteringModelData.meteringFeeAllocation.daoHoldingAccountId, meteringInvoiceSplitProperties.daoParty.name)
        val toAddress = AccountAddress(meteringInvoiceSplitProperties.finalAccountId, meteringInvoiceSplitProperties.daoParty.name)
        val tokenType = TokenType.Descriptor(meteringInvoiceSplitProperties.tokenDescriptor.symbol, 2, meteringInvoiceSplitProperties.daoParty.name)

        val amountInTokenType = Amount.fromDecimal(BigDecimal(meteringInvoiceSplitProperties.amount), tokenType)
        val tokenSigingKeys = TransferTokenSenderFunctions.prepareTokenMoveWithSummary(txBuilder, fromAddress, toAddress, amountInTokenType, serviceHub, ourIdentity, "metering invoice funds flow - local payment for Guardian notary")

        val fullySignedTx = verifyAndSignTransaction(txBuilder, tokenSigingKeys)
        val finalisedTx = finaliseTransaction(fullySignedTx)
        waitForLedgerCommit(finalisedTx.id)
        return finalisedTx
    }

    @Suspendable
    private fun dispersePayment(meteringInvoiceSplitState: MeteringInvoiceSplitState, sourceAccountId : String): SignedTransaction {
      val signedTx = subFlow(DisperseMeteringInvoiceSplitFlow.MeteringInvoiceSplitDisperser(meteringInvoiceSplitState, sourceAccountId ))
      waitForLedgerCommit(signedTx.id)
      return signedTx
    }


    @Suspendable
    fun splitMeteringInvoiceToAllocations(): SignedTransaction {

      var txBuilder = TransactionBuilder(notarisingNotary)

      val signers = mutableListOf<PublicKey>()

      disperseFundsForMeteringInvoiceRequests.forEach {

        val splitInvoiceId = it.meteredTransactionId // use the metered transaction id as the split invoice id

        val originalMeteringInvoiceStateAndRef = FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties
        val meteringFeeAllocation = meteringModelData.meteringFeeAllocation

        //calculate the feesToDisperse
        val feesToDisperse = MeteringFeeAllocator.AllocateFeesFromAmount(originalMeteringInvoiceProperties.amount, meteringFeeAllocation)

        val meteringNotaryMember = meteringModelData.meteringNotaryMembers[originalMeteringInvoiceProperties.invoicingNotary.name.toString()] as MeteringNotaryMember
        val guardianNotaryMember = meteringModelData.meteringNotaryMembers[notarisingNotary.name.toString()] as MeteringNotaryMember

        val invoiceSplitForMeteringNotary = MeteringInvoiceSplitProperties(meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
            amount = feesToDisperse.meterNotaryAllocation,
            meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
            allocations = feesToDisperse,
            finalAccountId = meteringNotaryMember.accountId,
            tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
            invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
            invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
            daoParty = originalMeteringInvoiceProperties.daoParty,
            finalParty = originalMeteringInvoiceProperties.invoicingNotary,
            splitInvoiceId = splitInvoiceId,
            createdDateTime = Instant.now())

        val invoiceSplitForGuardianNotary = MeteringInvoiceSplitProperties(meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
            amount = feesToDisperse.guardianNotaryAllocation,
            meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
            allocations = feesToDisperse,
            finalAccountId = guardianNotaryMember.accountId,
            tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
            invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
            invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
            daoParty = originalMeteringInvoiceProperties.daoParty,
            finalParty = guardianNotaryMember.notaryParty,
            createdDateTime = Instant.now())

        val invoiceSplitForDoaFoundation = MeteringInvoiceSplitProperties(meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
            amount = feesToDisperse.daoFoundationAllocation,
            meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
            allocations = feesToDisperse,
            finalAccountId = meteringFeeAllocation.daoFoundationAccount,
            tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
            invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
            invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
            daoParty = originalMeteringInvoiceProperties.daoParty,
            finalParty = originalMeteringInvoiceProperties.daoParty,
            createdDateTime = Instant.now())

        val invoiceSplitForMeteringNotaryState = MeteringInvoiceSplitState(invoiceSplitForMeteringNotary, daoParty)
        val invoiceSplitForGuardianNotaryState = MeteringInvoiceSplitState(invoiceSplitForGuardianNotary, daoParty)
        val invoiceSplitForDoaFoundationState = MeteringInvoiceSplitState(invoiceSplitForDoaFoundation, daoParty)

        val outputMeteringInvoiceProperties = originalMeteringInvoiceProperties.copy(meteringState = MeteringState.FUNDS_DISPERSED)
        val outputMeteringInvoiceState = MeteringInvoiceState(outputMeteringInvoiceProperties, daoParty)

        txBuilder.addInputState(originalMeteringInvoiceStateAndRef)
        txBuilder.addOutputState(invoiceSplitForMeteringNotaryState, MeteringInvoiceContract.METERING_CONTRACT_ID)
        txBuilder.addOutputState(invoiceSplitForGuardianNotaryState, MeteringInvoiceContract.METERING_CONTRACT_ID)
        txBuilder.addOutputState(invoiceSplitForDoaFoundationState, MeteringInvoiceContract.METERING_CONTRACT_ID)
        txBuilder.addOutputState(outputMeteringInvoiceState, MeteringInvoiceContract.METERING_CONTRACT_ID)

      }

      val txCommand = Command(MeteringInvoiceCommands.SplitInvoices(), daoParty.owningKey)
      txBuilder.withItems(txCommand)

      val partiallySignedTx = verifyAndSignTransaction(txBuilder, listOf(serviceHub.myInfo.legalIdentities.first().owningKey))

      val signatureParties = getSignatureParties()

      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx, signatureParties.toList())
      val finalisedTx = finaliseTransaction(fullySignedTx)

      waitForLedgerCommit(finalisedTx.id)

      return finalisedTx
    }

    // return the union set of DAOs and notaries appearing on the supplied invoices
    fun getSignatureParties() : Set<Party>{

      val signatureParties = mutableSetOf<Party>()

      disperseFundsForMeteringInvoiceRequests.forEach {
        val originalMeteringInvoiceStateAndRef = FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties
        signatureParties.addAll(setOf(originalMeteringInvoiceProperties.daoParty, originalMeteringInvoiceProperties.invoicingNotary))
      }
      return signatureParties
    }

    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties: List<Party>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(it) }
      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoiceFundDisperser::class)
  class MeteringInvoiceFundDispersee(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {
          stx.tx.outputs.filter { it.data is MeteringInvoiceState }.forEach {
            val output = it.data
            val meteringInvoiceState = output as MeteringInvoiceState
            "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceState, serviceHub, log))
          }
        }
      }
      return subFlow(signTransactionFlow)
    }
  }
}
