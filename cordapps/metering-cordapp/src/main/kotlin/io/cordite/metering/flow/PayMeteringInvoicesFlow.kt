/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.flow

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.flows.TransferTokenSenderFunctions.Companion.prepareTokenMoveWithSummary
import io.cordite.metering.contract.*
import net.corda.core.contracts.*
import net.corda.core.flows.*
import net.corda.core.identity.Party
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.ProgressTracker
import net.corda.core.utilities.ProgressTracker.Step
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.math.BigDecimal
import java.security.PublicKey
import java.time.Instant

object PayMeteringInvoicesFlow {

  private val log: Logger = LoggerFactory.getLogger(PayMeteringInvoicesFlow.javaClass)

  @InitiatingFlow
  @StartableByRPC
  @StartableByService
  class MeteringInvoicePayer(val payMeteringInvoiceRequests: List<MeteringInvoiceFlowCommands.PayMeteringInvoiceRequest>) : FlowLogic<SignedTransaction>() {

    private lateinit var originalInvoices: List<StateAndRef<MeteringInvoiceState>>
    private lateinit var invoicedParty: Party
    private lateinit var invoiceNotary: Party
    private lateinit var notarisingNotary: Party
    private lateinit var daoParty: Party
    private lateinit var fromAccount: String
    private lateinit var toAccount: String
    private lateinit var tokenType: TokenType.Descriptor

    companion object {
      object BUILD_METERING_INVOICE_TRANSACTION : Step("Build the metering invoice transaction")
      object VERIFYING_TRANSACTION : Step("Verifying contract constraints.")
      object SIGNING_TRANSACTION : Step("Signing transaction with our private key.")
      object GATHERING_SIGS : Step("Gathering the counterparty's signature.") {
        override fun childProgressTracker() = CollectSignaturesFlow.tracker()
      }
    }

    object FINALISING_TRANSACTION : Step("Obtaining notary signature and recording transaction.") {
      override fun childProgressTracker() = FinalityFlow.tracker()
    }

    override val progressTracker = ProgressTracker(
      BUILD_METERING_INVOICE_TRANSACTION,
      VERIFYING_TRANSACTION,
      SIGNING_TRANSACTION,
      GATHERING_SIGS,
      FINALISING_TRANSACTION
    )


    @Suspendable
    override fun call(): SignedTransaction {

      checkAndExtractInvoiceAttributes()
      val partiallySignedTx = buildTransaction()
      val signatureParties = getSignatureParties()
      val fullySignedTx = getSignatureFromReceiver(partiallySignedTx, signatureParties.toList())
      val finalisedTx = finaliseTransaction(fullySignedTx)
      return finalisedTx

    }

    fun checkAndExtractInvoiceAttributes() {

      val checker = MeteringInvoiceConsistencyChecker(originalInvoices = FlowUtils.getOriginalMeteringInvoices(serviceHub, payMeteringInvoiceRequests.map { it.meteredTransactionId }))

      invoicedParty = checker.getInvoicedParty()
      invoiceNotary = checker.getInvoiceNotary()
      notarisingNotary = checker.getNotarisingNotary()
      daoParty = checker.getDaoParty()
      toAccount = checker.getPayAccount()
      tokenType = checker.getTokenType()

      // finally, check the consistency of the fromAccount param on the supplied invoice payment requests
      try {
        fromAccount = payMeteringInvoiceRequests.groupBy { it.fromAccount }.asSequence().single().key
      } catch (e: Exception) {
        throw IllegalArgumentException("every payment request must have the same from address")
      }

    }

    /** Build the transaction. For each of the supplied invoices add an input state an output state.
     *  Then add a payment (token moves) to the transaction to cover all the invoices
     */

    fun buildTransaction(): SignedTransaction {

      progressTracker.currentStep = BUILD_METERING_INVOICE_TRANSACTION

      var txBuilder = TransactionBuilder(notarisingNotary)

      val invoiceSigningKeys = mutableListOf<PublicKey>()
      val tokenSigningKeys = mutableListOf<PublicKey>()

      var totalAmountToPay: Int = 0

      payMeteringInvoiceRequests.forEach {

        val originalMeteringInvoiceStateAndRef = FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties

        val meteringInvoiceProperties = MeteringInvoiceProperties(
            meteringState = MeteringState.PAID,
            invoicedParty = originalMeteringInvoiceProperties.invoicedParty,
            invoicingNotary = originalMeteringInvoiceProperties.invoicingNotary,
            daoParty = originalMeteringInvoiceProperties.daoParty,
            payAccountId = originalMeteringInvoiceProperties.payAccountId,
            amount = originalMeteringInvoiceProperties.amount,
            tokenDescriptor = originalMeteringInvoiceProperties.tokenDescriptor,
            meteredTransactionId = originalMeteringInvoiceProperties.meteredTransactionId,
            invoiceId = originalMeteringInvoiceProperties.invoiceId,
            reissueCount = originalMeteringInvoiceProperties.reissueCount,
            createdDateTime = Instant.now())

        val inputState = originalMeteringInvoiceStateAndRef
        txBuilder.addInputState(inputState)

        val meteringInvoiceStateToIssue = MeteringInvoiceState(meteringInvoiceProperties, originalMeteringInvoiceProperties.daoParty)
        val outputState = StateAndContract(meteringInvoiceStateToIssue, MeteringInvoiceContract.METERING_CONTRACT_ID)
        txBuilder.withItems(outputState)

        // accumulate other information necessary to make payment
        invoiceSigningKeys.addAll(meteringInvoiceStateToIssue.participants.map { it.owningKey })
        totalAmountToPay += meteringInvoiceProperties.amount

      }

      /* we need to add a single payment (set of token states) to the transaction
       * this assumes that the input metering invoices and payment requests share the same from_address, to_address
       * and token type, so these constraints are enforced here in the single() expectations
       */
      val keys = addPaymentToTransaction(AccountAddress(fromAccount, invoicedParty.name), AccountAddress(toAccount, daoParty.name), totalAmountToPay, tokenType, txBuilder)
      tokenSigningKeys.addAll(keys)

      // Add a single command to the transacton (required), with the keys required to sign
      val txCommand = Command(MeteringInvoiceCommands.Pay(), invoiceSigningKeys)
      txBuilder.addCommand(txCommand)

      // finally, execute the contract verification logic and sign the transaction with the token keys and our own nodes' signature
      val partiallySignedTx = verifyAndSignTransaction(txBuilder, tokenSigningKeys.toSet() + serviceHub.myInfo.legalIdentities.first().owningKey)
      return partiallySignedTx
    }

    fun addPaymentToTransaction(fromAddress: AccountAddress, toAddress: AccountAddress, totalAmountToPay: Int, tokenType: TokenType.Descriptor, txBuilder: TransactionBuilder) : List<PublicKey> {
      val amountInTokenType = Amount.fromDecimal(BigDecimal(totalAmountToPay), tokenType)
      val tokenSigningKeys = prepareTokenMoveWithSummary(txBuilder,fromAddress,toAddress,amountInTokenType,serviceHub, ourIdentity, "metering invoice flow - payment transaction")
      return tokenSigningKeys
    }

    // return the union set of DAOs and notaries appearing on the supplied invoices
    fun getSignatureParties() : Set<Party>{

      val signatureParties = mutableSetOf<Party>()

      payMeteringInvoiceRequests.forEach {
        val originalMeteringInvoiceStateAndRef =  FlowUtils.getOriginalMeteringInvoice(serviceHub, it.meteredTransactionId)
        val originalMeteringInvoiceProperties = originalMeteringInvoiceStateAndRef.state.data.meteringInvoiceProperties
        signatureParties.addAll(setOf(originalMeteringInvoiceProperties.daoParty, originalMeteringInvoiceProperties.invoicingNotary))
      }
      return signatureParties
    }


    @Suspendable
    fun verifyAndSignTransaction(txBuilder: TransactionBuilder, signingPubKeys: Iterable<PublicKey>): SignedTransaction {
      progressTracker.currentStep = VERIFYING_TRANSACTION
      txBuilder.verify(serviceHub)
      progressTracker.currentStep = SIGNING_TRANSACTION
      val partiallySignedTx = serviceHub.signInitialTransaction(txBuilder, signingPubKeys.distinct())
      return partiallySignedTx
    }

    @Suspendable
    fun getSignatureFromReceiver(partiallySignedTx: SignedTransaction, parties : List<Party>): SignedTransaction {
      progressTracker.currentStep = GATHERING_SIGS

      val flowSessions = parties.map { initiateFlow(it) }

      val fullySignedTx = subFlow(CollectSignaturesFlow(partiallySignedTx, flowSessions.toSet(), GATHERING_SIGS.childProgressTracker()))
      return fullySignedTx
    }

    @Suspendable
    fun finaliseTransaction(fullySignedTx: SignedTransaction): SignedTransaction {
      progressTracker.currentStep = FINALISING_TRANSACTION
      val finalisedTx = subFlow(FinalityFlow(fullySignedTx, FINALISING_TRANSACTION.childProgressTracker()))
      return finalisedTx
    }
  }

  @InitiatedBy(MeteringInvoicePayer::class)
  class MeteringInvoicePayee(val otherPartyFlow: FlowSession) : FlowLogic<SignedTransaction>() {
    @Suspendable
    override fun call(): SignedTransaction {
      val signTransactionFlow = object : SignTransactionFlow(otherPartyFlow) {
        override fun checkTransaction(stx: SignedTransaction) = requireThat {

          // for each output invoice state in the transaction, check the validity of the transaction
          stx.tx.outputs.filter { it.data is MeteringInvoiceState }.forEach {
            val output = it.data
            val meteringInvoiceState = output as MeteringInvoiceState
            "check that this this is a valid transaction we are being metered for" using (MeteringInvoiceFlowChecks.isTheMeteredTransactionValid(meteringInvoiceState, serviceHub, log))
          }
        }
      }
      return subFlow(signTransactionFlow)
    }


  }
}
