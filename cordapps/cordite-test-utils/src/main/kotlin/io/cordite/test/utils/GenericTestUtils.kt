/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import io.vertx.core.Future
import net.corda.core.context.InvocationContext
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.StartableByService
import net.corda.core.internal.FlowStateMachine
import net.corda.core.messaging.FlowHandle
import net.corda.core.messaging.FlowHandleImpl
import net.corda.core.messaging.FlowProgressHandle
import net.corda.core.messaging.FlowProgressHandleImpl
import net.corda.core.node.AppServiceHub
import net.corda.core.node.ServiceHub
import net.corda.core.utilities.getOrThrow
import net.corda.node.services.api.StartedNodeServices
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.MockServices
import net.corda.testing.node.StartedMockNode
import net.corda.testing.node.internal.startFlow
import net.corda.testing.node.makeTestIdentityService
import rx.Observable

fun forTheLoveOfGodIgnoreThisBit() {
  Thread.sleep(100)
}

fun <T> run(network: MockNetwork, call: () -> Future<T>): T {
  val future = call()
  while (!future.isComplete) {
    network.runNetwork()
  }
  if (future.failed()) {
    throw RuntimeException(future.cause())
  }
  return future.result()
}

val ledgerServices = MockServices(
    // A list of packages to scan for cordapps
    cordappPackages = listOf("io.cordite"),
    // The identity represented by this set of mock services. Defaults to a test identity.
    // You can also use the alternative parameter initialIdentityName which accepts a
    // [CordaX500Name]
    initialIdentity = proposerIdentity,
    // An implementation of IdentityService, which contains a list of all identities known
    // to the node. Use [makeTestIdentityService] which returns an implementation of
    // [InMemoryIdentityService] with the given identities
    identityService = makeTestIdentityService(proposerIdentity.identity)
)

class TempHackedAppServiceHubImpl(private val mockNode: StartedMockNode) : AppServiceHub, ServiceHub by mockNode.services {

  val serviceHub = mockNode.services // used for a meta hack in AppService.transaction

  override fun <T> startTrackedFlow(flow: FlowLogic<T>): FlowProgressHandle<T> {
    val stateMachine = startFlowChecked(flow)
    return FlowProgressHandleImpl(
        id = stateMachine.id,
        returnValue = stateMachine.resultFuture,
        progress = stateMachine.logic.track()?.updates ?: Observable.empty()
    )
  }

  override fun <T> startFlow(flow: FlowLogic<T>): FlowHandle<T> {
    val sns = mockNode.services as StartedNodeServices
    val stateMachine = sns.startFlow(flow)
    return FlowHandleImpl(id = stateMachine.id, returnValue = stateMachine.resultFuture)
  }

  private fun <T> startFlowChecked(flow: FlowLogic<T>): FlowStateMachine<T> {
    val logicType = flow.javaClass
    require(logicType.isAnnotationPresent(StartableByService::class.java)) { "${logicType.name} was not designed for starting by a CordaService" }
    // TODO check service permissions https://gitlab.com/cordite/cordite/issues/272
    // TODO switch from myInfo.legalIdentities[0].name to current node's identity as soon as available https://gitlab.com/cordite/cordite/issues/273
    val context = InvocationContext.service("dodgy mc dodge face", myInfo.legalIdentities[0].name)
    val sns = mockNode.services as StartedNodeServices
    return sns.startFlow(flow, context).getOrThrow()
  }
}