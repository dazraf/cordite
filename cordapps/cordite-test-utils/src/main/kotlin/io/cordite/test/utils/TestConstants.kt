/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import net.corda.core.crypto.entropyToKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.core.TestIdentity
import java.math.BigInteger
import java.security.KeyPair

val daoKey: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(314)) }
val daoParty : Party get() =  Party(CordaX500Name("Dao", "London", "GB"), daoKey.public)

val newMemberKey: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(315)) }
val newMemberName = CordaX500Name("NewMember", "London", "GB")
val newMemberParty : Party get() =  Party(newMemberName, newMemberKey.public)

val proposerKey: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(316)) }
val proposerName = CordaX500Name("Proposer", "London", "GB")
val proposerParty : Party get() =  Party(proposerName, proposerKey.public)
val proposerIdentity = TestIdentity(proposerName)

val member1Key: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(317)) }
val member1Party : Party get() =  Party(CordaX500Name("Member1", "London", "GB"), member1Key.public)

val anotherMemberKey: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(318)) }
val anotherMemberName = CordaX500Name("AnotherMember", "London", "GB")
val anotherMemberParty : Party get() =  Party(anotherMemberName, anotherMemberKey.public)

val voterKey: KeyPair by lazy { entropyToKeyPair(BigInteger.valueOf(319)) }
val voterParty : Party get() =  Party(CordaX500Name("Voter", "London", "GB"), voterKey.public)


