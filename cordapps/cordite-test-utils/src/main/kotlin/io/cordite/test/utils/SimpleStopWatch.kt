/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.test.utils

import io.cordite.commons.utils.contextLogger

class SimpleStopWatch {

  private val timings = mutableListOf<Pair<String, Long>>()

  companion object {
    private val log = contextLogger()

    fun elapse(callback: () -> Unit, message: String): Long {
      val elapsed = elapse(callback)
      log.info("$message took $elapsed milliseconds")
      return elapsed
    }

    fun elapse(callback: () -> Unit): Long {
      var start = System.currentTimeMillis()
      callback()
      return System.currentTimeMillis() - start
    }
  }

  fun elapseAndRecord(callback: () -> Unit, message: String): Long {
    val elapsed = elapse(callback, message)
    saveTiming(message, elapsed)
    return elapsed
  }

  fun logOutTimings() {
    timings.forEach {
      log.info("${it.second} : ${it.first} ")
    }
  }

  fun clearTimings() {
    timings.clear()
  }

  private fun saveTiming(message: String, elapsedTime: Long) {
    val elapsedTiming = Pair(message, elapsedTime)
    timings.add(elapsedTiming)
  }
}

