/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao

import io.bluebank.braid.corda.serialisation.BraidCordaJacksonInit
import io.cordite.dao.core.DaoKey
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.ProposalContract
import io.cordite.dao.proposal.ProposalState
import io.cordite.test.utils.*
import io.vertx.core.json.Json
import net.corda.core.contracts.CommandWithParties
import net.corda.core.identity.Party
import org.junit.Assert
import org.junit.Test


class ProposalStateTest {
  private fun proposalState(name: String = "proposal", description: String = "desc", proposer: Party = proposerParty,
                            supporters: Set<Party> = setOf(proposerParty), members: Set<Party> = setOf(proposerParty, newMemberParty),
                            daoKey: DaoKey = DaoKey("theDao")) = ProposalState(NormalProposal(name, description), proposer, supporters, members, daoKey)

  @Test
  fun `should be a valid proposal if supporter % is equal to target %`() {
    Assert.assertTrue("proposal should be valid", proposalState().isValid(0.5))
  }

  @Test
  fun `should be a valid proposal if supporter % is greater than target %`() {
    Assert.assertTrue("proposal should be valid", proposalState().isValid(0.4))
  }

  @Test
  fun `should not be a valid proposal if supporter % is less than target %`() {
    Assert.assertFalse("proposal should not be valid", proposalState().isValid(0.6))
  }

  @Test
  fun `check signers happy path`() {
    proposalState().checkSignersForProposal(CommandWithParties(listOf(proposerKey.public, newMemberKey.public), listOf(proposerParty, newMemberParty),
        ProposalContract.Commands.AcceptProposal()))
  }

  @Test
  fun `proposer must be signer`() {
    try {
      proposalState().checkSignersForProposal(CommandWithParties(listOf(newMemberKey.public), listOf(proposerParty), ProposalContract.Commands.AcceptProposal()))
      Assert.fail("should have blown up")
    } catch (e: IllegalArgumentException) {
      Assert.assertEquals("proposer not signer message incorrect", "Failed requirement: proposer must be signer", e.message)
    }
  }

  @Test
  fun `supporters must be members`() {
    try {
      proposalState(supporters = setOf(voterParty)).checkSignersForProposal(CommandWithParties(listOf(proposerKey.public, newMemberKey.public), listOf(proposerParty, newMemberParty), ProposalContract.Commands.AcceptProposal()))
      Assert.fail("should have blown up")
    } catch (e: IllegalArgumentException) {
      Assert.assertEquals("supporters must be members", "Failed requirement: supporters must be members", e.message)
    }
  }

  @Test
  fun `all members must be signers`() {
    try {
      proposalState(proposer = newMemberParty).checkSignersForProposal(CommandWithParties(listOf(newMemberKey.public), listOf(newMemberParty), ProposalContract.Commands.AcceptProposal()))
      Assert.fail("should have blown up")
    } catch (e: IllegalArgumentException) {
      Assert.assertEquals("all members must be signers", "Failed requirement: members must be signers", e.message)
    }
  }

  @Test
  fun `should be able to serialise and deserialise proposal state`() {
    val proposalState = proposalState()
    BraidCordaJacksonInit.init()
    Assert.assertEquals("proposalStates should be the same", proposalState, Json.decodeValue(Json.encode(proposalState), ProposalState::class.java))
  }

}
