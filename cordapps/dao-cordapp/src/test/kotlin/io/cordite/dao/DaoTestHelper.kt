/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao

import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.proposal.NormalProposal
import io.cordite.dao.proposal.ProposalState
import io.cordite.test.utils.proposerParty
import net.corda.core.identity.Party
import org.junit.Assert

fun daoState(name: String = "dao", members: Set<Party> = setOf(proposerParty)) = DaoState(name, members)

fun membershipState(members: Set<Party> = setOf(proposerParty)) = MembershipState(members, MembershipKey("myDao"))

fun proposalState(name: String = "proposal", description: String = "desc", proposer: Party = proposerParty,
                  supporters: Set<Party> = setOf(proposerParty), members: Set<Party> = setOf(proposerParty),
                  daoKey: DaoKey = DaoKey("theDao")) = ProposalState(NormalProposal(name, description), proposer, supporters, members, daoKey)

fun proposalCopy(proposalState: ProposalState<*>, proposer: Party = proposalState.proposer, members: Set<Party> = proposalState.members,
                 supporters: Set<Party> = proposalState.supporters) = ProposalState(proposalState.proposal, proposer, supporters, members, proposalState.daoKey)

fun daoCordappPackages(): List<String> {
  return listOf(DaoApi::class.java.`package`.name, "io.cordite")
}

fun assertDaoStateContainsMembers(daoStates: List<DaoState>, vararg parties: Party) {
  Assert.assertEquals("there should be one casa dao post creation", 1, daoStates.size)
  Assert.assertEquals("there should be ${parties.size} members", parties.size, daoStates.first().members.size)
  parties.forEach {
    Assert.assertTrue("members should include party: $it", daoStates.first().members.contains(it))
  }
}