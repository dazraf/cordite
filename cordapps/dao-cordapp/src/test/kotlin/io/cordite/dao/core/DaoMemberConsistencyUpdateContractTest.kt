/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.dao.*
import io.cordite.dao.proposal.PROPOSAL_CONTRACT_ID
import io.cordite.dao.proposal.ProposalContract
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test


// NB we cannot really check that all the DaoState members are participants in the current scheme, so members
// must check this in the responder core - discussion in CreateProposalFlow
class DaoMemberConsistencyUpdateContractTest {

  private val initialProposal = proposalState()
  private val outputProposalState = initialProposal.copyWithNewMember(newMemberParty)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, newMemberKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        verifies()
      }
    }
  }

  @Test
  fun `there should be one input proposal`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("There should be one input proposal")
      }
    }
  }


  @Test
  fun `there should be one output proposal`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(listOf(proposerKey.public, voterKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `can't have more signers than dao members`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposal, members = setOf(proposerParty)))
        command(listOf(proposerKey.public, voterKey.public, newMemberKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("only the dao members must be signers")
      }
    }
  }

  @Test
  fun `all the dao members must be signers`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(proposerKey.public, ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("only the dao members must be signers")
      }
    }
  }

  @Test
  fun `output members and input members must overlap`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, initialProposal.copyReplacingMembers(setOf(newMemberParty)))
        command(listOf(proposerKey.public, newMemberKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("output members and input members must overlap")
      }
    }
  }

  @Test
  fun `proposers should be the same`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(outputProposalState, proposer = daoParty))
        command(listOf(proposerKey.public, newMemberKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("proposers should be the same")
      }
    }
  }

  @Test
  fun `supporters should be correct`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposal)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(outputProposalState, supporters = setOf(voterParty)))
        command(listOf(proposerKey.public, newMemberKey.public), ProposalContract.Commands.DaoMemberConsistencyUpdate())
        `fails with`("supporters should be correct")
      }
    }
  }

}