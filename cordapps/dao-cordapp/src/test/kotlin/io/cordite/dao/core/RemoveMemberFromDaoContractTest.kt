/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.dao.daoState
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test

class RemoveMemberFromDaoContractTest {

  private val originalState = daoState().copyWith(MembershipModelData(MembershipKey("daoName"), minimumMemberCount = 1, hasMinNumberOfMembers = true, strictMode = true)).copyWith(newMemberParty)
  private val outputState = originalState.copyWithout(newMemberParty)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState)
        command(proposerKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        verifies()
      }
    }
  }

  @Test
  fun `should blow up if we are in strict mode and try to reduce minimum number of members`() {
    val originalState = daoState().copyWith(MembershipModelData(MembershipKey("daoName"), minimumMemberCount = 2, hasMinNumberOfMembers = true, strictMode = true)).copyWith(newMemberParty)
    val outputState = originalState.copyWithout(newMemberParty)
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState.copyWith(originalState.membershipModelData().copyWithMinMembers(false)))
        command(proposerKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("cannot drop below minimum number of members in strict mode")
      }
    }
  }

  @Test
  fun `there should be one input state`() {
    ledgerServices.ledger {
      transaction {
        output(DAO_CONTRACT_ID, originalState)
        command(proposerKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("there should be one input state")
      }
    }
  }

  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        val daoState = daoState()
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, daoState)
        output(DAO_CONTRACT_ID, daoState)
        command(proposerKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("there should be one output state")
      }
    }
  }

  @Test
  fun `inputState should contain all of the output state members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState.copyWith(anotherMemberParty))
        command(proposerKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("inputState should contain all of the output state members")
      }
    }
  }

  @Test
  fun `outputState should have one fewer members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, originalState)
        command(newMemberKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("outputState should have one fewer members")
      }
    }
  }

  @Test
  fun `removeMember should be in input members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState)
        command(newMemberKey.public, DaoContract.Commands.RemoveMember(anotherMemberParty))
        `fails with`("removeMember should be in input members")
      }
    }
  }

  @Test
  fun `removeMember should not be in output members`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState)
        command(newMemberKey.public, DaoContract.Commands.RemoveMember(proposerParty))
        `fails with`("removeMember should not be in output members")
      }
    }
  }

  @Test
  fun `exactly members must be signers`() {
    ledgerServices.ledger {
      transaction {
        input(DAO_CONTRACT_ID, originalState)
        output(DAO_CONTRACT_ID, outputState)
        command(newMemberKey.public, DaoContract.Commands.RemoveMember(newMemberParty))
        `fails with`("exactly members must be signers")
      }
    }
  }
}