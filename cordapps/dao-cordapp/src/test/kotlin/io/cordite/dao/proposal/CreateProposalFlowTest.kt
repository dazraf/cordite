/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
//package io.cordite.dao.core
//
//import io.cordite.dao.*
//import net.corda.core.utilities.getOrThrow
//import net.corda.node.internal.StartedNode
//import net.corda.testing.DUMMY_NOTARY_KEY
//import net.corda.testing.getDefaultNotary
//import net.corda.testing.node.MockNetwork
//import net.corda.testing.node.MockNetwork.MockNode
//import net.corda.testing.node.MockServices
//import net.corda.testing.unsetCordappPackages
//import org.junit.After
//import org.junit.Assert
//import org.junit.Before
//import org.junit.Test
//
//class CreateProposalFlowTest {
//    private lateinit var network: MockNetwork
//    private lateinit var proposer: StartedNode<MockNode>
//    private lateinit var member1: StartedNode<MockNode>
//    private lateinit var proposerServices: MockServices
//    private lateinit var member1Services: MockServices
//    private lateinit var notaryServices: MockServices
//
//    @Before
//    fun setup() {
//        setDaoCordappPackages()
//        network = MockNetwork()
//
//        proposerServices = MockServices(proposerKey)
//        member1Services = MockServices(member1Key)
//        notaryServices = MockServices(DUMMY_NOTARY_KEY)
//
//        val nodes = network.createSomeNodes(3)
//
//        proposer = nodes.partyNodes[0]
//        member1 = nodes.partyNodes[1]
//
//        nodes.partyNodes.forEach {
//            it.registerInitiatedFlow(CreateProposalFlowResponder::class.java)
//        }
//
//        network.runNetwork()
//    }
//
//    @After
//    fun tearDown() {
//        network.stopNodes()
//        unsetCordappPackages()
//    }
//
//    @Test
//    fun `should be able to create normal proposal`() {
//        val daoKey = createDao()
//        val flow = CreateProposalFlow(NormalProposal("newProposal", "some description"), proposer.info.legalIdentities.first(), daoKey)
//        val future = proposer.services.startFlow(flow).resultFuture
//        network.runNetwork()
//        val results = future.getOrThrow()
//        Assert.assertEquals("newProposal", results.name)
//    }
//
//    private fun createDao(name: String = "theDao"): DaoKey {
//        val proposerParty = proposer.info.legalIdentities.first()
//        val otherMemberParty = member1.info.legalIdentities.first()
//        val notary = proposer.services.getDefaultNotary()
//
//        // create Dao with 1 member
//        val (daoState, txBuilder) = DaoContract.generateCreate(name, proposerParty, notary)
//        val tx = proposerServices.signInitialTransaction(txBuilder)
//        notaryServices.addSignature(tx)
//
//        // move Dao to having 2 members
//
//        // first create a proposal, accept
//        val (pps, ptxBuilder) = ProposalContract.generateCreateProposal(MemberProposal(otherMemberParty, "theDao"), daoState, proposerParty, notary)
//        val ptx1  = member1Services.signInitialTransaction(ptxBuilder)
//        proposerServices.addSignature(ptx1)
//
//        // accept proposal
//        val (aps, ptxBuilder2) = ProposalContract.generateAcceptProposal<MemberProposal>(ptx1.tx.outRef(0), tx.tx.outRef(0), notary)
//        val ptx2  = member1Services.signInitialTransaction(ptxBuilder2)
//        proposerServices.addSignature(ptx1)
//
//
//        val (_, txBuilder2) = DaoContract.generateAddMember(ptx2.tx.outRef(0), ptx2.tx.outRef(1), notary)
//        val tx2 = proposerServices.signInitialTransaction(txBuilder2)
//        member1Services.addSignature(tx2)
//        notaryServices.addSignature(tx2)
//
//        proposer.database.transaction {
//            proposer.services.recordTransactions(tx)
//            proposer.services.recordTransactions(ptx1)
//            proposer.services.recordTransactions(ptx2)
//            proposer.services.recordTransactions(tx2)
//        }
//
//        member1.database.transaction {
//            member1.services.recordTransactions(tx)
//            member1.services.recordTransactions(ptx1)
//            member1.services.recordTransactions(ptx2)
//            member1.services.recordTransactions(tx2)
//        }
//
//        return daoState.daoKey
//    }
//
//}