/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import io.cordite.dao.coop.Address
import io.cordite.dao.coop.AddressLine
import io.cordite.dao.coop.CoopModelData
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.test.utils.newMemberParty
import net.corda.core.serialization.deserialize
import net.corda.core.serialization.serialize
import net.corda.nodeapi.internal.serialization.KRYO_CHECKPOINT_CONTEXT
import net.corda.testing.core.SerializationEnvironmentRule
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class DaoStateTest {

  @Rule
  @JvmField
  val testSerialization = SerializationEnvironmentRule()

  @Test(expected = IllegalStateException::class)
  fun `should blow up if we ask for a membership model data and there isn't one`() {
    val daoState = DaoState("someDao", emptySet())
    daoState.membershipModelData()
  }

  @Test
  fun serialisationTest() {
    val mmd = MembershipModelData(MembershipKey("theDao"))
    val cmd = CoopModelData("some objects", Address(listOf(AddressLine("an address line"))))
    val daoState = DaoState("theDao", setOf(newMemberParty)).copyWith(mmd).copyWith(cmd)

    val bytes = daoState.serialize(context = KRYO_CHECKPOINT_CONTEXT)
    val deserialized = bytes.deserialize(context = KRYO_CHECKPOINT_CONTEXT)

    Assert.assertEquals("membership model data incorrect", mmd, deserialized.get(MembershipModelData::class))
  }

}