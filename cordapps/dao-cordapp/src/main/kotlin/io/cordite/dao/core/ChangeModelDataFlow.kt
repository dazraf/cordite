/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import io.cordite.commons.utils.contextLogger
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
class ChangeModelDataFlow(private val proposal: ModelDataProposal, val daoKey: DaoKey): FlowLogic<DaoState>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): DaoState {
    val me = serviceHub.myInfo.legalIdentities.first()
    val daoName = daoKey.name

    val proposalStateAndRef = getAndCheckProposalState(proposal.proposalKey)
    val existingDaoTxStateAndRef = daoState(daoName)
    val existingMembershipStateAndRef = DataHelper.membershipStateAndRefFor(daoKey, serviceHub)

    val notary = existingDaoTxStateAndRef.state.notary

    val (outputDaoState, txBuilder) = DaoContract.generateChangeModelData(existingDaoTxStateAndRef, proposalStateAndRef, notary)
    MembershipContract.generateReferToMembershipState(existingMembershipStateAndRef, txBuilder)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)
    val signers = existingDaoTxStateAndRef.state.data.members + outputDaoState.members
    val flowSessions = signers.filter{ it != me }.map{ initiateFlow(it) }

    val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))
    log.info("-----------------------------")
    val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx))
    log.info("$proposal applied to $daoName in tx $finalTx")

    return outputDaoState
  }

  private fun daoState(daoName: String): StateAndRef<DaoState> {
    val vaultPage = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(externalId = listOf(daoName)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of DAOs with name: $daoName found (${vaultPage.totalStatesAvailable}")
      throw IllegalStateException("there should only be one Dao with a given name")
    }

    return vaultPage.states.first()
  }

  private fun getAndCheckProposalState(proposalKey: ProposalKey): StateAndRef<ProposalState<ModelDataProposal>> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(proposalKey.uniqueIdentifier)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of unspent proposal states with key: $proposalKey found (${vaultPage.totalStatesAvailable}")
      throw IllegalStateException("incorrect number of unspent proposal states with key: $proposalKey found (${vaultPage.totalStatesAvailable}")
    }
    @Suppress("UNCHECKED_CAST")
    val stateAndRef = vaultPage.states.first() as StateAndRef<ProposalState<ModelDataProposal>>
    val proposalState = stateAndRef.state.data

    if (proposalState.lifecycleState != ProposalLifecycle.ACCEPTED) {
      log.warn("proposal state for $proposalKey is not ACCEPTED")
      throw IllegalStateException("proposal state for $proposalKey is not ACCEPTED")
    }

    return stateAndRef
  }

}

@InitiatedBy(ChangeModelDataFlow::class)
class ChangeModelDataFlowResponder(val proposerSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {

    val signTransactionFlow = object : SignTransactionFlow(proposerSession, SignTransactionFlow.tracker()) {
      override fun checkTransaction(stx: SignedTransaction) = requireThat {
        // the proposal has already been accepted and the dao state verified
        log.info("making model data change - already verified")
      }
    }

    val stx = subFlow(signTransactionFlow)

    waitForLedgerCommit(stx.id)
  }

}