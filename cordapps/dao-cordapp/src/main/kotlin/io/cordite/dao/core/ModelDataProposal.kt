/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalState
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class ModelDataProposal(val name: String, val newModelData: ModelData, val proposalKey: ProposalKey = ProposalKey("$name:${newModelData::class.java.canonicalName}")) : Proposal {

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>) {
    "proposer must be only initial supporter" using (state.supporters == setOf(state.proposer))
  }

  override fun verify(state: ProposalState<*>) {
    "supporters must be members" using (state.members.containsAll(state.supporters))
  }

  override fun initialSupporters(proposer: Party): Set<Party> {
    return setOf(proposer)
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    flowLogic.subFlow(ChangeModelDataFlow(this, proposalState.daoKey))
    newModelData.handleAcceptanceOnAcceptor(proposalState, inputDao, flowLogic)
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    newModelData.handleAcceptanceAllButAcceptingMembers(proposalState, inputDao, flowLogic)
  }

}