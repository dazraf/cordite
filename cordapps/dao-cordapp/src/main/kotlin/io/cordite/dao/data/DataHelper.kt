/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.data

import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalLifecycle
import io.cordite.dao.proposal.ProposalState
import net.corda.core.contracts.StateAndRef
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.QueryCriteria
import org.slf4j.Logger
import org.slf4j.LoggerFactory

// currently hacking around with v. inefficient ways of getting data out of the db
// fine for the moment, but obvs nonsense later
// put all the methods in one place to aid refactoring later...

// presumes you are inside a tx...

object DataHelper {

  val log: Logger = LoggerFactory.getLogger("DataHelper")

  fun daoStates(serviceHub: ServiceHub): List<DaoState> {
    val daoStates = serviceHub.vaultService.queryBy(DaoState::class.java)
    log.info("daoStates: $daoStates")
    return daoStates.states.map { it.state.data }
  }

  fun daoTransactionStates(serviceHub: ServiceHub): List<StateAndRef<DaoState>> {
    val daoStates = serviceHub.vaultService.queryBy(DaoState::class.java)
    log.info("daoStates: $daoStates")
    return daoStates.states
  }

  fun daoStatesFor(daoName: String, serviceHub: ServiceHub): List<DaoState> {
    val daoStates = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(externalId = listOf(daoName)))
    log.info("daoStates: $daoStates")
    return daoStates.states.map { it.state.data }
  }

  fun daoStateAndRefFor(daoKey: DaoKey, serviceHub: ServiceHub): StateAndRef<DaoState> {
    val daoStates = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(daoKey.uniqueIdentifier)))
    log.info("daoStates: $daoStates")
    return daoStates.states.first() // there can be only one (highlander!)
  }

  fun daoStateFor(daoKey: DaoKey, serviceHub: ServiceHub): DaoState {
    return daoStateAndRefFor(daoKey, serviceHub).state.data
  }

  fun daoStateFor(daoName: String, serviceHub: ServiceHub): DaoState {
    val daos = daoStatesFor(daoName, serviceHub)
    return when {
      daos.size > 1 -> {
        log.warn("i belong to two daos with the same name...not good")
        throw IllegalStateException("i belong to two daos with the same name: $daoName")
      }
      daos.isEmpty() -> {
        log.warn("i don't belong to dao $daoName")
        throw IllegalStateException("i don't belong to dao $daoName")
      }
      else -> {
        daos.first()
      }
    }
  }

  fun membershipStateAndRefFor(daoKey: DaoKey, serviceHub: ServiceHub): StateAndRef<MembershipState> {
    val daoState = daoStateFor(daoKey, serviceHub)
    return membershipStateAndRefFor(daoState.get(MembershipModelData::class)!!.membershipKey, serviceHub)
  }

  fun membershipStateFor(daoKey: DaoKey, serviceHub: ServiceHub): MembershipState {
    return membershipStateAndRefFor(daoKey, serviceHub).state.data
  }

  fun membershipStateAndRefFor(membershipKey: MembershipKey, serviceHub: ServiceHub): StateAndRef<MembershipState> {
    val membershipVaultPage = serviceHub.vaultService.queryBy(MembershipState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(membershipKey.uniqueIdentifier)))
    val membershipStates = membershipVaultPage.states
    log.info("membership states: $membershipStates")
    return when {
      membershipStates.size > 1 -> {
        log.warn("two membership states with 1 linear id: $membershipKey")
        throw IllegalStateException("two membership states with 1 linear id: $membershipKey")
      }
      membershipStates.isEmpty() -> {
        log.warn("no MembershipState with id $membershipKey")
        throw IllegalStateException("no MembershipState with id $membershipKey")
      }
      else -> {
        membershipStates.first()
      }
    }
  }

  inline fun <reified T : Proposal> proposalStatesFor(daoKey: DaoKey, serviceHub: ServiceHub): List<ProposalState<T>> {
    val proposalStates = serviceHub.vaultService.queryBy(ProposalState::class.java)
    log.info("proposalStates: $proposalStates")
    @Suppress("UNCHECKED_CAST")
    return proposalStates.states.map { it.state.data }.filter { it.daoKey == daoKey }.filter { it.proposal is T } as List<ProposalState<T>>
  }

  // TODO make this less horrific...and work with large numbers of proposals - https://gitlab.com/cordite/cordite/issues/284
  fun findProposalKeysWithInconsistentDaoMembers(daoKey: DaoKey, serviceHub: ServiceHub): Set<ProposalKey> {
    val daoState = daoStateFor(daoKey, serviceHub)
    val currentMembers = daoState.members
    val proposalStates = serviceHub.vaultService.queryBy(ProposalState::class.java)
    return proposalStates.states.map { it.state.data }.filter { it.daoKey == daoKey }.filter { it.lifecycleState == ProposalLifecycle.OPEN }.filter { it.members != currentMembers }.map { it.proposal.key() }.toSet()
  }

  fun proposalStateAndRefsFor(keys: Set<ProposalKey>, serviceHub: ServiceHub): Set<StateAndRef<ProposalState<*>>> {
    if (keys.isEmpty()) return emptySet()
    val linearIds = keys.map { it.uniqueIdentifier }
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = linearIds))
    if (vaultPage.states.size != keys.size) {
      throw IllegalStateException("wanted ${keys.size} states but there were: ${vaultPage.states.size}")
    }
    return vaultPage.states.toSet()
  }

  fun proposalKeys(serviceHub: ServiceHub): List<ProposalKey> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java)
    return vaultPage.states.map{ it.state.data.proposal.key() }
  }

  fun proposalFor(proposalKey: ProposalKey, serviceHub: ServiceHub): ProposalState<out Proposal> {
    return proposalStateAndRefsFor(setOf(proposalKey), serviceHub).first().state.data
  }

}