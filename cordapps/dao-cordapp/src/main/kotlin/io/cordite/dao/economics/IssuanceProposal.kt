/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.economics

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.core.DaoState
import io.cordite.dao.proposal.Proposal
import io.cordite.dao.proposal.ProposalKey
import io.cordite.dao.proposal.ProposalState
import io.cordite.commons.utils.contextLogger
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.Token
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.flows.IssueTokensFlow
import io.cordite.dgl.corda.token.flows.TransferTokenFlow
import io.cordite.dgl.corda.token.issuedBy
import net.corda.core.contracts.Amount
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.math.BigDecimal

@CordaSerializable
data class IssuanceProposal(val tokenType: TokenType.Descriptor, val amount: String, val proposalKey: ProposalKey = ProposalKey("Issuance:${tokenType.symbol}:$amount")) : Proposal {

  companion object {
    private val log = contextLogger()
  }

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>) {
    "proposer must be only initial supporter" using (state.supporters == setOf(state.proposer))
  }

  override fun verify(state: ProposalState<*>) {
    "supporters must be members" using (state.members.containsAll(state.supporters))
  }

  override fun initialSupporters(proposer: Party): Set<Party> {
    return setOf(proposer)
  }

  @Suspendable
  override fun handleAcceptanceOnAcceptor(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    handleIssuanceAndTransferOnlyOnIssuer(proposalState, inputDao, flowLogic, notary)
  }

  @Suspendable
  override fun handleAcceptanceAllButAcceptingMembers(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    handleIssuanceAndTransferOnlyOnIssuer(proposalState, inputDao, flowLogic, notary)
  }

  @Suspendable
  private fun handleIssuanceAndTransferOnlyOnIssuer(proposalState: ProposalState<*>, inputDao: DaoState, flowLogic: FlowLogic<*>, notary: Party) {
    val myIdentity = flowLogic.ourIdentity
    when (tokenType.issuerName) {
      myIdentity.name -> {
        log.info("${tokenType.issuerName} is me, so issuing $amount tokens and transferring")
        val amountTokenType = Amount.fromDecimal(BigDecimal(amount), tokenType)
        val accountId = inputDao.name
        val token = Token.generateIssuance(amountTokenType.issuedBy(myIdentity.ref(1)), accountId, myIdentity, "Token issuance")
        val flow = IssueTokensFlow(token = token, notary = notary, description = "Token issuance")
        flowLogic.subFlow(flow)

        val splitAmount = amountTokenType.splitEvenly(inputDao.members.size).first() // issuer can have the last one, others ought to be the same
        val fromAccount = AccountAddress(accountId, myIdentity.name)
        val from = listOf(fromAccount).zip(listOf(splitAmount))

        inputDao.members.minus(myIdentity).forEach {
          val to = listOf(AccountAddress(accountId, it.name)).zip(listOf(splitAmount))
          flowLogic.subFlow(
            TransferTokenFlow(from = from, to = to,
              description = "issuance transfer of ${splitAmount.quantity} to ${it.name}", notary = notary))
        }
      }
      else -> {
        log.info("not the issuer, so ignoring issuance acceptance...")
      }
    }
  }
}