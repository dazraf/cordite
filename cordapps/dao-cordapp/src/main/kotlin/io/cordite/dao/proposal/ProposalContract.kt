/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.proposal

import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoKey
import io.cordite.dao.core.DaoState
import io.cordite.dao.core.ModelDataProposal
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipState
import net.corda.core.contracts.*
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.TransactionBuilder

val PROPOSAL_CONTRACT_ID = "io.cordite.dao.proposal.ProposalContract"

open class ProposalContract : Contract {

  companion object {
    // TODO: can we refactor this a lot??!! https://gitlab.com/cordite/cordite/issues/280

    fun <T : Proposal> generateCreateProposal(proposal: T, daoKey: DaoKey, membershipState: MembershipState, proposer: Party, notary: Party): Pair<ProposalState<T>, TransactionBuilder> {
      val initialSupporters = proposal.initialSupporters(proposer)
      val participants = membershipState.members + initialSupporters

      val newProposalState = ProposalState(proposal, proposer, initialSupporters, membershipState.members, daoKey)
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.CreateProposal(), participants.map { it.owningKey }.toList())

      val txBuilder = TransactionBuilder(notary = notary)
      txBuilder.withItems(newProposalContractAndState, cmd)

      return Pair(newProposalState, txBuilder)
    }

    fun generateVoteForProposal(inputProposalStateTxState: StateAndRef<ProposalState<Proposal>>, newSupporter: Party, membershipState: MembershipState, notary: Party): Pair<ProposalState<Proposal>, TransactionBuilder> {
      val newProposalState = inputProposalStateTxState.state.data.copyWithNewSupporter(newSupporter)
      if (newProposalState.members != membershipState.members) {
        throw IllegalStateException("vote state has incorrect members...")
      }
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.VoteForProposal(newSupporter), newProposalState.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(notary = notary)
      txBuilder.withItems(inputProposalStateTxState, newProposalContractAndState, cmd)

      return Pair(newProposalState, txBuilder)
    }

    fun generateAcceptProposal(inputProposalStateTxState: StateAndRef<ProposalState<Proposal>>, inputMembershipStateAndRef: StateAndRef<MembershipState>, referenceDaoStateAndRef: StateAndRef<DaoState>): Pair<ProposalState<Proposal>, TransactionBuilder> {
      val newProposalState = inputProposalStateTxState.state.data.copyWithNewLifecycleState(ProposalLifecycle.ACCEPTED)
      if (newProposalState.members != inputMembershipStateAndRef.state.data.members) {
        throw IllegalStateException("vote state has incorrect members...")
      }
      val newProposalContractAndState = StateAndContract(newProposalState, PROPOSAL_CONTRACT_ID)
      val cmd = Command(Commands.AcceptProposal(), newProposalState.participants.map { it.owningKey })

      val txBuilder = TransactionBuilder(referenceDaoStateAndRef.state.notary)
      MembershipContract.generateReferToMembershipState(inputMembershipStateAndRef, txBuilder) // this is because we need access to the current set of members
      DaoContract.generateReferToDao(referenceDaoStateAndRef, txBuilder) // this is for the membership model data params

      txBuilder.withItems(inputProposalStateTxState, newProposalContractAndState, cmd)

      return Pair(newProposalState, txBuilder)
    }

    fun generateMemberConsistencyProposal(inputProposalStateTxStates: Set<StateAndRef<ProposalState<*>>>, membershipState: MembershipState, notary: Party): TransactionBuilder {
      val newProposalStates = inputProposalStateTxStates.map { it.state.data.copyReplacingMembers(membershipState.members) }
      val txBuilder = TransactionBuilder(notary = notary)
      // only dao members need to sign these
      val cmd = Command(Commands.DaoMemberConsistencyUpdate(), membershipState.members.map { it.owningKey })
      txBuilder.withItems(*inputProposalStateTxStates.toTypedArray(), cmd)
      newProposalStates.forEach {
        if (it.members != membershipState.members) {
          throw IllegalStateException("proposal $it has incorrect members...")
        }
        txBuilder.addOutputState(it, PROPOSAL_CONTRACT_ID)
      }

      return txBuilder
    }

  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<Commands>()
    val groups = tx.groupStates { it: ProposalState<*> -> it.proposal.key() }

    for ((inputs, outputs, _) in groups) {

      when (command.value) {
        is Commands.CreateProposal -> {
          requireThat {
            "no inputs should be consumed when creating a proposal" using (inputs.isEmpty())
            "there should be one output proposal" using (outputs.size == 1)
            val outputProposal = outputs.first()
            outputProposal.proposal.verifyCreate(outputProposal)

            outputProposal.checkSignersForProposal(command)
          }
        }

        is Commands.VoteForProposal -> {
          requireThat {
            val voteForProposal = command.value as Commands.VoteForProposal

            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "no members should be removed" using (outputProposal.members.containsAll(inputProposal.members))
            "supporters should be old set plus new supporter" using (outputProposal.supporters == inputProposal.supporters + voteForProposal.newSupporter)

            outputProposal.checkSignersForProposal(command)
          }
        }

        is Commands.AcceptProposal -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "no members should be removed" using (outputProposal.members.containsAll(inputProposal.members))

            val membershipStates = tx.outputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()
            "the proposal state members must be the same as the membership state's members" using (outputProposal.members == membershipState.members)

            outputProposal.checkSignersForProposal(command)

            val daoStates = tx.outputsOfType(DaoState::class.java)
            "there must be a reference DaoState" using (daoStates.size == 1)  // NB this is implicitly the proposal has a dao

            val daoState = daoStates.first()
            "the dao must have a proposal model data" using (daoState.containsModelData(ProposalModelData::class))

            val proposalModelData = daoState.get(ProposalModelData::class)!!
            "the proposal must have enough support" using (outputProposal.isValid(proposalModelData.ordinaryVoteThreshold))

            val membershipModelData = daoState.membershipModelData()
            if (membershipModelData.strictMode && inputProposal.proposal !is MemberProposal) {
              "cannot accept non member proposals unless dao has min number of members in strict mode" using (daoState.membershipModelData().hasMinNumberOfMembers)
            }
          }
        }

      // currently only relevant for NewMemberProposals
        is Commands.ConsumeProposal -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be no output proposals" using (outputs.isEmpty())

            val inputProposal = inputs.first()
            "inputProposal must be accepted" using (inputProposal.lifecycleState == ProposalLifecycle.ACCEPTED)
            "inputProposal must be new member proposal" using (inputProposal.proposal is MemberProposal || inputProposal.proposal is ModelDataProposal)

            val membershipStates = tx.inputsOfType(MembershipState::class.java)
            "there must be a corresponding MembershipState" using (membershipStates.size == 1)  // NB this is implicitly the proposal has a dao

            val membershipState = membershipStates.first()
            "the proposal state members must be the same as the input membership state's members" using (inputProposal.members == membershipState.members)

            inputProposal.checkSignersForProposal(command)
            "the proposal must have enough support" using (inputProposal.isValid(0.6)) // do something sensible with the magic number!
          }
        }

        is Commands.DaoMemberConsistencyUpdate -> {
          requireThat {
            "there should be one input proposal" using (inputs.size == 1)
            "there should be one output proposal" using (outputs.size == 1)

            val inputProposal = inputs.first()
            val outputProposal = outputs.first()

            val originalMembers = inputProposal.members
            val outputMembers = outputProposal.members

            val removedMembers = originalMembers - outputMembers

            "output members and input members must overlap" using (outputMembers.intersect(originalMembers).isNotEmpty())
            "only the dao members must be signers" using (command.signers.containsAll(outputMembers.map { it.owningKey }) && command.signers.size == outputMembers.size)

            "proposers should be the same" using (inputProposal.proposer == outputProposal.proposer)
            "supporters should be correct" using (outputProposal.supporters == inputProposal.supporters - removedMembers)
          }
        }

        else -> throw IllegalArgumentException("unrecognised command: ${command.value}")
      }
    }
  }

  // Used to indicate the transaction's intent.
  interface Commands : CommandData {
    class CreateProposal : TypeOnlyCommandData(), Commands
    class VoteForProposal(val newSupporter: Party) : Commands
    class AcceptProposal : TypeOnlyCommandData(), Commands
    class DaoMemberConsistencyUpdate : TypeOnlyCommandData(), Commands
    class ConsumeProposal : TypeOnlyCommandData(), Commands
  }

}