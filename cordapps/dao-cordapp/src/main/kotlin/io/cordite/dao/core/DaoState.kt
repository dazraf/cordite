/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.core

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.cordite.dao.economics.EconomicsModelData
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.proposal.ProposalModelData
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.util.*
import kotlin.reflect.KClass

@CordaSerializable
data class DaoKey(val name: String, val uuid: UUID = UUID.randomUUID()) {
  val uniqueIdentifier: UniqueIdentifier = UniqueIdentifier(name, uuid)
}

@JsonIgnoreProperties("linearId")
@CordaSerializable
data class DaoState(val name: String, val members: Set<Party>, val daoKey: DaoKey = DaoKey(name), val modelDataMap: Map<String, ModelData> = mapOf()) : LinearState {

  override val participants: List<AbstractParty> = members.toList()

  fun <T : ModelData> containsModelData(key: KClass<T>): Boolean {
    return modelDataMap.containsKey(key.qualifiedName)
  }

  @Suppress("UNCHECKED_CAST")
  fun <T : ModelData> get(key: KClass<T>): T? {
    return modelDataMap[key.qualifiedName] as T?
  }
  
  override val linearId: UniqueIdentifier
    get() = daoKey.uniqueIdentifier

  fun membershipModelData(): MembershipModelData {
    if (!containsModelData(MembershipModelData::class)) {
      throw IllegalStateException("must have a membership model data instance for a dao to function")
    } else {
      return get(MembershipModelData::class)!!
    }
  }

  @Suspendable
  fun propogateModelDataEvent(event: ModelDataEvent, flowLogic: FlowLogic<*>, notary: Party){
// NB this doesn't work at all! serialisation issues
//    modelDataMap.values.forEach { it.handleEvent(event, this, flowLogic, notary) }

    modelDataMap.values.toList().forEach { it.handleEvent(event, this, flowLogic, notary) }
  }

  fun copyWith(newMember: Party): DaoState {
    return DaoState(name, members + newMember, daoKey, modelDataMap)
  }

  fun copyWithout(oldMember: Party): DaoState {
    return DaoState(name, members - oldMember, daoKey, modelDataMap)
  }

  fun copyWith(newModelData: ModelData): DaoState {
    val newModelDataSet = modelDataMap.toMutableMap()
    newModelDataSet[newModelData::class.qualifiedName!!] = newModelData
    return DaoState(name, members, daoKey, newModelDataSet)
  }

  fun copyWith(modelDataSet: Set<ModelData>): DaoState {
    val newModelDataSet = modelDataMap.toMutableMap()
    modelDataSet.forEach {
      newModelDataSet[it::class.qualifiedName!!] = it
    }
    return DaoState(name, members, daoKey, newModelDataSet)
  }
}