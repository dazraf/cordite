/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import io.cordite.dao.core.BaseModelData
import io.cordite.dao.core.DaoContract
import io.cordite.dao.core.DaoState
import net.corda.core.contracts.Requirements.using
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class MembershipModelData(val membershipKey: MembershipKey,
                               val minimumMemberCount: Int = 3,
                               val hasMinNumberOfMembers: Boolean = false,
                               val strictMode: Boolean = true) : BaseModelData() {

  companion object {
    fun nonStrictMembershipModelData(name: String): MembershipModelData {
      return MembershipModelData(MembershipKey(name), minimumMemberCount = 1, strictMode = false)
    }
  }

  fun copyWithMinMembers(value: Boolean): MembershipModelData {
    return MembershipModelData(membershipKey, minimumMemberCount, value)
  }

  override fun verify(oldDaoState: DaoState, newDaoState: DaoState, command: DaoContract.Commands) {
    when (command) {
      is DaoContract.Commands.RemoveMember -> "cannot drop below minimum number of members in strict mode" using (oldDaoState.membershipModelData().hasMinNumberOfMembers && strictMode && hasMinNumberOfMembers)
    }
    "hasMinNumberOfMembers should be correct" using (hasMinNumberOfMembers == minimumMemberCount <= newDaoState.members.size)
  }

}