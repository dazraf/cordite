/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.integration

import io.bluebank.braid.corda.services.SimpleNetworkMapService
import io.bluebank.braid.core.async.getOrThrow
import io.cordite.dao.*
import io.cordite.dao.util.contextLogger
import io.cordite.test.utils.*
import io.vertx.ext.unit.junit.VertxUnitRunner
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith

//TODO - decide if we need to replace of refactor https://gitlab.com/cordite/cordite/issues/277
class TestNode(port: Int, host: String) {

    private val braidClient = BraidClientHelper.braidClient(port, "dao", host)

    private val networkMapBraidClient = BraidClientHelper.braidClient(port, "network", host)
    val networkService = networkMapBraidClient.bind(SimpleNetworkMapService::class.java)
    val party: Party = networkService.myNodeInfo().legalIdentities.first()
    val daoService = braidClient.bind(DaoService::class.java)


}

@RunWith(VertxUnitRunner::class)

class DaoLanIntegrationTest {

    companion object {
        private val log = contextLogger()
        private val casaDaoBase = "casaDao-${System.currentTimeMillis()}"
        //TODO add the host name to the base name https://gitlab.com/cordite/cordite/issues/278
        private val proposalBase = "proposal"

        private lateinit var proposer: TestNode
        private lateinit var newMember: TestNode
        private lateinit var anotherMember: TestNode

        private lateinit var notaryName : CordaX500Name

        private var salt = 0

        @BeforeClass @JvmStatic
        fun setup() {
            proposer = TestNode(8081, "localhost")
            newMember = TestNode(8082, "localhost")
            anotherMember = TestNode(8083, "localhost")

            notaryName = proposer.networkService.notaryIdentities().first().name
        }
    }
    private val saltedDaoName = casaDaoBase + salt++
    private val saltedProposalName = proposalBase + salt++

    @Test
    fun `should be able to create a dao`() {
        createDaoWithName(saltedDaoName, notaryName)
    }

    @Test
    fun `should be able to create another dao in the same node`() {
        // because the salt changes for each test run this is actually a different dao..
        createDaoWithName(saltedDaoName, notaryName)
    }

    @Test
    fun `should be able to add a new member`() {
        createDaoWithName(saltedDaoName, notaryName)
        val proposal = newMember.daoService.createNewMemberProposal(saltedDaoName, proposer.party).getOrThrow()
        Assert.assertEquals("should be 2 supporters", 2, proposal.supporters.size)
        Assert.assertTrue("proposer should be a supporter", proposal.supporters.contains(proposer.party))
        Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMember.party))

        // accept member proposal
        val x = newMember.daoService.acceptNewMemberProposal(proposal.proposal.key(), proposer.party).getOrThrow()
        Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED,x.first.lifecycleState)

        assertDaoStateContainsMembers(getDaoWithRetry(newMember), proposer.party, newMember.party)
    }

    @Test
    fun `should be able to add two members`() {
        createDaoWithName(saltedDaoName, notaryName)
        addMemberToDao(newMember, proposer, saltedDaoName)
        addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    }

    @Test
    fun `should be able to create, vote for and accept new proposal`() {
        val daoState = createDaoWithName(saltedDaoName, notaryName)
        addMemberToDao(newMember, proposer, saltedDaoName)
        createAndAcceptProposalWithName(saltedProposalName, proposer, daoState, newMember)
    }

    @Test
    fun `new member should receive proposals`() {
        val daoState = createDaoWithName(saltedDaoName, notaryName)
        createProposal(saltedProposalName, proposer, daoState)
        addMemberToDao(newMember, proposer, saltedDaoName)
        val proposals = newMember.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
        Assert.assertEquals("should have correct proposal", saltedProposalName, proposals.first().name)
    }

    @Test
    fun`removing member from dao should remove them from proposals`() {
        val daoState = createDaoWithName(saltedDaoName, notaryName)
        addMemberToDao(newMember, proposer, saltedDaoName)
        addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
        createProposal(saltedProposalName, proposer, daoState)
        val proposals = newMember.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
        Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)

        val removeProposal = proposer.daoService.createRemoveMemberProposal(daoState.daoKey, newMember.party).getOrThrow()

        Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
        Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

        // other member approves
        anotherMember.daoService.voteForMemberProposal(removeProposal.proposal.proposalKey).getOrThrow()

        val (acceptedProposal, _) = proposer.daoService.acceptRemoveMemberProposal(removeProposal.proposal.key()).getOrThrow()
        Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

        assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

        val currentProposals = proposer.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
        Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
    }

    @Test
    fun `removing member from dao should remove them from proposal supporters`() {
        val daoState = createDaoWithName(saltedDaoName, notaryName)
        addMemberToDao(newMember, proposer, saltedDaoName)
        addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
        val proposal = createProposal(saltedProposalName, proposer, daoState)

        // support the proposal
        forTheLoveOfGodIgnoreThisBit()
        newMember.daoService.voteForProposal(proposal.proposal.proposalKey).getOrThrow()

        val proposals = newMember.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
        Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)
        Assert.assertTrue("new member should be supporter", proposals.first().supporters.contains(newMember.party))

        val removeProposal = proposer.daoService.createRemoveMemberProposal(daoState.daoKey, newMember.party).getOrThrow()

        Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
        Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

        // other member approves
        anotherMember.daoService.voteForMemberProposal(removeProposal.proposal.proposalKey).getOrThrow()

        val (acceptedProposal, _) = proposer.daoService.acceptRemoveMemberProposal(removeProposal.proposal.key()).getOrThrow()
        Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

        assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

        val currentProposals = proposer.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
        Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
        Assert.assertFalse("new member should not now be a supporter", currentProposals.first().supporters.contains(newMemberParty))

    }

    private fun createDaoWithName(daoName: String, notaryName : CordaX500Name) : DaoState{
        Assert.assertEquals("there should be no casa dao at the beginning", 0, proposer.daoService.daoInfo(daoName).size)
        val daoState = proposer.daoService.createDao(daoName,notaryName).getOrThrow()
        forTheLoveOfGodIgnoreThisBit()
        assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
        return daoState
    }

    private fun getDaoWithRetry(testNode: TestNode): List<DaoState> {
        (1..5).forEach {
            log.info("trying to get dao list")
            val daos = testNode.daoService.daoInfo(saltedDaoName)
            if (daos.isNotEmpty()) {
                log.info("phew - daos returned")
                return daos
            }
            log.info("dao state not arrived yet - snoozing")
            Thread.sleep(500)
        }
        throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
    }

    @Test
    fun `should be able to create a metering model data for a dao`() {
      val daoState = createDaoWithName(saltedDaoName, notaryName)
      addMemberToDao(newMember, proposer, saltedDaoName)

      Assert.assertFalse(daoState.containsModelData(SampleModelData::class))

      val newModelData = SampleModelData("initial")
      val proposalState = proposer.daoService.createModelDataProposal("fiddle with metering model data", newModelData, daoState.daoKey).getOrThrow()

      newMember.daoService.voteForModelDataProposal(proposalState.proposal.proposalKey).getOrThrow()

      val proposals = proposer.daoService.modelDataProposalsFor(daoState.daoKey)
//        println(proposals.first().supporters)

      Assert.assertEquals("there should only be one proposal", 1, proposals.size)
      Assert.assertEquals("the proposal should have 2 supporters", 2, proposals.first().supporters.size)
      Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposer.party, newMember.party)))

        val acceptedProposal = run(network) { proposer.daoApi.acceptModelDataProposal(proposalState.proposal.proposalKey) }
        Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)
        val newDaoState = proposer.daoApi.daoFor(acceptedProposal.daoKey)
        Assert.assertEquals("dao should contain metering model data", newModelData, newDaoState.get(newModelData::class))
    }

    private fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, vararg extraSigners: TestNode) {
        val proposal = newMemberNode.daoService.createNewMemberProposal(daoName, proposerNode.party).getOrThrow()

        Assert.assertEquals("should be 2 supporters", 2, proposal.supporters.size)
        Assert.assertTrue("proposer should be a supporter", proposal.supporters.contains(proposerNode.party))
        Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMemberNode.party))

        var numberOfSupporters = 2
        extraSigners.forEach {
            val postVote = it.daoService.voteForMemberProposal(proposal.proposal.proposalKey).getOrThrow()
            Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.supporters.size)
        }

        // accept member proposal
        val acceptedProposal = newMember.daoService.acceptNewMemberProposal(proposal.proposal.key(), proposerNode.party).getOrThrow()
        Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.first.lifecycleState)

        assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party, *extraSigners.map{ it.party}.toTypedArray())
    }

    private fun createAndAcceptProposalWithName(proposalName: String, proposalProposer: TestNode, daoState: DaoState, vararg supporters: TestNode) {
        val proposal = createProposal(proposalName, proposalProposer, daoState)

        supporters.forEach {
            it.daoService.voteForProposal(proposal.proposal.proposalKey).getOrThrow()
        }

        val proposals = proposalProposer.daoService.normalProposalsFor(daoState.daoKey)

        Assert.assertEquals("there should only be one proposal", 1, proposals.size)
        Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().supporters.size)
        Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

        val acceptedProposal = proposalProposer.daoService.acceptProposal(proposal.proposal.proposalKey).getOrThrow()
        Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)
    }

    private fun createProposal(proposalName: String, proposalProposer: TestNode, daoState: DaoState): ProposalState<NormalProposal> {
        val proposal = proposalProposer.daoService.createProposal(proposalName, "some description", daoState.daoKey).getOrThrow()

        val origProposals = proposalProposer.daoService.normalProposalsFor(daoState.daoKey)
        Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
        Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().supporters.size)
        Assert.assertTrue("proposer should be supporter", origProposals.first().supporters.containsAll(setOf(proposalProposer.party)))
        Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
        return proposal
    }

    private fun assertDaoStateContainsMembers(daoStates: List<DaoState>, vararg parties: Party) {
        Assert.assertEquals("there should be one casa dao post creation", 1, daoStates.size)
        Assert.assertEquals("there should be ${parties.size} members", parties.size, daoStates.first().members.size)
        parties.forEach {
            Assert.assertTrue("members should include party: $it", daoStates.first().members.contains(it))
        }
    }
}