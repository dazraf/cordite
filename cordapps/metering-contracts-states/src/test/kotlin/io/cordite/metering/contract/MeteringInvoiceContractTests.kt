/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.token.Token
import io.cordite.dgl.corda.token.TokenType
import io.cordite.dgl.corda.token.issuedBy
import io.cordite.metering.daostate.MeteringFeeAllocation
import io.cordite.metering.daostate.MeteringFeeAllocator
import io.cordite.metering.tesutils.TestUtils.Companion.TestTokenDescriptor
import io.cordite.test.utils.ledgerServices
import net.corda.core.contracts.Amount
import net.corda.core.crypto.generateKeyPair
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.transactions.TransactionBuilder
import net.corda.testing.node.ledger
import org.junit.Test
import java.math.BigDecimal
import java.time.Instant

class MeteringInvoiceContractTests {

  val meteringNotaryKeys = generateKeyPair()
  val meteringNotaryParty = Party(CordaX500Name("MeteringNotary", "MeteringNotary", "Sector 1", "GB"), meteringNotaryKeys.public)

  val corditeBusinessNodeKeys = generateKeyPair()
  val corditeBusinessNodeParty = Party(CordaX500Name("Flourish and Blotts", "Florish and Blotts", "Sector 1", "GB"), corditeBusinessNodeKeys.public)

  val daoNodeKeys = generateKeyPair()
  val daoNodeParty = Party(CordaX500Name("Cordite dao", "Cordite dao", "Sector 1", "GB"), daoNodeKeys.public)

  val guardianNotaryKeys = generateKeyPair()
  val guardianNotaryParty = Party(CordaX500Name("Guardian Notary", "Guardian Notary", "Sector 2","GB"), guardianNotaryKeys.public)

  val meteringFeeAllocation = MeteringFeeAllocation("daoHoldingAccount", 50, 40, 10, "daoFoundationAccount")

  val daoAccountName = "dao-fund-acc1"

  val issuedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName, // was "ACDCFORTHOSABOUTTORICK"
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.ISSUED,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val issuedMeteringInvoiceState = MeteringInvoiceState(issuedMeteringInvoice,corditeBusinessNodeParty)

  val issuedMeteringInvoice2 = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering2",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 50,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.ISSUED,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val issuedMeteringInvoiceState2 = MeteringInvoiceState(issuedMeteringInvoice2, corditeBusinessNodeParty)

  val dodgyIssuedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty, ""),
    amount = 1000,
    payAccountId = "",
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.ISSUED,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val dodgyIssuedMeteringInvoiceState = MeteringInvoiceState(dodgyIssuedMeteringInvoice, corditeBusinessNodeParty)

  val reIssuedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.REISSUED,
    reissueCount = 1,
    createdDateTime = Instant.now())

  val reIssuedMeteringInvoiceState = MeteringInvoiceState(reIssuedMeteringInvoice, corditeBusinessNodeParty)

  val disputedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.IN_DISPUTE,
    invoiceId = issuedMeteringInvoice.invoiceId,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val disputedMeteringInvoiceState = MeteringInvoiceState(disputedMeteringInvoice, meteringNotaryParty)

  val paidMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.PAID,
    invoiceId = issuedMeteringInvoice.invoiceId,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val paidMeteringInvoiceState = MeteringInvoiceState(paidMeteringInvoice, daoNodeParty)

  val reissuedPaidMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.PAID,
    invoiceId = issuedMeteringInvoice.invoiceId,
    reissueCount = 1,
    createdDateTime = Instant.now())

  val reissuedPaidMeteringInvoiceState = MeteringInvoiceState(reissuedPaidMeteringInvoice, daoNodeParty)

  val paidMeteringInvoice2 = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering2",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 50,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.PAID,
    invoiceId = issuedMeteringInvoice2.invoiceId,
    reissueCount = 0,
    createdDateTime = Instant.now())

  val paidMeteringInvoiceState2 = MeteringInvoiceState(paidMeteringInvoice2, daoNodeParty)

  val reissuedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.REISSUED,
    invoiceId = issuedMeteringInvoice.invoiceId,
    reissueCount = 1,
    createdDateTime = Instant.now())

  val reissuedMeteringInvoiceState = MeteringInvoiceState(reissuedMeteringInvoice, corditeBusinessNodeParty)

  val reissuedByWrongNotaryMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
      tokenDescriptor = TestTokenDescriptor(daoNodeParty),
      amount = 200,
      payAccountId = daoAccountName,
      daoParty = daoNodeParty,
      invoicingNotary = guardianNotaryParty, // wrong notary
      invoicedParty = corditeBusinessNodeParty,
      meteringState = MeteringState.REISSUED,
      invoiceId = issuedMeteringInvoice.invoiceId,
      reissueCount = 1,
      createdDateTime = Instant.now())

  val reissuedByWrongNotaryMeteringInvoiceState = MeteringInvoiceState(reissuedByWrongNotaryMeteringInvoice, corditeBusinessNodeParty)

  val splitMeteringInvoice1 = MeteringInvoiceSplitProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 100,
    allocations = MeteringFeeAllocator.AllocateFeesFromAmount(100, meteringFeeAllocation),
    finalAccountId = "JUSTGIVEMETHEEFFINGMONEY",
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
    finalParty = meteringNotaryParty,
    createdDateTime = Instant.now())

  val splitMeteringInvoiceState1 = MeteringInvoiceSplitState(splitMeteringInvoice1, daoNodeParty)

  val splitMeteringInvoice2 = MeteringInvoiceSplitProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 80,
    allocations = MeteringFeeAllocator.AllocateFeesFromAmount(80, meteringFeeAllocation),
    finalAccountId = "MONEYTHATSWHATIWANT",
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
    finalParty = daoNodeParty,
    createdDateTime = Instant.now())

  val splitMeteringInvoiceState2 = MeteringInvoiceSplitState(splitMeteringInvoice2, daoNodeParty)

  val splitMeteringInvoice3 = MeteringInvoiceSplitProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 20,
    allocations = MeteringFeeAllocator.AllocateFeesFromAmount(20, meteringFeeAllocation),
    finalAccountId = "MONEYGETAWAY",
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringSplitState = MeteringSplitState.SPLIT_ISSUED,
    finalParty = guardianNotaryParty,
    createdDateTime = Instant.now())

  val splitMeteringInvoiceState3 = MeteringInvoiceSplitState(splitMeteringInvoice3, daoNodeParty)


  val dispersedMeteringInvoice = MeteringInvoiceProperties(meteredTransactionId = "ACandidateTransactionForMetering",
    tokenDescriptor = TestTokenDescriptor(daoNodeParty),
    amount = 200,
    payAccountId = daoAccountName,
    daoParty = daoNodeParty,
    invoicingNotary = meteringNotaryParty,
    invoicedParty = corditeBusinessNodeParty,
    meteringState = MeteringState.FUNDS_DISPERSED,
    invoiceId = issuedMeteringInvoice.invoiceId,
    reissueCount = 1,
    createdDateTime = Instant.now())

  val dispersedMeteringInvoiceState = MeteringInvoiceState(dispersedMeteringInvoice,daoNodeParty)

  val splitMeteringInvoice1_dispersed = splitMeteringInvoice1.copy(meteringSplitState = MeteringSplitState.SPLIT_DISPERSED)
  val splitMeteringInvoiceState1_dispersed = MeteringInvoiceSplitState(splitMeteringInvoice1_dispersed,meteringNotaryParty)

  val splitMeteringInvoice2_dispersed = splitMeteringInvoice2.copy(meteringSplitState = MeteringSplitState.SPLIT_DISPERSED)
  val splitMeteringInvoiceState2_dispersed = MeteringInvoiceSplitState(splitMeteringInvoice2_dispersed,meteringNotaryParty)

  val splitMeteringInvoice3_dispersed = splitMeteringInvoice3.copy(meteringSplitState = MeteringSplitState.SPLIT_DISPERSED)
  val splitMeteringInvoiceState3_dispersed = MeteringInvoiceSplitState(splitMeteringInvoice3_dispersed,meteringNotaryParty)

  val xtsTokenType = TokenType.State("XTS",2, daoNodeParty)
  val issue150 = "150"
  val amountTokenType150 = Amount.fromDecimal(BigDecimal(issue150), xtsTokenType.descriptor)

  val issue75 = "75"
  val amountTokenType75 = Amount.fromDecimal(BigDecimal(issue75), xtsTokenType.descriptor)

  val gbpTokenType = TokenType.State("GBP",2, daoNodeParty)
  val amountTokenType75GBP = Amount.fromDecimal(BigDecimal(issue75), gbpTokenType.descriptor)

  val transfer200 = "200"
  val amountTokenType200 = Amount.fromDecimal(BigDecimal(transfer200), xtsTokenType.descriptor)

  val change25 = "25"
  val amountTokenType25 = Amount.fromDecimal(BigDecimal(change25), xtsTokenType.descriptor)

  val issue250 = "250"
  val amountTokenType250 = Amount.fromDecimal(BigDecimal(issue250), xtsTokenType.descriptor)

  val businessAccountId = "biz-account-aac1"
  val fullBusinessAccountName ="${WellKnownTagCategories.DGL_ID}:$businessAccountId@${corditeBusinessNodeParty.name}"

  val token150BusinessNode = Token.generateIssuance(amountTokenType150.issuedBy(daoNodeParty.ref(1)), fullBusinessAccountName, corditeBusinessNodeParty, "Dao Issuing to BusinessNode 125 XTS")
  val token75BusinessNode = Token.generateIssuance(amountTokenType75.issuedBy(daoNodeParty.ref(1)), fullBusinessAccountName, corditeBusinessNodeParty, "Dao Issuing to BusinessNode 75 XTS")
  val token75GBPBusinessNode = Token.generateIssuance(amountTokenType75GBP.issuedBy(daoNodeParty.ref(1)), fullBusinessAccountName, corditeBusinessNodeParty, "Dao Issuing to BusinessNode 75 GBP")
  val token250BusinessNode = Token.generateIssuance(amountTokenType250.issuedBy(daoNodeParty.ref(1)), fullBusinessAccountName, corditeBusinessNodeParty, "Dao Issuing to Business Node 250 XTS")

  val daoFullAccountName = "${WellKnownTagCategories.DGL_ID}:$daoAccountName@${daoNodeParty.name}"
  val daoWrongAccountId = "dao-wrong-account"
  val daoFullWrongAccountName ="${WellKnownTagCategories.DGL_ID}:$daoWrongAccountId@${daoNodeParty.name}"

  val token200PaidToDao = Token.generateIssuance(amountTokenType200.issuedBy(daoNodeParty.ref(1)), daoFullAccountName, daoNodeParty, "Business Node paying to Dao")
  val token200PaidToWrongAccount = Token.generateIssuance(amountTokenType200.issuedBy(daoNodeParty.ref(1)), daoFullWrongAccountName , daoNodeParty, "Business Node paying to Dao")
  val token25ReturnedToBusinessNode = Token.generateIssuance(amountTokenType25.issuedBy(daoNodeParty.ref(1)), daoFullAccountName, daoNodeParty, "Change Returned to Business Node")
  val token250PaidToDao = Token.generateIssuance(amountTokenType250.issuedBy(daoNodeParty.ref(1)), daoFullAccountName, daoNodeParty, "Business Node paying to Dao for two transactions")


  @Test
  fun `you can issue a metering invoice`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, issuedMeteringInvoiceState)
        command(corditeBusinessNodeParty.owningKey, MeteringInvoiceCommands.Issue())
        this.verifies()
      }
    }
  }

  @Test
  fun `you can't issue a bad metering invoice`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, dodgyIssuedMeteringInvoiceState)
        command(corditeBusinessNodeParty.owningKey, MeteringInvoiceCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you can't notarise your own metering invoice`() {
    ledgerServices.ledger {

      // set the notary in the transaction to the same as the notary in the metering invoice  properties -  this should fail the contract checks
      transaction( transactionBuilder = TransactionBuilder(notary = issuedMeteringInvoiceState.meteringInvoiceProperties.invoicingNotary)) {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, issuedMeteringInvoiceState)
        command(corditeBusinessNodeParty.owningKey, MeteringInvoiceCommands.Issue())
        this.fails()
      }
    }
  }

  @Test
  fun `you can dispute a metering invoice`() {
    ledgerServices.ledger {
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1", issuedMeteringInvoiceState)
        command(corditeBusinessNodeParty.owningKey, MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(meteringNotaryParty.owningKey, MeteringInvoiceCommands.Dispute())
        input("issuedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, disputedMeteringInvoiceState)
        verifies()
      }
    }
  }

  @Test
  fun `you can pay an invoice using the DGL`() {

    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken75BusinessNode",token75BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",  issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), MeteringInvoiceCommands.Pay())
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, paidMeteringInvoiceState)
        output(token200PaidToDao.contractId, token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId,token25ReturnedToBusinessNode)
        verifies()
      }
    }
  }

  @Test
  fun `you cant pay an invoice to the wrong account`() {

    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken75BusinessNode",token75BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",  issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), MeteringInvoiceCommands.Pay())
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, paidMeteringInvoiceState)
        output(token200PaidToWrongAccount.contractId, token200PaidToWrongAccount)
        output(token25ReturnedToBusinessNode.contractId,token25ReturnedToBusinessNode)
        fails()
      }
    }
  }

  @Test
  fun `you cant pay an invoice to any party other than the dao`() {

    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken75BusinessNode",token75BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",  issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction{

        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), MeteringInvoiceCommands.Pay())
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, paidMeteringInvoiceState)
        output(token200PaidToWrongAccount.contractId, token200PaidToWrongAccount)
        output(token25ReturnedToBusinessNode.contractId,token25ReturnedToBusinessNode)
        fails()
      }
    }
  }



  @Test
  fun `you cant underpay an invoice`() {

    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",  issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), MeteringInvoiceCommands.Pay())
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, paidMeteringInvoiceState)
        output(token200PaidToDao.contractId, token200PaidToDao)
        fails()
      }
    }
  }

  @Test
  fun `you cant pay an invoice in the wrong currency`() {

    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(token75GBPBusinessNode.contractId)
        output(token75GBPBusinessNode.contractId, "xtsToken75GBPBusinessNode",token75GBPBusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",  issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), MeteringInvoiceCommands.Pay())
        command(listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey), Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75GBPBusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, paidMeteringInvoiceState)
        output(token200PaidToDao.contractId, token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId,token25ReturnedToBusinessNode)

        fails()
      }
    }
  }

  @Test
  fun `you can reissue an invoice but only if it's been disputed and then pay it and then split and disperse the proceeds`() {
    val amountTokenType100 = Amount.fromDecimal(BigDecimal(100), xtsTokenType.descriptor)
    val token100PaidToDao = Token.generateIssuance(
      amountTokenType100.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    val amountTokenType80 = Amount.fromDecimal(BigDecimal(80), xtsTokenType.descriptor)
    val token80PaidToDao = Token.generateIssuance(
      amountTokenType80.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    val amountTokenType120 = Amount.fromDecimal(BigDecimal(120), xtsTokenType.descriptor)
    val token120PaidToDao = Token.generateIssuance(
      amountTokenType120.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    val amountTokenType20 = Amount.fromDecimal(BigDecimal(20), xtsTokenType.descriptor)
    val token20PaidToDao = Token.generateIssuance(
      amountTokenType20.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    val amountTokenType180 = Amount.fromDecimal(BigDecimal(180), xtsTokenType.descriptor)
    val token180PaidToDao = Token.generateIssuance(
      amountTokenType180.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey,Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken75BusinessNode",token75BusinessNode)
        command(daoNodeParty.owningKey,Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(listOf(corditeBusinessNodeParty.owningKey,daoNodeParty.owningKey),MeteringInvoiceCommands.Dispute())
        input("issuedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "disputedMeteringInvoice1", disputedMeteringInvoiceState)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey),MeteringInvoiceCommands.ReIssue())
        input("disputedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "reissuedMeteringInvoice1", reissuedMeteringInvoiceState)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(corditeBusinessNodeParty.owningKey,MeteringInvoiceCommands.Pay())
        command(corditeBusinessNodeParty.owningKey,Token.Command.Move())
        input("reissuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "reissuedPaidMeteringInvoice1",reissuedPaidMeteringInvoiceState)
        output(token200PaidToDao.contractId,"daoFunds",token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId,token25ReturnedToBusinessNode)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey,MeteringInvoiceCommands.SplitInvoice())
        input("reissuedPaidMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice1", splitMeteringInvoiceState1)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice2", splitMeteringInvoiceState2)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice3", splitMeteringInvoiceState3)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "dispersedMeteringInvoice", dispersedMeteringInvoiceState)
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey,MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey,Token.Command.Move())
        input("splitMeteringInvoice1")
        input("daoFunds")
        output(token100PaidToDao.contractId, token100PaidToDao.copy(accountId = "JUSTGIVEMETHEEFFINGMONEY"))
        output(token100PaidToDao.contractId, token100PaidToDao)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice1_dispersed", splitMeteringInvoiceState1_dispersed)
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey,MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey,Token.Command.Move())
        input("splitMeteringInvoice2")
        input("daoFunds")
        output(token80PaidToDao.contractId, token80PaidToDao.copy(accountId = "MONEYTHATSWHATIWANT"))
        output(token120PaidToDao.contractId, token120PaidToDao)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice2_dispersed", splitMeteringInvoiceState2_dispersed)
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey,MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey,Token.Command.Move())
        input("splitMeteringInvoice3")
        input("daoFunds")
        output(token20PaidToDao.contractId, token20PaidToDao.copy(accountId = "MONEYGETAWAY"))
        output(token180PaidToDao.contractId, token180PaidToDao)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "splitMeteringInvoice3_dispersed", splitMeteringInvoiceState3_dispersed)
        verifies()
      }
    }
  }

  @Test
  fun `you cant reissue an invoice that hasnt been disputed`() {
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode",token150BusinessNode)
        command(daoNodeParty.owningKey,Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken50BusinessNode",token75BusinessNode)
        command(daoNodeParty.owningKey,Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1",issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(listOf(meteringNotaryParty.owningKey,daoNodeParty.owningKey),MeteringInvoiceCommands.ReIssue())
        input("issuedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "reissuedMeteringInvoice1", reissuedMeteringInvoiceState)
        fails()
      }
    }
  }

  @Test
  fun `you cant reissue an invoice from a notary that didn't issue it`() {
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(token150BusinessNode.contractId, "xtsToken150BusinessNode", token150BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(token75BusinessNode.contractId, "xtsToken75BusinessNode", token75BusinessNode)
        command(daoNodeParty.owningKey, Token.Command.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1", issuedMeteringInvoiceState)
        command(listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey), MeteringInvoiceCommands.Issue())
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(listOf(corditeBusinessNodeParty.owningKey, daoNodeParty.owningKey), MeteringInvoiceCommands.Dispute())
        input("issuedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "disputedMeteringInvoice1", disputedMeteringInvoiceState)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey), MeteringInvoiceCommands.ReIssue())
        input("disputedMeteringInvoice1")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "reissuedMeteringInvoice1", reissuedByWrongNotaryMeteringInvoiceState)
        fails()
      }
    }
  }

  @Test
  fun `you can issue and pay for several invoices at once`() {
    ledgerServices.ledger {
      transaction {
        attachment(token250BusinessNode.contractId)
        output(token250BusinessNode.contractId, "xtsToken250BusinessNode",token250BusinessNode)
        command(corditeBusinessNodeParty.owningKey,Token.Command.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice1", issuedMeteringInvoiceState)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID, "issuedMeteringInvoice2", issuedMeteringInvoiceState2)
        command(corditeBusinessNodeParty.owningKey, MeteringInvoiceCommands.Issue())
        verifies()
      }

      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token250BusinessNode.contractId)
        command(corditeBusinessNodeParty.owningKey,MeteringInvoiceCommands.Pay())
        command(corditeBusinessNodeParty.owningKey,Token.Command.Move())
        input("issuedMeteringInvoice1")
        input("issuedMeteringInvoice2")
        input("xtsToken250BusinessNode")
        output(MeteringInvoiceContract.METERING_CONTRACT_ID,paidMeteringInvoiceState)
        output(MeteringInvoiceContract.METERING_CONTRACT_ID,paidMeteringInvoiceState2)
        output(token250BusinessNode.contractId,token250PaidToDao)
        verifies()
      }
    }
  }

  @Test
  fun `the amount of tokens dispersed must be equal to the amount in the split invoice`() {
    val amountTokenType100 = Amount.fromDecimal(BigDecimal(100), xtsTokenType.descriptor)
    val token100PaidToDao = Token.generateIssuance(
      amountTokenType100.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(
          token150BusinessNode.contractId,
          "xtsToken150BusinessNode",
          token150BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(
          token75BusinessNode.contractId,
          "xtsToken75BusinessNode",
          token75BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "issuedMeteringInvoice1",
          issuedMeteringInvoiceState
        )
        command(
          listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey),
          MeteringInvoiceCommands.Issue()
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          MeteringInvoiceCommands.Pay()
        )
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          Token.Command.Move()
        )
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "paidMeteringInvoice",
          paidMeteringInvoiceState
        )
        output(token200PaidToDao.contractId, "200ToDao", token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId, token25ReturnedToBusinessNode)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.SplitInvoice())
        input("paidMeteringInvoice")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1",
          splitMeteringInvoiceState1.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1.copy(finalAccountId = daoAccountName)
          )
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice2",
          splitMeteringInvoiceState2
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice3",
          splitMeteringInvoiceState3
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "dispersedMeteringInvoice",
          dispersedMeteringInvoiceState.copy(
            meteringInvoiceProperties = dispersedMeteringInvoice.copy(reissueCount = 0)
          )
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey, Token.Command.Move())
        input("splitMeteringInvoice1")
        input("200ToDao")
        output(token100PaidToDao.contractId, token100PaidToDao)
        output(token100PaidToDao.contractId, token100PaidToDao.copy(accountId = "JUSTGIVEMETHEEFFINGMONEY"))
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1_dispersed",
          splitMeteringInvoiceState1_dispersed.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1_dispersed.copy(
              finalAccountId = daoAccountName
            )
          )
        )
        verifies()
      }
    }
  }

  @Test
  fun `the amount of tokens dispersed cannot be greater than the amount in the split invoice`() {
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(
          token150BusinessNode.contractId,
          "xtsToken150BusinessNode",
          token150BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(
          token75BusinessNode.contractId,
          "xtsToken75BusinessNode",
          token75BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "issuedMeteringInvoice1",
          issuedMeteringInvoiceState
        )
        command(
          listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey),
          MeteringInvoiceCommands.Issue()
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          MeteringInvoiceCommands.Pay()
        )
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          Token.Command.Move()
        )
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "paidMeteringInvoice",
          paidMeteringInvoiceState
        )
        output(token200PaidToDao.contractId, "200ToDao", token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId, token25ReturnedToBusinessNode)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.SplitInvoice())
        input("paidMeteringInvoice")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1",
          splitMeteringInvoiceState1.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1.copy(finalAccountId = daoAccountName)
          )
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice2",
          splitMeteringInvoiceState2
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice3",
          splitMeteringInvoiceState3
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "dispersedMeteringInvoice",
          dispersedMeteringInvoiceState.copy(
            meteringInvoiceProperties = dispersedMeteringInvoice.copy(reissueCount = 0)
          )
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey, Token.Command.Move())
        input("splitMeteringInvoice1")
        input("200ToDao")
        output(token200PaidToDao.contractId, token200PaidToDao)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1_dispersed",
          splitMeteringInvoiceState1_dispersed.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1_dispersed.copy(
              finalAccountId = daoAccountName
            )
          )
        )
        this `fails with` "amount paid must equal the invoice split amount"
      }
    }
  }

  @Test
  fun `the amount of tokens dispersed cannot be less than the amount in the split invoice`() {
    val amountTokenType20 = Amount.fromDecimal(BigDecimal("20"), xtsTokenType.descriptor)
    val token20PaidToDao = Token.generateIssuance(
      amountTokenType20.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    ledgerServices.ledger {
      transaction {
        attachment(token150BusinessNode.contractId)
        output(
          token150BusinessNode.contractId,
          "xtsToken150BusinessNode",
          token150BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(
          token75BusinessNode.contractId,
          "xtsToken75BusinessNode",
          token75BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "issuedMeteringInvoice1",
          issuedMeteringInvoiceState
        )
        command(
          listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey),
          MeteringInvoiceCommands.Issue()
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          MeteringInvoiceCommands.Pay()
        )
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          Token.Command.Move()
        )
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "paidMeteringInvoice",
          paidMeteringInvoiceState
        )
        output(token200PaidToDao.contractId, "200ToDao", token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId, token25ReturnedToBusinessNode)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.SplitInvoice())
        input("paidMeteringInvoice")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1",
          splitMeteringInvoiceState1.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1.copy(finalAccountId = daoAccountName)
          )
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice2",
          splitMeteringInvoiceState2
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice3",
          splitMeteringInvoiceState3
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "dispersedMeteringInvoice",
          dispersedMeteringInvoiceState.copy(
            meteringInvoiceProperties = dispersedMeteringInvoice.copy(reissueCount = 0)
          )
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey, Token.Command.Move())
        input("splitMeteringInvoice1")
        input("200ToDao")
        output(token20PaidToDao.contractId, token20PaidToDao)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1_dispersed",
          splitMeteringInvoiceState1_dispersed.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1_dispersed.copy(
              finalAccountId = daoAccountName
            )
          )
        )
        this `fails with` "amount paid must equal the invoice split amount"
      }
    }
  }

  @Test
  fun `the tokens included in the dispersed amount are of the same type marked in the split invoice`() {
    val amountTokenType100 = Amount.fromDecimal(BigDecimal(100), xtsTokenType.descriptor)
    val token100PaidToDao = Token.generateIssuance(
      amountTokenType100.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    val randomTokenType = TokenType.State("RANDOM", 2, daoNodeParty)
    val amountRandomTokenType200 =
      Amount.fromDecimal(BigDecimal("200"), randomTokenType.descriptor)
    val tokenRandom200BusinessNode = Token.generateIssuance(
      amountRandomTokenType200.issuedBy(daoNodeParty.ref(1)),
      fullBusinessAccountName,
      corditeBusinessNodeParty,
      "Dao Issuing to BusinessNode 200 RANDOM"
    )
    val amountRandom200PaidToDao = Token.generateIssuance(
      amountRandomTokenType200.issuedBy(daoNodeParty.ref(1)),
      daoFullAccountName,
      daoNodeParty,
      "Business Node paying to Dao"
    )
    ledgerServices.ledger {
      transaction {
        attachment(tokenRandom200BusinessNode.contractId)
        output(
          tokenRandom200BusinessNode.contractId,
          "randomToken200BusinessNode",
          tokenRandom200BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(token150BusinessNode.contractId)
        output(
          token150BusinessNode.contractId,
          "xtsToken150BusinessNode",
          token150BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(token75BusinessNode.contractId)
        output(
          token75BusinessNode.contractId,
          "xtsToken75BusinessNode",
          token75BusinessNode
        )
        command(daoNodeParty.owningKey, Token.Command.Issue())
        this.verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "issuedMeteringInvoice1",
          issuedMeteringInvoiceState
        )
        command(
          listOf(meteringNotaryParty.owningKey, daoNodeParty.owningKey),
          MeteringInvoiceCommands.Issue()
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        attachment(token200PaidToDao.contractId)
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          MeteringInvoiceCommands.Pay()
        )
        command(
          listOf(corditeBusinessNodeParty.owningKey, meteringNotaryParty.owningKey),
          Token.Command.Move()
        )
        input("issuedMeteringInvoice1")
        input("xtsToken150BusinessNode")
        input("xtsToken75BusinessNode")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "paidMeteringInvoice",
          paidMeteringInvoiceState
        )
        output(token200PaidToDao.contractId, "200ToDao", token200PaidToDao)
        output(token25ReturnedToBusinessNode.contractId, token25ReturnedToBusinessNode)
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.SplitInvoice())
        input("paidMeteringInvoice")
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1",
          splitMeteringInvoiceState1.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1.copy(finalAccountId = daoAccountName)
          )
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice2",
          splitMeteringInvoiceState2
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice3",
          splitMeteringInvoiceState3
        )
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "dispersedMeteringInvoice",
          dispersedMeteringInvoiceState.copy(
            meteringInvoiceProperties = dispersedMeteringInvoice.copy(reissueCount = 0)
          )
        )
        verifies()
      }
      transaction {
        attachment(MeteringInvoiceContract.METERING_CONTRACT_ID)
        command(daoNodeParty.owningKey, MeteringInvoiceCommands.DisperseFunds())
        command(daoNodeParty.owningKey, Token.Command.Move())
        input("splitMeteringInvoice1")
        input("200ToDao")
        input("randomToken200BusinessNode")
        output(amountRandom200PaidToDao.contractId, amountRandom200PaidToDao)
        output(token100PaidToDao.contractId, token100PaidToDao)
        output(token100PaidToDao.contractId, token100PaidToDao.copy(accountId = "JUSTGIVEMETHEEFFINGMONEY"))
        output(
          MeteringInvoiceContract.METERING_CONTRACT_ID,
          "splitMeteringInvoice1_dispersed",
          splitMeteringInvoiceState1_dispersed.copy(
            meteringInvoiceSplitProperties = splitMeteringInvoice1_dispersed.copy(
              finalAccountId = daoAccountName
            )
          )
        )
        verifies()
      }
    }
  }

}