/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.token.Token
import net.corda.core.contracts.Contract
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.requireSingleCommand
import net.corda.core.contracts.requireThat
import net.corda.core.transactions.LedgerTransaction
import java.math.BigDecimal

/// MeteringInvoice contract validate the full lifecycle of
///   metering a transaction on the metering notary and raising an invoice
//    the creator of the transaction accepting the invoice or disputing it
//    the creator of the transaction paying the invoice to the dao or disputing the invoice
//    the metering notary re-issuing the invoice if it was disputed
//    the dao distributing the fees to the metering notary and dao funds according to the dao rules

//In order to make this efficient, we will allow many invoices to be sent and paid in the same transaction by the same party
//Payments will be made in the 'pay' command and must be in the same currency and tally to the amount in the invoice

class MeteringInvoiceContract : Contract {

  companion object {
    val METERING_CONTRACT_ID: ContractClassName = MeteringInvoiceContract::class.java.canonicalName
  }

  override fun verify(tx: LedgerTransaction) {
    val command = tx.commands.requireSingleCommand<MeteringInvoiceCommands>()
    val meteringInvoiceStateGroups = tx.groupStates { it: MeteringInvoiceState -> it.meteringInvoiceProperties.invoicedParty }
    val meteringInvoiceSplitStateGroups = tx.groupStates { it: MeteringInvoiceSplitState -> it.meteringInvoiceSplitProperties.splitInvoiceId }
    val tokenStates = tx.groupStates { it: Token.State -> it.accountAddress.accountId }

    for ((inputs, outputs, _) in meteringInvoiceSplitStateGroups) {
      when (command.value) {
        is MeteringInvoiceCommands.DisperseFunds -> {
          requireThat {
            "there should be one input meteringInvoiceSplitState" using (inputs.count()==1)
            "there should be one output meteringInvoiceSplitState" using (outputs.count()==1)

            val meteringInvoiceStateSplitInput = inputs[0]
            val meteringInvoiceStateSplitOutput = outputs[0]

            "the input state must be issued" using (inputs.all { it.meteringInvoiceSplitProperties.meteringSplitState == MeteringSplitState.SPLIT_ISSUED })
            "the output state must be issued" using (outputs.all { it.meteringInvoiceSplitProperties.meteringSplitState == MeteringSplitState.SPLIT_DISPERSED })
            "metering transaction Id must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.meteredTransactionId == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.meteredTransactionId)
            "daoParty must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.daoParty == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.daoParty)
            "invoiced party must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.invoicedParty == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.invoicedParty)
            "invoicing notary must not be change in the split" using  (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.invoicingNotary == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.invoicingNotary)
            "currency/token symbol must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.tokenDescriptor.symbol == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.tokenDescriptor.symbol)
            "currency/token exponent must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.tokenDescriptor.exponent == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.tokenDescriptor.exponent)
            "currency/token issuer must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.tokenDescriptor.issuerName == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.tokenDescriptor.issuerName)
            "amount must not be changed in the split" using (meteringInvoiceStateSplitInput.meteringInvoiceSplitProperties.amount == meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties.amount)
            "amount paid must equal the invoice split amount" using (inputs.all { verifySufficientAmountDispersed(meteringInvoiceStateSplitOutput.meteringInvoiceSplitProperties, tokenStates) })
          }
        }
      }
    }

    for ((inputs, outputs, _) in meteringInvoiceStateGroups) {
      when (command.value) {
        is MeteringInvoiceCommands.Issue -> {
          requireThat {
            "issue requires there to be no inputs" using (inputs.count() == 0)
            "output metering state must be issued" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.ISSUED && it.meteringInvoiceProperties.reissueCount == 0 })
            "output metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "notaries must be valid" using (outputs.all { verifyNotaries(it.meteringInvoiceProperties, tx) })  // https://gitlab.com/cordite/cordite/issues/266
          }
        }
        is MeteringInvoiceCommands.Dispute -> {
          requireThat {
            "input and outputs count of metering invoices must be the same" using (inputs.count() == outputs.count())
            "all input metering invoice transaction Ids must correspond with outputs" using (checkInputsAndOutputsAreForTheSameTransactions(inputs,outputs))
            "metering input state must be issued or reissued" using (inputs.all { listOf(MeteringState.ISSUED, MeteringState.REISSUED).contains(it.meteringInvoiceProperties.meteringState) })
            "metering state must be in dispute" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.IN_DISPUTE })
            "input metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "output metering invoice fields must be valid" using (inputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "notaries must be valid" using (outputs.all { verifyNotaries(it.meteringInvoiceProperties, tx) })
          }
        }
        is MeteringInvoiceCommands.Pay -> {
          requireThat {
            "input and outputs count of metering invoices must be the same" using (inputs.count() == outputs.count())
            "all input metering invoice transaction Ids must correspond with outputs" using (checkInputsAndOutputsAreForTheSameTransactions(inputs,outputs))
            "metering input state must be issued or reissued" using (inputs.all { listOf(MeteringState.ISSUED, MeteringState.REISSUED).contains(it.meteringInvoiceProperties.meteringState) })
            "metering state must be paid" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.PAID })
            "input metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "output metering invoice fields must be valid" using (inputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "account being paid must be account on invoice" using (inputs.all { verifyAccountBeingPaid(it.meteringInvoiceProperties, tokenStates) })
            "amount paid must cover invoice" using (inputs.all { verifySufficientAmountPaid(it.meteringInvoiceProperties, tokenStates) })
            "payment must go to DAO dispersal node" using (inputs.all { verifyPaymentIsToDao(it.meteringInvoiceProperties, tokenStates) })
            "notaries must be valid" using (outputs.all { verifyNotaries(it.meteringInvoiceProperties, tx) })
          }
        }
        //Single Invoice split
        is MeteringInvoiceCommands.SplitInvoice -> {

          val outputMeteringInvoiceSplitStates = tx.outputStates.filterIsInstance<MeteringInvoiceSplitState>()

          requireThat {
            //Verify the metering Invoices
            "there must be only one metering invoice input!!" using (inputs.count()==1)
            "there must be only one metering invoice output" using (outputs.count()==1)
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "input metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "output metering invoice fields must be valid" using (inputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "output metering state must be dispersed" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED })

            //Verify the splits
            "there must be three output split states" using (outputMeteringInvoiceSplitStates.count()==3)
            "the sum of output split amounts must be equal to the input amount" using (outputMeteringInvoiceSplitStates.sumBy{ it.meteringInvoiceSplitProperties.amount } == outputs.sumBy { it.meteringInvoiceProperties.amount})
            "input state must be paid" using (inputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.PAID })
            "output state must be split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.meteringSplitState == MeteringSplitState.SPLIT_ISSUED })
            "metering transaction Id must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.meteredTransactionId == inputs[0].meteringInvoiceProperties.meteredTransactionId})
            "daoParty must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.daoParty == inputs[0].meteringInvoiceProperties.daoParty})
            "invoiced party must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.invoicedParty == inputs[0].meteringInvoiceProperties.invoicedParty})
            "invoicing notary must not be change in the split" using  (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.invoicingNotary == inputs[0].meteringInvoiceProperties.invoicingNotary})
            "currency/token symbol must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.tokenDescriptor.symbol == inputs[0].meteringInvoiceProperties.tokenDescriptor.symbol})
            "currency/token exponent must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.tokenDescriptor.exponent == inputs[0].meteringInvoiceProperties.tokenDescriptor.exponent})
            "currency/token issuer must not be changed in the split" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.tokenDescriptor.issuerName == inputs[0].meteringInvoiceProperties.tokenDescriptor.issuerName})
          }
        }

        //Multiple Invoice split
        is MeteringInvoiceCommands.SplitInvoices -> {

          val outputMeteringInvoiceSplitStates = tx.outputStates.filterIsInstance<MeteringInvoiceSplitState>()
          val splitGroups = outputMeteringInvoiceSplitStates.groupBy { it.meteringInvoiceSplitProperties.meteredTransactionId }

          requireThat {
            //Verify the metering Invoices
            "there must be only one metering invoice input per metered transaction" using (inputs.map{it.meteringInvoiceProperties.meteredTransactionId}.count()==inputs.count())
            "there must be only one metering invoice output per metered transaction" using (outputs.map { it.meteringInvoiceProperties.meteredTransactionId }.count()==outputs.count())
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "input metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "output metering invoice fields must be valid" using (inputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "can't change any properties except metering state" using (inputs.map { verifyOnlyTheMeteringStateHasChanged(it, outputs) }.all { it == true })
            "output metering state must be dispersed" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.FUNDS_DISPERSED })

            //Verify the splits
            "there must be three output split states per metering invoice" using (splitGroups.all { it.value.count()== 3})
            "the splits must be correct" using ( inputs.all { verifySplits( it, splitGroups.get(it.meteringInvoiceProperties.meteredTransactionId)!! )})
            "input state must be paid" using (inputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.PAID })
            "output state must be split_issued" using (outputMeteringInvoiceSplitStates.all { it.meteringInvoiceSplitProperties.meteringSplitState == MeteringSplitState.SPLIT_ISSUED })
          }
        }
        is MeteringInvoiceCommands.ReIssue -> {
          requireThat {
            "input and outputs count of metering invoices must be the same" using (inputs.count() == outputs.count())
            "all input metering invoice transaction Ids must correspond with outputs" using (checkInputsAndOutputsAreForTheSameTransactions(inputs,outputs))
            "metering input state must be issued or reissued" using (inputs.all { listOf(MeteringState.IN_DISPUTE).contains(it.meteringInvoiceProperties.meteringState) })
            "metering state must be in dispute" using (outputs.all { it.meteringInvoiceProperties.meteringState == MeteringState.REISSUED })
            "#inputs == #outputs " using (outputs.count() == inputs.count())
            "input metering invoice fields must be valid" using (outputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "output metering invoice fields must be valid" using (inputs.all { verifyMeteringInvoiceFields(it.meteringInvoiceProperties) })
            "only specific fields can change during reissue" using (inputs.map { verifyChangedFieldsForReIssue(it, outputs) }.all { it == true })
            "notaries must be valid" using (outputs.all { verifyNotaries(it.meteringInvoiceProperties, tx) })

          }
        }
      }
    }
  }


  fun verifySplits( invoice: MeteringInvoiceState , splitGroup: List<MeteringInvoiceSplitState>) : Boolean {
    requireThat {
      "the sum of output split amounts must be equal to the input amount" using (splitGroup.sumBy { it.meteringInvoiceSplitProperties.amount } == invoice.meteringInvoiceProperties.amount )
      "metering transaction Id must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.meteredTransactionId == invoice.meteringInvoiceProperties.meteredTransactionId} )
      "daoParty must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.daoParty == invoice.meteringInvoiceProperties.daoParty} )
      "invoiced party must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.invoicedParty == invoice.meteringInvoiceProperties.invoicedParty} )
      "invoicing notary must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.invoicingNotary == invoice.meteringInvoiceProperties.invoicingNotary} )
      "token type/currency must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.tokenDescriptor.symbol == invoice.meteringInvoiceProperties.tokenDescriptor.symbol} )
      "token type/exponent must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.tokenDescriptor.exponent == invoice.meteringInvoiceProperties.tokenDescriptor.exponent} )
      "token type/issuer must not be changed in the split must not be changed in the split" using (splitGroup.all { it.meteringInvoiceSplitProperties.tokenDescriptor.issuerName == invoice.meteringInvoiceProperties.tokenDescriptor.issuerName} )
    }
    return true;
  }

  fun verifyMeteringInvoiceFields(meteringInvoiceProperties: MeteringInvoiceProperties): Boolean {
    requireThat {
      "transaction id must not be blank" using (meteringInvoiceProperties.meteredTransactionId.isNotBlank())
      "currency/token symbol is not blank" using (meteringInvoiceProperties.tokenDescriptor.symbol.isNotBlank())
      "currency/token issuer is not blank" using (meteringInvoiceProperties.tokenDescriptor.issuerName.toString().isNotBlank())
      "invoice id is not blank" using (meteringInvoiceProperties.invoiceId.isNotBlank())
      "pay account id must not be blank " using (meteringInvoiceProperties.payAccountId.isNotBlank())
    }
    return true
  }

  fun verifyOnlyTheMeteringStateHasChanged(inputMeteringInvoiceState: MeteringInvoiceState, outputMeteringInvoiceStates: List<MeteringInvoiceState>): Boolean {
    val inputMeteringInvoiceProperties = inputMeteringInvoiceState.meteringInvoiceProperties
    val outputMeteringInvoiceProperties = outputMeteringInvoiceStates.single { it.meteringInvoiceProperties.meteredTransactionId == inputMeteringInvoiceProperties.meteredTransactionId }.meteringInvoiceProperties

    requireThat {
      "transaction Id must not be changed" using (inputMeteringInvoiceProperties.meteredTransactionId == outputMeteringInvoiceProperties.meteredTransactionId)
      "currency/token symbol must not be changed" using (inputMeteringInvoiceProperties.tokenDescriptor.symbol == outputMeteringInvoiceProperties.tokenDescriptor.symbol)
      "currency/token exponent must not be changed" using (inputMeteringInvoiceProperties.tokenDescriptor.exponent == outputMeteringInvoiceProperties.tokenDescriptor.exponent)
      "currency/token issuer must not be changed" using (inputMeteringInvoiceProperties.tokenDescriptor.issuerName == outputMeteringInvoiceProperties.tokenDescriptor.issuerName)

      "amount must not be changed" using (inputMeteringInvoiceProperties.amount == outputMeteringInvoiceProperties.amount)
      "pay account id must not be changed " using (inputMeteringInvoiceProperties.payAccountId == outputMeteringInvoiceProperties.payAccountId)
      "invoice id must not be changed" using (inputMeteringInvoiceProperties.invoiceId == outputMeteringInvoiceProperties.invoiceId)
      "daoParty must not be changed" using (inputMeteringInvoiceProperties.daoParty == outputMeteringInvoiceProperties.daoParty)
      "invoiced party must not be changed" using (inputMeteringInvoiceProperties.invoicedParty == outputMeteringInvoiceProperties.invoicedParty)
      "invoicing notary must not be change" using (inputMeteringInvoiceProperties.invoicingNotary == outputMeteringInvoiceProperties.invoicingNotary)
      "reissue count must not be changed unless we are reissuing" using (inputMeteringInvoiceProperties.reissueCount == outputMeteringInvoiceProperties.reissueCount)
    }
    return true
  }

  fun verifyChangedFieldsForReIssue(inputMeteringInvoiceState: MeteringInvoiceState, outputMeteringInvoiceStates: List<MeteringInvoiceState>): Boolean {

    val inputMeteringInvoiceProperties = inputMeteringInvoiceState.meteringInvoiceProperties
    val outputMeteringInvoiceProperties = outputMeteringInvoiceStates.single { it.meteringInvoiceProperties.meteredTransactionId == inputMeteringInvoiceProperties.meteredTransactionId }.meteringInvoiceProperties

    requireThat {

      "transaction Id must not be changed" using (inputMeteringInvoiceProperties.meteredTransactionId == outputMeteringInvoiceProperties.meteredTransactionId)
      "invoice id must not be changed" using (inputMeteringInvoiceProperties.invoiceId == outputMeteringInvoiceProperties.invoiceId)
      "daoParty must not be changed" using (inputMeteringInvoiceProperties.daoParty == outputMeteringInvoiceProperties.daoParty)
      "invoiced party must not be changed" using (inputMeteringInvoiceProperties.invoicedParty == outputMeteringInvoiceProperties.invoicedParty)
      "invoicing notary must not be change" using (inputMeteringInvoiceProperties.invoicingNotary == outputMeteringInvoiceProperties.invoicingNotary)

      //Note: Even though the following conditions will all pass whatever, I wanted to explicitly put these in the contract to make it clear - code is law !
      "currency/token symbol can be changed" using (inputMeteringInvoiceProperties.tokenDescriptor.symbol == outputMeteringInvoiceProperties.tokenDescriptor.symbol ||
                                                    inputMeteringInvoiceProperties.tokenDescriptor.symbol != outputMeteringInvoiceProperties.tokenDescriptor.symbol)

      "amount can be changed" using (inputMeteringInvoiceProperties.amount == outputMeteringInvoiceProperties.amount || inputMeteringInvoiceProperties.amount != outputMeteringInvoiceProperties.amount)
      "pay account id can be changed " using (inputMeteringInvoiceProperties.payAccountId == outputMeteringInvoiceProperties.payAccountId || inputMeteringInvoiceProperties.payAccountId != outputMeteringInvoiceProperties.payAccountId)
      //This constraint prevents reissuing bogus invoices, and was introduced to stop primary key violations in the vault
      "reissue count must be incremented by 1 only" using (inputMeteringInvoiceProperties.reissueCount + 1 == outputMeteringInvoiceProperties.reissueCount)
    }
    return true
  }

  private fun checkInputsAndOutputsAreForTheSameTransactions(inputMeteringInvoiceStates: List<MeteringInvoiceState>, outputMeteringInvoiceStates: List<MeteringInvoiceState>): Boolean {
    requireThat {
      "some of the metering invoice inputs are not in the outputs - indicates attempt to burn invoice " using (inputMeteringInvoiceStates.dropLastWhile { outputMeteringInvoiceStates.map { it.meteringInvoiceProperties.meteredTransactionId }.contains(it.meteringInvoiceProperties.meteredTransactionId) }.count() == 0)
      "some of the metering invoice outputs are not in the inputs - indicates attempt to create duplicate invoice " using (outputMeteringInvoiceStates.dropLastWhile { inputMeteringInvoiceStates.map { it.meteringInvoiceProperties.meteredTransactionId }.contains(it.meteringInvoiceProperties.meteredTransactionId) }.count() == 0)
      "inputs are not unique" using (inputMeteringInvoiceStates.count() == outputMeteringInvoiceStates.distinctBy { it.meteringInvoiceProperties.meteredTransactionId }.count())
      "outputs are not unique" using (inputMeteringInvoiceStates.count() == outputMeteringInvoiceStates.distinctBy { it.meteringInvoiceProperties.meteredTransactionId }.count())
    }
    return true
  }

  private fun verifyNotaries(meteringInvoiceProperties: MeteringInvoiceProperties, tx: LedgerTransaction): Boolean {

    // check that the notary issuing the metering invoice is not the same as the notary notarising this transaction
    requireThat {
      "issuing notary is not the same as the notarising notary" using( meteringInvoiceProperties.invoicingNotary != tx.notary)
    }
    return true
  }

  private fun verifyAccountBeingPaid(meteringInvoiceProperties: MeteringInvoiceProperties, tokenStates: List<LedgerTransaction.InOutGroup<Token.State, String>>): Boolean {
    // the transaction outputs must contain a token bearing the invoiced account payable id
    // (the outputs may also contain change returned to sender)
    tokenStates.forEach {
      if(it.outputs.isNotEmpty()) {
        it.outputs.forEach {
          val accountId = extractAccountId(it.accountId)
          if (accountId == meteringInvoiceProperties.payAccountId)  {
            return true
          }
        }
      }
    }
    return false
  }

  private fun verifySufficientAmountPaid(meteringInvoiceProperties: MeteringInvoiceProperties, tokenStates: List<LedgerTransaction.InOutGroup<Token.State, String>>): Boolean {
    // the transaction outputs must contain sufficient tokens being paid to the invoice account with the required issuer and currency
    var amountPaid: BigDecimal = BigDecimal.ZERO;

    tokenStates.forEach {

      if(it.outputs.isNotEmpty()) {

        it.outputs.forEach {
          val accountId = extractAccountId(it.accountId)
          if ((accountId == meteringInvoiceProperties.payAccountId) &&
              (it.amount.token.product.symbol == meteringInvoiceProperties.tokenDescriptor.symbol) &&
              (it.amount.token.product.issuerName == meteringInvoiceProperties.tokenDescriptor.issuerName)) {
            amountPaid += it.amount.toDecimal()
          }
        }
      }
    }
    return amountPaid.intValueExact() >= meteringInvoiceProperties.amount
  }

  private fun verifySufficientAmountDispersed(meteringInvoiceProperties: MeteringInvoiceSplitProperties, tokenStates: List<LedgerTransaction.InOutGroup<Token.State, String>>): Boolean {
    var amountPaid: BigDecimal = BigDecimal.ZERO
    tokenStates.forEach {
      if(it.outputs.isNotEmpty()) {
        it.outputs.forEach {
          val accountId = extractAccountId(it.accountId)
          if ((accountId == meteringInvoiceProperties.finalAccountId) &&
            (it.amount.token.product.symbol == meteringInvoiceProperties.tokenDescriptor.symbol) &&
            (it.amount.token.product.issuerName == meteringInvoiceProperties.tokenDescriptor.issuerName)) {
            amountPaid += it.amount.toDecimal()
          }
        }
      }
    }
    return amountPaid.intValueExact() == meteringInvoiceProperties.amount
  }

  private fun verifyPaymentIsToDao(meteringInvoiceProperties: MeteringInvoiceProperties, tokenStates: List<LedgerTransaction.InOutGroup<Token.State, String>>): Boolean {

    // all output token states in this transaction must be paid to the account address corresponding to the dao node named on the metering invoice
    // OR to the invoice payer (representing change returned)

    // TODO: this is not a sufficient check to ensure that the payment is going to a genuine DAO node,
    // and not just the dao party that's mentioned on the invoice.  Add a further check into the invoice pay
    // receiver flow logic to test agains the DaoState available there

    tokenStates.forEach {
      if (it.outputs.isNotEmpty()) {
        it.outputs.forEach {
          if ((it.accountAddress.party != meteringInvoiceProperties.daoParty.name) &&
              (it.accountAddress.party != meteringInvoiceProperties.invoicedParty.name)) {
            return false;
          }
        }
      }
    }
    return true
  }

  private fun extractAccountId( taggedAccountId: String ) : String {
    // extract the bare accountId from an account id found in a TokenState. The metering invoice only records this

    var qualifiedAccountId: String = taggedAccountId

    // qualified account id may include a DGL tag, if it doesn't, don't worry :)
    try {
      qualifiedAccountId = Tag.parse(taggedAccountId).value
    } catch (e: Exception) {
      // ignore
    }

    // qualified account id may include qualified party name separated by '@'
    val parts = qualifiedAccountId.split('@')
    if (parts.size > 2 ) {
      throw RuntimeException("accountId unexpected form: $taggedAccountId")
    }
    return parts[0];
  }
}

