/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.schema

import io.cordite.metering.contract.MeteringState
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import java.time.Instant
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Index
import javax.persistence.Table

object MeteringInvoiceSchema

object MeteringInvoiceSchemaV1 : MappedSchema(schemaFamily = MeteringInvoiceSchema.javaClass, version = 1, mappedTypes = listOf(PersistentMeteringInvoice::class.java)) {

  //TODO - added invoiced party as extra part of key, due to this issue: https://gitlab.com/cordite/cordite/issues/250
  @Entity
  @Table(name = "Cordite_Metering_Invoice",
    indexes = arrayOf(Index(name = "index_metered_transaction_id", columnList = "metered_transaction_id,metering_state,reissue_count,invoiced_party", unique = true)))
  class PersistentMeteringInvoice(
    @Column(name = "metered_transaction_id", nullable = false)
    var meteredTransactionId: String,
    @Column(name = "token_type_symbol", nullable = false)
    var tokenTypeSymbol: String,
    @Column(name = "token_type_exponent", nullable = false)
    var tokenTypeExponent: Int,
    @Column(name = "token_type_issuer", nullable = false)
    var tokenTypeIssuer: String, // x.500 name as string
    @Column(name = "amount", nullable = false)
    var amount : Int,
    @Column(name = "invoice_id", nullable = false)
    var invoiceId: String,
    @Column(name = "metering_state", nullable = false)
    var meteringState: String,
    @Column(name = "invoiced_party", nullable = false)
    var invoicedParty: String,
    @Column(name = "invoicing_notary", nullable = false)
    var invoicingNotary: String,
    @Column(name = "dao_party", nullable = false)
    var daoParty: String,
    @Column(name = "pay_account_id", nullable = false)
    var payAccountId: String,
    @Column(name = "reissue_count", nullable = false)
    var reissueCount : Int,
    @Column(name = "owner", nullable = false)
    var owner : String,
    @Column(name = "created_date_time", nullable = false)
    var createdDateTime : Instant?
  ) :  PersistentState() {
    constructor() : this("", "",0,"",0,"", MeteringState.UN_METERED.toString(),"","","","",0,"", null)
  }
}


