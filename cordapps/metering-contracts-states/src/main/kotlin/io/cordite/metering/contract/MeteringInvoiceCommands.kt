/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import net.corda.core.contracts.CommandData
import net.corda.core.contracts.TypeOnlyCommandData

interface MeteringInvoiceCommands : CommandData {
  class Issue : TypeOnlyCommandData(), MeteringInvoiceCommands
  class Pay : TypeOnlyCommandData(), MeteringInvoiceCommands
  class Dispute : TypeOnlyCommandData(), MeteringInvoiceCommands
  class ReIssue : TypeOnlyCommandData(), MeteringInvoiceCommands
  class SplitInvoice : TypeOnlyCommandData(), MeteringInvoiceCommands
  class SplitInvoices : TypeOnlyCommandData(), MeteringInvoiceCommands
  class DisperseFunds : TypeOnlyCommandData(), MeteringInvoiceCommands
}

