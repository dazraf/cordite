/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.daostate

import net.corda.core.serialization.CordaSerializable

/*
  This is used to determine who gets what when a metering fee is paid.
  Cordite is not opinionated and neither is the allocation table, so you can stick what ever numbers
  you like in the allocations table and it'll all work out - it's not strict percentages
 */
@CordaSerializable
data class MeteringFeeAllocation(val daoHoldingAccountId : String,
                                 val daoFoundationAllocation: Int,
                                 val meterNotaryAllocation : Int,
                                 val guardianNotaryAllocation : Int,
                                 val daoFoundationAccount : String)

class MeteringFeeAllocator{

  @CordaSerializable
  data class FeesToDisperse(val daoFoundationAllocation: Int,
                            val meterNotaryAllocation : Int,
                            val guardianNotaryAllocation : Int)

  companion object {
    fun AllocateFeesFromAmount(amount : Int, meteringFeeAllocation: MeteringFeeAllocation) : FeesToDisperse {

      val totalAllocationDivisor = meteringFeeAllocation.daoFoundationAllocation + meteringFeeAllocation.guardianNotaryAllocation + meteringFeeAllocation.meterNotaryAllocation

      val daoFoundationAllocation = calcIntegerAllocation(amount,meteringFeeAllocation.daoFoundationAllocation,totalAllocationDivisor)
      val meteringNotaryAllocation = calcIntegerAllocation(amount,meteringFeeAllocation.meterNotaryAllocation,totalAllocationDivisor)
      val guardianNotaryAllocation = calcIntegerAllocation(amount,meteringFeeAllocation.guardianNotaryAllocation,totalAllocationDivisor)

      val totalAllocated = daoFoundationAllocation + meteringNotaryAllocation + guardianNotaryAllocation
      val unallocated = amount - totalAllocated
      val adjustedMeteringAllocation = meteringNotaryAllocation + unallocated


      return FeesToDisperse(daoFoundationAllocation,adjustedMeteringAllocation,guardianNotaryAllocation)
    }

    //Kept this calculation separate as we may change it later
    private fun calcIntegerAllocation(amount : Int, percentAllocation : Int, totalAllocationDivisor : Int) : Int {
      if (amount==0 || percentAllocation==0 || totalAllocationDivisor ==0) return 0
      return (amount * percentAllocation).div(totalAllocationDivisor)
    }
  }
}





