/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.metering.contract

import io.cordite.dgl.corda.token.TokenType
import io.cordite.metering.schema.MeteringInvoiceSchemaV1
import net.corda.core.contracts.Contract
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.internal.x500Name
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import java.time.Instant
import java.util.*

/**
 * Please note: we are using the concept of ownable state here, without using ownable state as we don't want the move command...
 * we can't arbitrarily move the MeteringInvoice between parties - the flow is strict for which party can do what
 */
data class MeteringInvoiceState(val meteringInvoiceProperties: MeteringInvoiceProperties, val owner: AbstractParty) : QueryableState, LinearState {

  val contract: Contract = MeteringInvoiceContract()
  override val participants: List<AbstractParty> = listOf(meteringInvoiceProperties.invoicedParty, meteringInvoiceProperties.daoParty, meteringInvoiceProperties.invoicingNotary)
  override val linearId: UniqueIdentifier =UniqueIdentifier(meteringInvoiceProperties.meteredTransactionId)

  override fun generateMappedObject(schema: MappedSchema): PersistentState {
    return when (schema) {
      is MeteringInvoiceSchemaV1 -> MeteringInvoiceSchemaV1.PersistentMeteringInvoice(
        meteredTransactionId = meteringInvoiceProperties.meteredTransactionId,
        tokenTypeSymbol = meteringInvoiceProperties.tokenDescriptor.symbol,
        tokenTypeExponent = meteringInvoiceProperties.tokenDescriptor.exponent,
        tokenTypeIssuer = meteringInvoiceProperties.tokenDescriptor.issuerName.toString(),

        amount = meteringInvoiceProperties.amount,
        invoiceId = meteringInvoiceProperties.invoiceId,
        meteringState = meteringInvoiceProperties.meteringState.name,
        invoicedParty = meteringInvoiceProperties.invoicedParty.name.toString(),
        invoicingNotary = meteringInvoiceProperties.invoicingNotary.name.toString(),
        daoParty = meteringInvoiceProperties.daoParty.name.toString(),
        payAccountId = meteringInvoiceProperties.payAccountId,
        reissueCount = meteringInvoiceProperties.reissueCount,
        owner = owner.nameOrNull()?.x500Name?.toString() ?: "",
        createdDateTime = meteringInvoiceProperties.createdDateTime)
      else -> throw IllegalArgumentException("Unrecognised schema $schema")
    }
  }
  override fun supportedSchemas(): Iterable<MappedSchema> = listOf(MeteringInvoiceSchemaV1)
}

@CordaSerializable
  data class MeteringInvoiceProperties(
    val meteredTransactionId : String,
    val tokenDescriptor: TokenType.Descriptor,
    val amount : Int,
    val invoiceId: String = UUID.randomUUID().toString(),
    val meteringState: MeteringState,
    val invoicedParty : Party,
    val invoicingNotary : Party,
    val daoParty : Party,
    val payAccountId : String,
    val reissueCount : Int,
    val createdDateTime: Instant
)

@CordaSerializable
    enum class MeteringState {
    UN_METERED,
    ISSUED,
    REISSUED,
    FAILED_TO_ISSUE,
    PAID,
    IN_DISPUTE,
    SPLIT,
    FUNDS_DISPERSED
  }
