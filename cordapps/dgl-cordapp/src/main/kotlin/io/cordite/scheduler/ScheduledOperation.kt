/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.scheduler

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.crud.CrudCommands
import io.vertx.core.json.JsonObject
import net.corda.core.contracts.*
import net.corda.core.flows.*
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.transactions.LedgerTransaction
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import rx.subjects.UnicastSubject
import java.time.Instant
import java.util.concurrent.ConcurrentHashMap

class ScheduledOperation : Contract {
  override fun verify(tx: LedgerTransaction) {
  }

  class State(
      val clientId: String,
      val payload: JsonObject,
      val schedule: List<Instant>,
      override val participants: List<AbstractParty>
  ) : ContractState, SchedulableState {
    override fun nextScheduledActivity(thisStateRef: StateRef, flowLogicRefFactory: FlowLogicRefFactory): ScheduledActivity? {
      val now = Instant.now()
      val nextInstant = schedule.firstOrNull { it >= now } ?: return null
      return ScheduledActivity(flowLogicRefFactory.create(HandleScheduledEventFlow::class.java, this), nextInstant)
    }
  }
}

class CreateScheduledOperationFlow(private val scheduledOperation: ScheduledOperation.State, private val notary: Party) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val txb = TransactionBuilder(notary)
    val stx = serviceHub.signInitialTransaction(txb.apply {
      addCommand(CrudCommands.Create(), ourIdentity.owningKey)
      addOutputState(scheduledOperation, ScheduledOperation::class.java.name)
    })
    val secureHash = subFlow(FinalityFlow(stx)).id
    return waitForLedgerCommit(secureHash)
  }
}


@InitiatingFlow
@SchedulableFlow
class HandleScheduledEventFlow(private val scheduledOperation: ScheduledOperation.State) : FlowLogic<Unit>() {
  companion object {
    private val clientStreams = ConcurrentHashMap<String, UnicastSubject<ScheduledOperation.State>>()
    fun getActiveSubject(clientId: String) : UnicastSubject<ScheduledOperation.State>? = clientStreams[clientId]
    fun getObservable(clientId: String) = clientStreams.computeIfAbsent(clientId) { UnicastSubject.create() }
  }

  @Suspendable
  override fun call() {
    val subject = getActiveSubject(scheduledOperation.clientId) ?:
      throw RuntimeException("client id ${scheduledOperation.clientId} not connected yet to receive event")
    subject.onNext(scheduledOperation)
  }
}