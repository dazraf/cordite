/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.impl

import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.toVertxFuture
import io.cordite.commons.utils.transaction
import io.cordite.dgl.corda.CreateAccountRequest
import io.cordite.dgl.corda.LedgerApi
import io.cordite.dgl.corda.account.*
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.tag.WellKnownTagCategories
import io.cordite.dgl.corda.tag.WellKnownTagValues
import io.cordite.dgl.corda.token.*
import io.cordite.dgl.corda.token.flows.IssueTokensFlow
import io.cordite.dgl.corda.token.flows.TransferTokenFlow
import io.cordite.scheduler.CreateScheduledOperationFlow
import io.cordite.scheduler.HandleScheduledEventFlow
import io.cordite.scheduler.ScheduledOperation
import io.vertx.core.Future
import io.vertx.core.Future.succeededFuture
import io.vertx.core.json.JsonObject
import net.corda.core.contracts.Amount
import net.corda.core.contracts.StateRef
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.QueryCriteria.VaultCustomQueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.utilities.loggerFor
import rx.Observable
import java.math.BigDecimal
import java.security.InvalidParameterException
import java.time.LocalDateTime
import java.time.ZoneOffset

open class LedgerApiImpl(private val services: AppServiceHub, private val sync: () -> Unit = {}) : LedgerApi {
  companion object {
    val log = loggerFor<LedgerApiImpl>()
  }

  override fun wellKnownTagCategories(): List<String> {
    val fields = WellKnownTagCategories::class.java.fields
    return fields
      .filter { it.type == String::class.java }
      .map { it.get(WellKnownTagCategories).toString() }
  }

  override fun wellKnownTagValues(): List<String> {
    val fields = WellKnownTagValues::class.java.fields
    return fields
      .filter { it.type == String::class.java }
      .map { it.get(WellKnownTagCategories).toString() }
  }

  override fun createTokenType(symbol: String, exponent: Int, notary: CordaX500Name): Future<TokenType.State> {
    val notaryParty = findNotary(notary)
    return createTokenType(symbol, exponent, notaryParty)
  }

  private fun createTokenType(symbol: String, exponent: Int, notary: Party): Future<TokenType.State> {
    val flow = CreateTokenTypeFlow(symbol, exponent, notary)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as TokenType.State }
  }


  override fun listTokenTypes(page: Int, pageSize: Int): Future<List<TokenType.State>> {
    return succeededFuture(services.listAllTokenTypes(page, pageSize))
  }

  override fun createAccount(accountId: String, notary: CordaX500Name): Future<Account.State> {
    return createAccount(accountId, findNotary(notary))
  }

  override fun createAccounts(requests: List<CreateAccountRequest>, notary: CordaX500Name): Future<Set<Account.State>> {
    val notaryParty = findNotary(notary)
    return createAccounts(requests, notaryParty)
  }

  private fun createAccount(accountId: String, notaryParty: Party): Future<Account.State> {
    return createAccounts(listOf(CreateAccountRequest(accountId)), notaryParty).map { it.first() }
  }

  private fun createAccounts(requests: List<CreateAccountRequest>, notaryParty: Party): Future<Set<Account.State>> {
    val mappedRequests = requests.map { CreateAccountFlow.Request(it.accountId) }
    val createFlow = CreateAccountFlow(mappedRequests, notaryParty)
    val createAccountFuture = services.startFlow(createFlow).returnValue
    sync()
    return createAccountFuture.toVertxFuture().map { it.tx.outputStates.map { it as Account.State }.toSet() }
  }

  override fun setAccountTag(accountId: String, tag: Tag, notary: CordaX500Name): Future<Account.State> {
    val updateFlow = SetAccountTagFlow(accountId, tag, findNotary(notary))
    val future = services.startFlow(updateFlow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as Account.State }
  }

  override fun removeAccountTag(accountId: String, category: String, notary: CordaX500Name): Future<Account.State> {
    val updateFlow = RemoveAccountTagFlow(accountId, category, findNotary(notary))
    val future = services.startFlow(updateFlow).returnValue
    sync()
    return future.toVertxFuture().map { it.tx.outputStates.first() as Account.State }
  }

  override fun findAccountsByTag(tag: Tag): Future<Set<Account.State>> {
    val flow = FindAccountsFlow(tag)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun getAccount(accountId: String): Future<Account.State> {
    val flow = GetAccountFlow(accountId)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.state.data }
  }

  override fun listAccounts(): Future<List<Account.State>> {
    log.trace("listAccounts")
    val flow = ListAllAccountsFlow()
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun listAccountsPaged(paging: PageSpecification): Future<List<Account.State>> {
    log.trace("listAccountsPaged")
    val flow = ListAllAccountsFlow(paging.pageNumber, paging.pageSize)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture()
  }

  override fun bulkIssueTokens(accountIds: List<String>, amount: String, symbol: String, description: String, notary: CordaX500Name): Future<SecureHash> {
    val descriptor = loadLocalTokenType(symbol)
    val amountTokenType = Amount.fromDecimal(BigDecimal(amount), descriptor)
    val issuer = services.myInfo.legalIdentities.first()
    val tokens = generateIssuedTokens(accountIds, amountTokenType, description, issuer)
    val notaryParty = findNotary(notary)
    val flow = IssueTokensFlow(tokens = tokens, notary = notaryParty, description = description)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.id }
  }

  private fun generateIssuedTokens(
    accountIds: List<String>,
    amountTokenType: Amount<TokenType.Descriptor>,
    description: String,
    issuer: Party
  ) = accountIds.asSequence()
    .map(this::parseAccountId)
    .map { Token.generateIssuance(amountTokenType.issuedBy(issuer.ref(1)), it, issuer, description) }
    .toList()

  private fun parseAccountId(accountId: String): String {
    val parsed = try {
      AccountAddress.parse(accountId)
    } catch (e: RuntimeException) {
      null
    }
    return when (parsed) {
      null -> accountId
      else -> {
        val identities = services.myInfo.legalIdentities
        require(identities.asSequence().map(Party::name).contains(parsed.party)) {
          "account is not owned by ${identities.first()}"
        }
        parsed.accountId
      }
    }
  }

  override fun issueToken(
    accountId: String,
    amount: String,
    symbol: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> = bulkIssueTokens(listOf(accountId), amount, symbol, description, notary)

  override fun balanceForAccount(accountId: String): Future<Set<Amount<TokenType.Descriptor>>> {
    return succeededFuture(Account.getBalances(services, accountId))
  }

  override fun balanceForAccountTag(tag: Tag): Future<Set<Amount<TokenType.Descriptor>>> {
    return succeededFuture(Account.getBalancesForTag(services, tag))
  }

  @Deprecated(message = "this is part of the old API", replaceWith = ReplaceWith("transferAccountToAccount"))
  override fun transferToken(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    return transferAccountToAccount(amount, tokenTypeUri, fromAccount, toAccount, description, notary)
  }

  override fun transferAccountToAccount(
    amount: String,
    tokenTypeUri: String,
    fromAccount: String,
    toAccount: String,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    val from = mapOf(fromAccount to amount)
    val to = mapOf(toAccount to amount)
    return transferAccountsToAccounts(tokenTypeUri, from, to, description, notary)
  }

  override fun transferAccountsToAccounts(
    tokenTypeUri: String,
    from: Map<String, String>,
    to: Map<String, String>,
    description: String,
    notary: CordaX500Name
  ): Future<SecureHash> {
    val tokenType = readTokenType(tokenTypeUri)
    val parsedFrom = from.map { Pair(parseFromAccount(it.key), Amount.fromDecimal(BigDecimal(it.value), tokenType)) }
    val parsedTo = to.map { Pair(parseToAccount(it.key), Amount.fromDecimal(BigDecimal(it.value), tokenType)) }
    val notaryParty = findNotary(notary)
    val flow = TransferTokenFlow(parsedFrom, parsedTo, description, notaryParty)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.id }
  }

  override fun scheduleEvent(clientId: String, payload: JsonObject, iso8601DateTime: LocalDateTime, notary: CordaX500Name): Future<SecureHash> {
    return scheduleEvent(clientId, payload, iso8601DateTime, findNotary(notary))
  }

  override fun listenForScheduledEvents(clientId: String): Observable<ScheduledOperation.State> {
    return HandleScheduledEventFlow.getObservable(clientId)
  }

  override fun transactionsForAccount(accountId: String, paging: PageSpecification): List<TokenTransactionSummary.State> {
    val accountAddress = parseFromAccount(accountId)
    val stmt = """
SELECT TRANSACTION_ID, OUTPUT_INDEX from CORDITE_TOKEN_TRANSACTION_AMOUNT
WHERE ACCOUNT_ID_URI in ('${accountAddress.uri}')
ORDER BY TRANSACTION_TIME DESC
      """
    return services.transaction {
      val stateRefs = services.jdbcSession().executeCaseInsensitiveQuery(stmt)
        .map {
          val sh = SecureHash.parse(it.getString("TRANSACTION_ID"))
          val i = it.getInt("OUTPUT_INDEX")
          StateRef(sh, i)
        }.toList().toBlocking().first()

      services.vaultService.queryBy(
        contractStateType = TokenTransactionSummary.State::class.java,
        criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs),
        paging = paging
      ).states.map { it.state.data }.sortedByDescending { it.transactionTime }
    }
  }

  override fun listenForTransactions(accountIds: List<String>): Observable<TokenTransactionSummary.State> {
    return listenForTransactionsWithPaging(accountIds, PageSpecification())
  }

  override fun listenForTransactionsWithPaging(accountIds: List<String>, paging: PageSpecification): Observable<TokenTransactionSummary.State> {
    val accountAddresses = accountIds.map { it.trim() }.filter { it.isNotEmpty() }.map { parseFromAccount(it) }
    return services.transaction {
      services.vaultService
        .trackBy(TokenTransactionSummary.State::class.java, QueryCriteria.VaultQueryCriteria(), paging)
        .updates
        // it appears we need to filter the produced states
        // because they do carry all outputs of the tx, rather than those specified in the query
        .map { update ->
          update.produced.single()
            .let {
            it.state.data.transactionId = it.ref.txhash
            it.state.data
          }
        }.let {
          if (accountAddresses.isNotEmpty()) {
            it.filter {
              it.amounts.any { accountAddresses.contains(it.accountAddress) }
            }
          } else {
            it
          }
        }
        .doOnError {
          log.error("listen for account failed", it)
        }
        .doOnNext {
          log.info("item received {}", it)
        }
    }
  }

  private fun scheduleEvent(clientId: String, payload: JsonObject, dateTime: LocalDateTime, notary: Party): Future<SecureHash> {
    val flow = CreateScheduledOperationFlow(
      ScheduledOperation.State(
        clientId = clientId,
        payload = payload,
        schedule = listOf(dateTime.toInstant(ZoneOffset.UTC)),
        participants = listOf(services.myInfo.legalIdentities[0])), notary)
    val future = services.startFlow(flow).returnValue
    sync()
    return future.toVertxFuture().map { it.id }
  }

  private fun readTokenType(str: String): TokenType.Descriptor {
    val parts = str.split(TokenType.Descriptor.SEPARATOR)
    return if (parts.size > 1) {
      // this is a uri to a token type
      TokenType.Descriptor.parse(str)
    } else {
      // this is a reference to a local token type
      loadLocalTokenType(str)
    }
  }

  private fun loadLocalTokenType(symbol: String): TokenType.Descriptor {
    val criteria = builder { VaultCustomQueryCriteria(TokenType.TokenTypeSchemaV1.PersistedTokenType::symbol.equal(symbol)) }
    return services.transaction {
      services.vaultService.queryBy(TokenType.State::class.java, criteria).states.firstOrNull()?.state?.data?.descriptor
        ?: throw InvalidParameterException("unknown token type $symbol")
    }
  }

  private fun parseToAccount(toAccount: String): AccountAddress {
    return try {
      AccountAddress.parse(toAccount)
    } catch (_: Throwable) {
      AccountAddress.create(toAccount, services.myInfo.legalIdentities.first().name)
    }
  }

  private fun parseFromAccount(fromAccount: String): AccountAddress {
    return try {
      val tag = Tag.parse(fromAccount)
      Account.findAccountsWithTag(services, tag).first().address
    } catch (_: Throwable) {
      AccountAddress.create(fromAccount, services.myInfo.legalIdentities.first().name)
    }
  }

  private fun findNotary(notary: CordaX500Name): Party {
    return services.networkMapCache.getNotary(notary) ?: throw RuntimeException("could not find notary $notary")
  }
}
