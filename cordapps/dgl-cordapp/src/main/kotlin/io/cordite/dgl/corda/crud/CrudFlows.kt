/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.crud

import co.paralleluniverse.fibers.Suspendable
import net.corda.core.contracts.ContractClassName
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.StateAndRef
import net.corda.core.flows.FinalityFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder

class CrudCreateFlow<T : LinearState>(
    private val clazz: Class<T>,
    private val states: List<T>,
    private val contractClassName: ContractClassName,
    private val notary: Party
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val ids = states.filter { it.linearId.externalId != null }.map { it.linearId.externalId!! }
    val qc = QueryCriteria.LinearStateQueryCriteria(externalId = ids)
    serviceHub.vaultService.queryBy(clazz, qc).apply {
      if (states.isNotEmpty()) {
        throw RuntimeException("cannot create the following $contractClassName because they exist: ${states.map { it.state.data.linearId.externalId }.joinToString(", ")}")
      }
    }
    val txb = TransactionBuilder(notary)
    val stx = serviceHub.signInitialTransaction(txb.apply {
      addCommand(CrudCommands.Create(), ourIdentity.owningKey)
      states.forEach { addOutputState(it, contractClassName) }
    })
    val secureHash = subFlow(FinalityFlow(stx)).id
    return waitForLedgerCommit(secureHash)
  }
}

class CrudUpdateFlow<T : LinearState>(
    private val inputStates: List<StateAndRef<T>>,
    private val states: List<T>,
    private val contractClassName: ContractClassName,
    private val notary: Party
) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val txb = TransactionBuilder(notary)
    val stx = serviceHub.signInitialTransaction(txb.apply {
      addCommand(CrudCommands.Update(), ourIdentity.owningKey)
      inputStates.forEach { addInputState(it) }
      states.forEach { addOutputState(it, contractClassName) }
    })
    val secureHash = subFlow(FinalityFlow(stx)).id
    return waitForLedgerCommit(secureHash)
  }
}
