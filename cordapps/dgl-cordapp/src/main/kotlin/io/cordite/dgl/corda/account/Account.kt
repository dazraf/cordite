/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.account

import io.cordite.commons.database.executeCaseInsensitiveQuery
import io.cordite.commons.utils.transaction
import io.cordite.dgl.corda.crud.CrudContract
import io.cordite.dgl.corda.tag.Tag
import io.cordite.dgl.corda.token.TokenType
import net.corda.core.contracts.*
import net.corda.core.crypto.SecureHash
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.node.AppServiceHub
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import rx.Observable
import java.util.*
import javax.persistence.*

class Account : CrudContract<Account.State>(Account.State::class) {

  companion object {
    val CONTRACT_ID: ContractClassName = Account::class.java.name

    fun exists(services: ServiceHub, account: AccountAddress): Boolean {
      val stmt = """SELECT COUNT(*) as AC_COUNT FROM CORDITE_ACCOUNT_ALIAS WHERE CATEGORY = 'DGL.ID' AND VALUE='$account'"""

      return services.transaction {
        val accountCount = services.jdbcSession().executeCaseInsensitiveQuery(stmt).map { it.getLong("AC_COUNT") }.toList().toBlocking().first().first()
        accountCount >= 1L
      }
    }

    fun getBalances(services: AppServiceHub, accountId: String): Set<Amount<TokenType.Descriptor>> {

      val stmt = """
SELECT SUM(TOKEN.AMOUNT) as TOTAL, TOKEN_TYPE_SYMBOL, EXPONENT, STATE_STATUS, ISSUER_NAME
FROM CORDITE_TOKEN as TOKEN
  JOIN VAULT_STATES AS STATES
    ON TOKEN.TRANSACTION_ID = STATES.TRANSACTION_ID
       AND TOKEN.OUTPUT_INDEX = STATES.OUTPUT_INDEX
       AND STATES.STATE_STATUS = 0
  LEFT JOIN VAULT_FUNGIBLE_STATES as FSTATES
    ON TOKEN.TRANSACTION_ID = FSTATES.TRANSACTION_ID
       AND TOKEN.OUTPUT_INDEX = FSTATES.OUTPUT_INDEX
WHERE TOKEN.ACCOUNT_ID = '$accountId'
GROUP BY TOKEN.TOKEN_TYPE_SYMBOL, TOKEN.EXPONENT, STATES.STATE_STATUS, FSTATES.ISSUER_NAME
 """

      return services.transaction {
        services.jdbcSession().executeCaseInsensitiveQuery(stmt)
          .map {
            val quantity = it.getBigDecimal("TOTAL")
            val symbol = it.getString("TOKEN_TYPE_SYMBOL")
            val exponent = it.getInt("EXPONENT")
            val issuerName = CordaX500Name.parse(it.getString("ISSUER_NAME"))
            Amount(quantity.longValueExact(), TokenType.Descriptor(symbol, exponent, issuerName))
          }
          .toList()
          .onErrorResumeNext {
            Observable.error(RuntimeException("failed to get balance from query", it))
          }
          .toBlocking()
          .first()
          .toSet()
      }
    }

    fun findAccountsWithTag(services: ServiceHub, tag: Tag, paging: PageSpecification = PageSpecification()): Set<Account.State> {
      val stmt = """
        SELECT TRANSACTION_ID, OUTPUT_INDEX
        FROM CORDITE_ACCOUNT_ALIAS
        WHERE CATEGORY = '${tag.category}'
        AND VALUE = '${tag.value}'
        """
      return services.transaction {
        val stateRefs = services.jdbcSession().executeCaseInsensitiveQuery(stmt)
          .map {
            val sh = SecureHash.parse(it.getString("TRANSACTION_ID"))
            val i = it.getInt("OUTPUT_INDEX")
            StateRef(sh, i)
          }.toList().toBlocking().first()

        services.vaultService.queryBy(
          contractStateType = Account.State::class.java,
          criteria = QueryCriteria.VaultQueryCriteria(stateRefs = stateRefs),
          paging = paging
        ).states.map { it.state.data }.toSet()
      }
    }

    fun getBalancesForTag(services: ServiceHub, accountTag: Tag): Set<Amount<TokenType.Descriptor>> {
      val stmt = """
      SELECT SUM(TOKEN.AMOUNT) as TOTAL, TOKEN_TYPE_SYMBOL, EXPONENT, STATE_STATUS, ISSUER_NAME
       FROM CORDITE_ACCOUNT_ALIAS as ALIAS
       JOIN CORDITE_ACCOUNT as ACCOUNT
        ON ALIAS.TRANSACTION_ID = ACCOUNT.TRANSACTION_ID
          AND ALIAS.OUTPUT_INDEX = ACCOUNT.OUTPUT_INDEX
       JOIN CORDITE_TOKEN as TOKEN
        ON TOKEN.ACCOUNT_ID = ACCOUNT.ACCOUNT
       JOIN VAULT_STATES AS STATES
        ON TOKEN.TRANSACTION_ID = STATES.TRANSACTION_ID
          AND TOKEN.OUTPUT_INDEX = STATES.OUTPUT_INDEX
          AND STATES.STATE_STATUS = 0
       LEFT JOIN VAULT_FUNGIBLE_STATES as FSTATES
        ON TOKEN.TRANSACTION_ID = FSTATES.TRANSACTION_ID
          AND TOKEN.OUTPUT_INDEX = FSTATES.OUTPUT_INDEX
        WHERE ALIAS.CATEGORY = '${accountTag.category}'
        AND ALIAS.VALUE = '${accountTag.value}'
        GROUP BY TOKEN.TOKEN_TYPE_SYMBOL, TOKEN.EXPONENT, STATES.STATE_STATUS, FSTATES.ISSUER_NAME
        """

      return services.transaction {
        services.jdbcSession().executeCaseInsensitiveQuery(stmt)
          .map {
            val quantity = it.getBigDecimal("TOTAL")
            val symbol = it.getString("TOKEN_TYPE_SYMBOL")
            val exponent = it.getInt("EXPONENT")
            val issuerName = CordaX500Name.parse(it.getString("ISSUER_NAME"))
            Amount(quantity.longValueExact(), TokenType.Descriptor(symbol, exponent, issuerName))
          }
          .toList()
          .toBlocking()
          .first()
          .toSet()
      }
    }
  }

  data class State(
    val address: AccountAddress,
    val tags: Set<Tag> = emptySet(),
    override val linearId: UniqueIdentifier = UniqueIdentifier(id = UUID.randomUUID(), externalId = address.accountId),
    override val participants: List<AbstractParty>) : LinearState, QueryableState {

    override fun generateMappedObject(schema: MappedSchema): PersistentState {
      val account = AccountSchemaV1.PersistentAccount(accountId = address.accountId, linearStateId = linearId.id)
      account.persistentAliases = tags.map { tag ->
        val alias = AccountSchemaV1.PersistentAlias(tag.category, tag.value)
        alias.persistentAccount = account
        alias
      }.toMutableSet()
      return account
    }

    override fun supportedSchemas(): Iterable<MappedSchema> = listOf(AccountSchemaV1)
  }


  object AccountSchema
  object AccountSchemaV1 : MappedSchema(
    AccountSchema::class.java, 1,
    setOf(PersistentAccount::class.java, PersistentAlias::class.java)) {
    @Entity
    @Table(name = "CORDITE_ACCOUNT")
    class PersistentAccount(
      @Column(name = "linearStateId")
      val linearStateId: UUID,
      @Column(name = "account")
      val accountId: String
    ) : PersistentState() {
      @OneToMany(fetch = FetchType.LAZY, cascade = arrayOf(CascadeType.PERSIST))
      @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
      @OrderColumn
      var persistentAliases: MutableSet<PersistentAlias> = mutableSetOf()
    }

    @Entity
    @Table(name = "CORDITE_ACCOUNT_ALIAS",
      indexes = arrayOf(
        Index(name = "tag_index", columnList = "category,value", unique = false)
      ))
    class PersistentAlias(
      @Column(name = "category", nullable = false)
      var category: String,
      @Column(name = "value", nullable = true)
      var value: String
    ) {
      @Id
      @GeneratedValue
      @Column(name = "child_id", unique = true, nullable = false)
      var childId: Int? = null

      @ManyToOne(fetch = FetchType.LAZY)
      @JoinColumns(JoinColumn(name = "transaction_id", referencedColumnName = "transaction_id"), JoinColumn(name = "output_index", referencedColumnName = "output_index"))
      var persistentAccount: PersistentAccount? = null
    }
  }
}

