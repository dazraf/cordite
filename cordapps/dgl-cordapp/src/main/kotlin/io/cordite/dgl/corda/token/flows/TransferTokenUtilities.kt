/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dgl.corda.account.Account
import io.cordite.dgl.corda.account.AccountAddress
import io.cordite.dgl.corda.token.*
import io.cordite.dgl.corda.token.flows.TokenTransactionSummaryFunctions.addTokenTransactionSummary
import net.corda.confidential.IdentitySyncFlow
import net.corda.core.contracts.*
import net.corda.core.flows.CollectSignatureFlow
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.node.ServiceHub
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.node.services.vault.builder
import net.corda.core.transactions.SignedTransaction
import net.corda.core.transactions.TransactionBuilder
import net.corda.core.utilities.loggerFor
import net.corda.core.utilities.unwrap
import java.security.InvalidParameterException
import java.security.PublicKey

class TransferTokenRecipientFunctions {
  companion object {
    fun checkTokenMoveTransaction(stx: SignedTransaction, serviceHub: ServiceHub) {
      // verify that we are receiving funds for a known account
      // Verify that we know who all the participants in the transaction are
      val states: Iterable<ContractState> = stx.tx.inputs.map { serviceHub.loadState(it).data } + stx.tx.outputs.map { it.data }
      states.forEach { state ->
        state.participants.forEach { anon ->
          require(serviceHub.identityService.wellKnownPartyFromAnonymous(anon) != null) {
            "Transaction state $state involves unknown participant $anon"
          }
        }
      }
      // TODO: check that the totals of input and output cash are equal https://gitlab.com/cordite/cordite/issues/289
    }
  }
}

class TransferTokenSenderFunctions {
  companion object {
    private val logger = loggerFor<TransferTokenRecipientFunctions>()

    @Suspendable
    fun prepareTokenMoveWithSummary(
      txb: TransactionBuilder,
      fromAccount: AccountAddress,
      toAccount: AccountAddress,
      amount: Amount<TokenType.Descriptor>,
      serviceHub: ServiceHub,
      ourIdentity: Party,
      description: String
    ): List<PublicKey> {
      val recipient = findRecipient(serviceHub, toAccount)
      val inputs = collectCoinsAndSoftLock(txb, fromAccount, amount, serviceHub, txb.notary as AbstractParty)
      val outputs = computeOutputs(fromAccount, toAccount, inputs, recipient, amount)
      return prepareTokenMove(txb, inputs, outputs).apply {
        txb.addTokenTransactionSummary(Token.Command.Move(), ourIdentity, description, listOf(recipient), listOf(
          TokenTransactionSummary.NettedAccountAmount(fromAccount, -amount.quantity, amount.token),
          TokenTransactionSummary.NettedAccountAmount(toAccount, amount.quantity, amount.token)
        ))
      }
    }

    @Suspendable
    fun prepareMultiTokenMoveWithSummary(
      txb: TransactionBuilder,
      from: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>,
      to: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>,
      serviceHub: ServiceHub,
      ourIdentity: Party,
      description: String): List<PublicKey> {
      val recipients = to.asSequence().map { findRecipient(serviceHub, it.first) }.toList().distinct()
      require(recipients.size == 1) { "there can be one and only one recipient in a multi token transfer" }
      val recipient = recipients.single()
      verifyAccounts(from.map { it.first }, to.map { it.first }, recipient, serviceHub)
      // gets account balances for token used in amount
      val inputsForMultipleAccounts = from.map { collectCoinsAndSoftLock(txb, it.first, it.second, serviceHub, txb.notary as AbstractParty) }
      val inputs = inputsForMultipleAccounts.reduce { acc, set -> acc.union(set) }
      val outputs = computeOutputsForMultiMove(inputsForMultipleAccounts, from, to, recipient)
      return prepareTokenMove(txb, inputs, outputs).apply {
        val summaryList = generateSummaryList(from, to)
        txb.addTokenTransactionSummary(Token.Command.Move(), ourIdentity, description, listOf(recipient), summaryList)
      }
    }

    @Suspendable
    private fun generateSummaryList(from: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>,
                                    to: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>):
      List<TokenTransactionSummary.NettedAccountAmount> {

      val fromSummary = from.map {
        TokenTransactionSummary.NettedAccountAmount(it.first, -it.second.quantity, it.second.token)
      }
      val toSummary = to.map {
        TokenTransactionSummary.NettedAccountAmount(it.first, it.second.quantity, it.second.token)
      }
      return fromSummary + toSummary
    }


    @Suspendable
    fun prepareTokenMoveWithNoSummary(
      txb: TransactionBuilder,
      fromAccount: AccountAddress,
      toAccount: AccountAddress,
      amount: Amount<TokenType.Descriptor>,
      serviceHub: ServiceHub
    ): List<PublicKey> {
      val recipient = findRecipient(serviceHub, toAccount)
      val inputs = collectCoinsAndSoftLock(txb, fromAccount, amount, serviceHub, txb.notary as AbstractParty)
      val outputs = computeOutputs(fromAccount, toAccount, inputs, recipient, amount)
      return prepareTokenMove(txb, inputs, outputs)
    }

    @Suspendable
    private fun prepareTokenMove(txb: TransactionBuilder, inputs: Set<StateAndRef<Token.State>>, outputs: Set<TransactionState<Token.State>>): List<PublicKey> {
      val inputSigningKeys = inputs.map { it.state.data.owner.owningKey }.distinct()
      val outputSigningKeys = outputs.map { it.data.owner.owningKey }
      val signingKeys = (inputSigningKeys + outputSigningKeys).distinct()
      inputs.forEach { txb.addInputState(it) }
      outputs.forEach { txb.addOutputState(it) }
      txb.addCommand(Token.Command.Move(), signingKeys)
      return inputSigningKeys
    }

    @Suspendable
    fun verifyAccounts(fromAccount: AccountAddress, toAccount: AccountAddress, recipient: Party, serviceHub: ServiceHub) {
      verifyAccounts(listOf(fromAccount), listOf(toAccount), recipient, serviceHub)
    }


    @Suspendable
    private fun verifyAccounts(fromAccounts: List<AccountAddress>, toAccounts: List<AccountAddress>, recipient: Party, serviceHub: ServiceHub) {
      toAccounts.intersect(fromAccounts).apply {
        if (isNotEmpty()) {
          throw RuntimeException("cannot transfer between the same accounts ${joinToString(",")}")
        }
      }
      verifyAccountsExist(serviceHub, fromAccounts)
      if (serviceHub.myInfo.isLegalIdentity(recipient)) {
        verifyAccountsExist(serviceHub, toAccounts)
      }
    }

    @Suspendable
    private fun computeOutputsForMultiMove(
      inputsForMultipleAccounts: List<Set<StateAndRef<Token.State>>>,
      from: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>,
      to: List<Pair<AccountAddress, Amount<TokenType.Descriptor>>>,
      recipient: Party): Set<TransactionState<Token.State>> {
      val outputs = mutableSetOf<TransactionState<Token.State>>()
      val templateState = inputsForMultipleAccounts.first().first().state
      val issuer = templateState.data.amount.token.issuer
      if (inputsForMultipleAccounts.size != from.size) {
        val msg = "input data sizes mismatch: balance list size (${inputsForMultipleAccounts.size}) does not match account list size (${from.size})"
        logger.error(msg)
        throw IllegalStateException(msg)
      }
      from.forEachIndexed { index, _ ->
        val addressAndAmount = from[index]
        outputs += createRemainder(addressAndAmount.first, inputsForMultipleAccounts[index], addressAndAmount.second)
      }

      to.forEach {
        outputs += deriveState(
          templateState,
          it.first.accountId,
          it.second.issuedBy(issuer),
          recipient)
      }

      return outputs
    }

    @Suspendable
    private fun deriveState(txs: TransactionState<Token.State>,
                            accountId: String,
                            amount: Amount<Issued<TokenType.Descriptor>>,
                            owner: AbstractParty) =
      txs.copy(data = txs.data.copy(amount = amount, owner = owner, accountId = accountId))

    @Suspendable
    private fun createRemainder(
      fromAccount: AccountAddress,
      inputs: Set<StateAndRef<Token.State>>,
      amount: Amount<TokenType.Descriptor>
    ): Set<TransactionState<Token.State>> {
      val outputs = mutableSetOf<TransactionState<Token.State>>()
      val total = inputs.map { it.state.data.amount.quantity }.sum()
      val remainder = total - amount.quantity
      val templateState = inputs.first().state
      val issuer = inputs.first().state.data.amount.token.issuer
      when {
        remainder > 0 -> // calculate the 'change' returned
          outputs += deriveState(templateState,
            fromAccount.accountId,
            Amount(remainder, Issued(issuer, amount.token)),
            inputs.first().state.data.owner)
        remainder < 0 -> {
          val msg = "required $amount but only collected ${amount.copy(quantity = total)}"
          logger.error(msg)
          throw IllegalStateException(msg)
        }
        else -> {
          logger.trace("no change required for ${amount.quantity} from $total")
        }
      }
      return outputs
    }

    @Suspendable
    private fun computeOutputs(
      fromAccount: AccountAddress,
      toAccount: AccountAddress,
      inputs: Set<StateAndRef<Token.State>>,
      recipient: AbstractParty,
      amount: Amount<TokenType.Descriptor>
    ): Set<TransactionState<Token.State>> {
      val issuer = inputs.first().state.data.amount.token.issuer
      val outputs = mutableSetOf<TransactionState<Token.State>>()
      outputs += createRemainder(fromAccount, inputs, amount)
      val templateState = inputs.first().state

      outputs += deriveState(
        templateState,
        toAccount.accountId,
        amount.issuedBy(issuer),
        recipient)
      return outputs
    }

    @Suspendable
    private fun collectCoinsAndSoftLock(
      txb: TransactionBuilder,
      fromAccount: AccountAddress,
      amount: Amount<TokenType.Descriptor>,
      serviceHub: ServiceHub,
      notary: AbstractParty
    ): Set<StateAndRef<Token.State>> {
      val qc = builder {
        QueryCriteria.VaultCustomQueryCriteria(Token.TokenSchemaV1.PersistedToken::accountId.equal(fromAccount.accountId)) and
          QueryCriteria.VaultCustomQueryCriteria(Token.TokenSchemaV1.PersistedToken::tokenTypeSymbol.equal(amount.token.symbol)) and
          QueryCriteria.VaultCustomQueryCriteria(Token.TokenSchemaV1.PersistedToken::issuer.equal(amount.token.issuerName.toString())) and
          QueryCriteria.VaultQueryCriteria(notary = listOf(notary))
      }
      val inputs = serviceHub.vaultService
        .tryLockFungibleStatesForSpending(txb.lockId, qc, amount, Token.State::class.java)
        .toSet()

      if (inputs.isEmpty()) {
        throw RuntimeException("could not find enough tokens to meet $amount from notary ${notary.nameOrNull()?.toString()
          ?: "<unknown>"}")
      }
      return inputs
    }

    @Suspendable
    fun <T> FlowLogic<T>.collectTokenMoveSignatures(stx: SignedTransaction, serviceHub: ServiceHub, toAccount: AccountAddress): SignedTransaction {
      val recipient = findRecipient(serviceHub, toAccount)
      return if (!serviceHub.myInfo.isLegalIdentity(recipient)) {
        logger.debug("checking existence of remote account")
        val session = initiateFlow(recipient)
        val response = session.sendAndReceive<String>(toAccount).unwrap { it }
        logger.debug("account check response $response")
        subFlow(IdentitySyncFlow.Send(session, stx.tx))
        val sellerSignature = subFlow(CollectSignatureFlow(stx, session, session.counterparty.owningKey))
        stx + sellerSignature
      } else {
        if (!Account.exists(serviceHub, toAccount)) {
          throw InvalidParameterException("${serviceHub.myInfo.legalIdentities.first().name} - account does not exist: $toAccount")
        }
        stx
      }
    }

    @Suspendable
    fun findRecipient(serviceHub: ServiceHub, recipientAccount: AccountAddress): Party {
      return serviceHub.networkMapCache
        .getNodeByLegalName(recipientAccount.party)
        ?.legalIdentities?.first()
        ?: throw InvalidParameterException("cannot find recipient")
    }
  }
}

object TokenTransactionSummaryFunctions {
  @Suspendable
  fun TransactionBuilder.addTokenTransactionSummary(
    command: Token.Command,
    ourIdentity: Party,
    description: String,
    participants: List<AbstractParty>,
    amounts: List<TokenTransactionSummary.NettedAccountAmount>) {
    val summary = TokenTransactionSummary.State(
      participants = (participants.toSet() + ourIdentity).toList(),
      command = command,
      description = description,
      amounts = amounts)
    addOutputState(summary, TokenTransactionSummary.CONTRACT_ID)
  }
}

/*
class TransferTokenCollectSignaturesHelper {
  companion object {
    @Suspendable
    fun <T> FlowLogic<T>.collectTokenMoveSignatures(stx: SignedTransaction, serviceHub: ServiceHub, toAccount: AccountAddress): SignedTransaction {
      val recipient = findRecipient(serviceHub, toAccount)
      return if (!serviceHub.myInfo.isLegalIdentity(recipient)) {
        val session = initiateFlow(recipient)
        subFlow(IdentitySyncFlow.Send(session, stx.tx))
        val sellerSignature = subFlow(CollectSignatureFlow(stx, session, session.counterparty.owningKey))
        stx + sellerSignature
      } else {
        stx
      }
    }

    @Suspendable
    fun findRecipient(serviceHub: ServiceHub, recipientAccount: AccountAddress): Party {
      return serviceHub.networkMapCache
        .getNodeByLegalName(recipientAccount.party)
        ?.legalIdentities?.first()
        ?: throw InvalidParameterException("cannot find recipient")
    }
  }
}
*/