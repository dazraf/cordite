<!--

      Copyright 2018, Cordite Foundation.

       Licensed under the Apache License, Version 2.0 (the "License");
       you may not use this file except in compliance with the License.
       You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing, software
       distributed under the License is distributed on an "AS IS" BASIS,
       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       See the License for the specific language governing permissions and
       limitations under the License.

-->
# ![Cordite](/uploads/37fcd730405f565bca6aa09a453ec865/logo-watermark-50.png) Cordite


## What is Cordite?  
Cordite provides decentralised economic and governance services including:

  + decentralised stores and transfers of value allowing new financial instruments to be created inside the existing regulatory framework. eg. tokens, crypto-coins, digital cash, virtual currency, distributed fees, micro-billing  
  + decentralised forms of governance allowing new digital autonomous organisations to be created using existing legal entities eg. digital mutual societies or digital joint stock companies  
  + decentralised consensus in order to remove the need for a central operator, owner or authority. Allowing Cordite to be more resilient, cheaper, agile and private than incumbent market infrastructure  

Cordite is open source, regulatory friendly, enterprise ready and finance grade.  

Cordite is built on [Corda](http://corda.net), a finance grade distributed ledger technology, meeting the highest standards of the banking industry, yet it is applicable to any commercial scenario. The outcome of over two years of intense research and development by over 80 of the world’s largest financial institutions. 

## How do I get started?
   1. [Connect to a Cordite node](#how-do-i-connect-to-a-cordite-node)
   2. [Read the docs](https://cordite.readthedocs.io)
   3. [Deploy your own node](#how-do-i-deploy-my-own-cordite-node)
   4. [Use Cordite in your project](#how-do-i-use-cordite-in-my-own-project)
   5. [Build Cordite from source](#how-do-i-build-from-source)

## How do I get in touch?
  + News is announced on [@We_are_Cordite](https://twitter.com/we_are_cordite)
  + More information can be found on [Cordite website](https://cordite.foundation)
  + We use #cordite channel on [Corda slack](https://slack.corda.net/) 
  + We informally meet at the [Corda London meetup](https://www.meetup.com/pro/corda/)

## What if something does not work?
We encourage you to raise any issues/bugs you find in Cordite. Please follow the below steps before raising issues:
   1. Check on the [Issues backlog](https://gitlab.com/cordite/cordite/issues) to make sure an issue on the topic has not already been raised
   2. Post your question on the #cordite channel on [Corda slack](https://slack.corda.net/)
   3. If none of the above help solve the issue, [raise an issue](https://gitlab.com/cordite/cordite/issues/new?issue) following the contributions guide

## How do I contribute?
We welcome contributions both technical and non-technical with open arms! There's a lot of work to do here. The [Contributing Guide](https://gitlab.com/cordite/cordite/blob/master/contributing.md) provides more information on how to contribute.

## Who is behind Cordite?
Cordite is being developed by a group of financial services companies, software vendors and open source contributors. The project is hosted on here on GitLab. 

## What open source license has this been released under?
All software in this repository is licensed under the Apache License, Version 2.0 (the "License"); you may not use this software except in compliance with the License. You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.


--- 

<!-- FAQ START 01 -->
## How do I connect to a Cordite node?
We have created a few test nodes:
   + https://amer-test.cordite.foundation:8080
   + https://apac-test.cordite.foundation:8080
   + https://emea-test.cordite.foundation:8080

These are available as public nodes hosted in East US, SE Asia and W Europe locations respectively. You can use the REST endpoint `/api` or use the following clients:
   + [example node client](https://gitlab.com/cordite/cordite/tree/master/clients/braid-js-example)
   + [Interactive console](https://gitlab.com/cordite/cordite/tree/master/clients/cli)
<!-- FAQ END 01 -->

<!-- FAQ START 02 -->
## How do I deploy my own Cordite node?
We use [Docker](https://docs.docker.com/get-started/) to deploy Cordite nodes. For instructions on starting up a Cordite node using Docker, please visit our [Docker Hub Repo](https://hub.docker.com/r/cordite/cordite/) for more information on configuration including environment variables:
```
$ docker run -p 8080:8080 -p 10002:10002 cordite/cordite
```  
Once the node is running, you will be able to see the REST API for accessing Cordite at https://localhost:8080/api  

If you do not wish to use Docker then alternatively you can download the [Cordite node](https://gitlab.com/cordite/cordite/tags) and run without docker by running `./start.sh`. You will need Oracle JRE 8 JVM - minimum supported version 8u131.

### Environment Variables
Cordite uses environment variables to configure Cordite and create amoungst other things the Corda node.conf file.

Env Name| Description | Default 
------------- |:-------------:|-------------:|
CORDITE_LEGAL_NAME | The name of the node | `O=Cordite-XXX, OU=Cordite, L=London, C=GB`
CORDITE_P2P_ADDRESS | The address other nodes will use to speak to your node | `localhost:10002`
CORDITE_COMPATIBILITY_ZONE_URL | The address of the Network Map Service. | `https://network-map-test.cordite.foundation`
CORDITE_KEY_STORE_PASSWORD | Keystore password | `cordacadevpass`
CORDITE_TRUST_STORE_PASSWORD | Truststore password | `trustpass`
CORDITE_DB_USER | Username for db | `sa`
CORDITE_DB_PASS | Password for db | `dnpass`
CORDITE_DB_DIR | Path to db directory - only used for H2 | `/opt/cordite/db/`
CORDITE_DB_URL | database JDBC URI | `<default corda url>`
CORDITE_DB_DRIVER | driver class name for database access - this image comes preconfigured with postgres 42.2.5 - you can use `org.postgresql.ds.PGSimpleDataSource` to enable it | `org.h2.jdbcx.JdbcDataSource`
CORDITE_BRAID_PORT | Braid port | `8080`
CORDITE_DEV_MODE | Start up node in dev mode | `true`
CORDITE_DETECT_IP | Allow node to auto detect external visible IP | `false`
CORDITE_TLS_CERT_PATH | Path to TLS certificate | `null`
CORDITE_TLS_KEY_PATH | Path to TLS Key | `null`
CORDITE_NOTARY | Set to true to be a validating notary, false for non-validating or do not set to be a notary | `null`
CORDITE_METERING_CONFIG | JSON to set metering notary configuration | `null`
CORDITE_FEE_DISPERSAL_CONFIG | JSON to set metering fee dispersal config | `null`

### Volume Mounts
File/Folder | Description 
------------- |:-------------:|
/opt/cordite/node.conf |	Configuration file detailing specifics of your node - will be created using env variables if a node.conf is not mounted
/opt/cordite/db	| Location of the database for that node - for persistence, use volume mount
/opt/cordite/certificates	| Location of the nodes' corda certificates - will be created if no certificates are mounted to node and devmode=true
/opt/cordite/tls-certificates	| Location of TLS certificates - will use self-signed certificates if none are mounted to node  
<!-- FAQ END 02 -->

<!-- FAQ START 03 -->
## How do I build from source?
When building from source we recommend the following setup:
* Unix OS (ie OS X)
* Docker - minimum supported version 18.03.0
* NPM - minimum supported version 5.6.0
* Oracle JDK 8 JVM - minimum supported version 8u131

For those wishing to build Cordite from source run `./build.sh`. (NOTE: this script is not to designed to be run on Windows.)
Cordite node is laid out in `./node` and gradle builds in `./cordapps`. 
To start node after the build run `(cd node && start.sh)`. 
<!-- FAQ END 03 -->

<!-- FAQ START 04 -->
## How do I use Cordite in my own project
The core of Cordite is a collection of CorDapps which are java libraries. These are releasd to [Maven Central](http://central.maven.org/maven2/io/cordite/) and can be used in your project 
<!-- FAQ END 04 -->

<!-- FAQ START 05 -->
## Can I run multiple nodes on a single machine?
Yes you can. You need to be aware of the ports you are using for each node on the same host to ensure none clash. For all new ports assigned to each node, you will need to update the firewall accordingly.
<!-- FAQ END 05 -->

<!-- FAQ START 06 -->
## How do I use Cordite in my own project
The core of Cordite is a collection of CorDapps which are java libraries. These are releasd to [Maven Central](http://central.maven.org/maven2/io/cordite/) and can be used in your project 
<!-- FAQ END 06 -->
