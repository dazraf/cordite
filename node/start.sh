#!/bin/sh
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# Starts Cordite node. Creates node.conf if missing

set -e

echo "  _____            ___ __     
 / ___/__  _______/ (_) /____ 
/ /__/ _ \\/ __/ _  / / __/ -_)
\\___/\\___/_/  \\_,_/_/\\__/\\__/"

if [ -f ./build-info.txt ]; then
   cat build-info.txt
fi

echo
echo

printenv

# because bash and corda HOCON sometimes have a tiff about quotes!
defaulturl='"jdbc:h2:file:"${baseDirectory}"/db/persistence;DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;WRITE_DELAY=100;AUTO_SERVER_PORT="${h2port}'

# Variables used to create node.conf, defaulted if not set
CORDITE_LEGAL_NAME=${CORDITE_LEGAL_NAME:-O=Cordite-$(od -x /dev/urandom | head -1 | awk '{print $7$8$9}'), OU=Cordite, L=London, C=GB}
CORDITE_P2P_ADDRESS=${CORDITE_P2P_ADDRESS:-localhost:10002}
# CORDITE_DB_DIR see below for defaulting
CORDITE_COMPATIBILITY_ZONE_URL=${CORDITE_COMPATIBILITY_ZONE_URL:=https://network-map-test.cordite.foundation}
CORDITE_KEY_STORE_PASSWORD=${CORDITE_KEY_STORE_PASSWORD:=cordacadevpass}
CORDITE_TRUST_STORE_PASSWORD=${CORDITE_TRUST_STORE_PASSWORD:=trustpass}
CORDITE_DB_USER=${CORDITE_DB_USER:=sa}
CORDITE_DB_PASS=${CORDITE_DB_PASS:=dbpass}
CORDITE_DB_DRIVER=${CORDITE_DB_DRIVER:=org.h2.jdbcx.JdbcDataSource}
CORDITE_BRAID_PORT=${CORDITE_BRAID_PORT:=8080}
CORDITE_DEV_MODE=${CORDITE_DEV_MODE:=true}
CORDITE_DETECT_IP=${CORDITE_DETECT_IP:=false}
CORDITE_CACHE_NODEINFO=${CORDITE_CACHE_NODEINFO:=false}
if [[ -z "${CORDITE_DB_URL}" ]]; then
    echo "CORDITE_DB_URL is not set"
    if [[ -z "${CORDITE_DB_DIR}" ]]; then
        echo "CORDITE_DB_DIR is not set"
        CORDITE_DB_URL=${defaulturl}
    else
        echo "CORDITE_DB_DIR is set to ${CORDITE_DB_DR}"
        CORDITE_DB_URL="\"jdbc:h2:file:${CORDITE_DB_DIR};DB_CLOSE_ON_EXIT=FALSE;LOCK_TIMEOUT=10000;WRITE_DELAY=100;AUTO_SERVER_PORT=\""'${h2port}'
    fi
else
    echo "CORDITE_DB_URL is already set to ${CORDITE_DB_URL}"
    CORDITE_DB_URL="\"${CORDITE_DB_URL}\""
fi

echo "CORDITE_DB_URL is: ${CORDITE_DB_URL}"
# Create node.conf if it does not exist and default if variables not set
if [ ! -f ./node.conf ]; then
  echo "./node.conf not found, creating"
  basedir=\"\${baseDirectory}\"
  braidhost=${CORDITE_LEGAL_NAME#*O=} && braidhost=${braidhost%%,*} && braidhost=$(echo $braidhost | sed 's/ //g')
cat > node.conf <<EOL
myLegalName : "${CORDITE_LEGAL_NAME}"
p2pAddress : "${CORDITE_P2P_ADDRESS}"
compatibilityZoneURL : "${CORDITE_COMPATIBILITY_ZONE_URL}"
dataSourceProperties : {
    "dataSourceClassName" : "${CORDITE_DB_DRIVER}"
    "dataSource.url" : ${CORDITE_DB_URL}
    "dataSource.user" : "${CORDITE_DB_USER}"
    "dataSource.password" : "${CORDITE_DB_PASS}"
}
keyStorePassword : "${CORDITE_KEY_STORE_PASSWORD}"
trustStorePassword : "${CORDITE_TRUST_STORE_PASSWORD}"
devMode : ${CORDITE_DEV_MODE}
detectPublicIp: ${CORDITE_DETECT_IP}
jvmArgs : [ "-Dbraid.${braidhost}.port=${CORDITE_BRAID_PORT}" ]
jarDirs=[
    ${basedir}/postgres
]
EOL
fi

echo "node.conf contents:"
cat node.conf

# Configure notaries
# for the moment we're dealing with two systems - later we can do this in a slightly different way
if [ "$CORDITE_NOTARY" == "true" ] || [ "$CORDITE_NOTARY" == "validating" ] || [ "$CORDITE_NOTARY" == "non-validating" ] ; then
    NOTARY_VAL=false
    if [ "$CORDITE_NOTARY" == "true" ] || [ "$CORDITE_NOTARY" == "validating" ]; then
    NOTARY_VAL=true
    fi
    echo "CORDITE_NOTARY set to ${CORDITE_NOTARY}. Configuring node to be a notary with validating ${NOTARY_VAL}"
cat >> node.conf <<EOL
notary {
    validating=${NOTARY_VAL}
}
EOL
fi

if [ ! -z "$CORDITE_METERING_CONFIG" ] ; then
   echo "CORDITE_METERING_CONFIG set to ${CORDITE_METERING_CONFIG}. Creating metering-service-config.json"
   echo $CORDITE_METERING_CONFIG > metering-service-config.json
fi

if [ ! -z "$CORDITE_FEE_DISPERSAL_CONFIG" ] ; then
   echo "CORDITE_FEE_DISPERSAL_CONFIG set to ${CORDITE_FEE_DISPERSAL_CONFIG}. Creating fee-dispersal-service-config.json"
   echo $CORDITE_FEE_DISPERSAL_CONFIG > fee-dispersal-service-config.json
fi

# use cached node info if there and we want to
if [ "${CORDITE_CACHE_NODEINFO}" = "true" ]; then
    mkdir -p config
    if [ ! -f nodeInfo* ]; then
        echo there is no node info file in the basedir
        if [ ! -f config/nodeInfo* ]; then
            echo there is also no nodeInfo file cached, creating...
            java -jar corda.jar --just-generate-node-info
            echo caching config file
            mv nodeInfo* config/
            echo is it there
            ls config
        fi
        echo copy config file back to basedir
        cp config/nodeInfo* .
        echo check dir
        ls -latr
    fi
fi

# start Cordite Node, if in docker container use CMD from docker to allow override
if [ -f /.dockerenv ]; then
    "$@"
else
    java -Xms1024m -Xmx1024m -jar corda.jar --log-to-console --no-local-shell
fi

