# Copyright 2018, Cordite Foundation.

#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

FROM openjdk:8-jre-alpine

LABEL VENDOR="Cordite.foundation" \
      MAINTAINER="devops@cordite.foundation"

WORKDIR /opt/cordite

COPY . .

RUN addgroup -g 1000 -S cordite \
 && adduser -u 1000 -S cordite -G cordite \
 && chgrp -R 0 /opt/cordite \
 && chmod -R g=u /opt/cordite \
 && chown -R cordite:cordite /opt/cordite

USER cordite

ENTRYPOINT ["./start.sh"]
CMD java -Xmx1536m -Dlog4j.configurationFile=cordite-log4j2.xml -Dlog4j2.debug -jar corda.jar --log-to-console --no-local-shell